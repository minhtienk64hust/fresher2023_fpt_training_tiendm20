package src.fpt.training.assignment4

fun <T> Array<T>.swap(index1: Int, index2: Int) {
    if (index1 < 0 || index1 >= this.size || index2 < 0 || index2 >= this.size) {
        throw IllegalArgumentException("Invalid indices for swapping")
    }

    val temp = this[index1]
    this[index1] = this[index2]
    this[index2] = temp
}

fun main() {
    val arrayInt = arrayOf(1, 2, 3, 4, 5)
    println("Original array: ${arrayInt.joinToString()}")

    arrayInt.swap(1, 3)
    println("Mảng sau khi hoán đổi vị trí index 1 and 3: ${arrayInt.joinToString()}")

    val arrayString = arrayOf("Hello", "World", "Kolin", "Java", "Dart")
    println("Original array: ${arrayString.joinToString()}")

    arrayString.swap(2, 4)
    println("Mảng sau khi hoán đổi vị trí index 2 and 4: ${arrayString.joinToString()}")
}