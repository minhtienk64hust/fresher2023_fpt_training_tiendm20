package src.fpt.training.assignment4

fun <T : Comparable<T>> List<T>.findMax(begin: Int, end: Int): T? {
    if (begin < 0 || end > this.size || begin >= end) {
        throw IllegalArgumentException("Invalid range")
    }

    val subList = this.slice(begin until end)
    return subList.maxOrNull()

}

fun main() {
    val listInt = listOf(3, 1, 4, 1, 5, 9, 2, 6, 5, 3)
    val maxInSubRangeInt = listInt.findMax(2, 7)
    println("Phần tử lớn nhất trong khoảng [2, 7): $maxInSubRangeInt")

    val listString = listOf(3.2, 4.6, 7.3, 2.8)
    val maxInSubRangeDouble = listString.findMax(1, 3)
    println("Phần tử lớn nhất trong khoảng [1,3): $maxInSubRangeDouble")
}