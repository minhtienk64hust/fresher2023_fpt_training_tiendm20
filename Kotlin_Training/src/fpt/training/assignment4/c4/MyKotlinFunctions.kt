package src.fpt.training.assignment4.c4

class Calculator {
    fun add(a: Long, b: Long): Long {
        return a + b
    }

    fun subtract(a: Long, b: Long): Long {
        return a - b
    }

    fun multiply(a: Long, b: Long): Long {
        return a * b
    }

    fun divide(a: Long, b: Long): Long {
        require(b != 0L) { "Cannot divide by zero" }
        return a / b
    }
}