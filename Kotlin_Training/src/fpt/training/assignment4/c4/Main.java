package src.fpt.training.assignment4.c4;

public class Main {
    public static void main(String[] args) {
        long a = 10L;
        long b = 5L;

        long tong = new Calculator().add(a, b);
        long hieu = new Calculator().subtract(a, b);
        long nhan = new Calculator().multiply(a, b);

        try {
            long chia = new Calculator().divide(a, b);
            System.out.println("Tổng: " + tong);
            System.out.println("Hiệu: " + hieu);
            System.out.println("Nhân: " + nhan);
            System.out.println("Chia: " + chia);
        } catch (IllegalArgumentException e) {
            System.err.println("Error: " + e.getMessage());
        }
    }
}
