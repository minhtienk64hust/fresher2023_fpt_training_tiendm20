package src.fpt.training.assignment4

fun <T> Collection<T>.countMatching(predicate: (T) -> Boolean): Int {
    return this.count { predicate(it) }
}

fun main() {
    val listOfIntegers = setOf(1, 2, 3, 4, 5)
    val countOfOddNumbers = listOfIntegers.countMatching { it % 2 != 0 }
    println("Số phần tử trong tập hợp có tính chất lẻ: $countOfOddNumbers")

    val listOfPrimes = setOf(2, 3, 5, 6, 7, 8, 11)
    val countOfPrimes = listOfPrimes.countMatching { isPrime(it) }
    println("ố phần tử trong tập hợp có tính chất nguyên tố: $countOfPrimes")
}

fun isPrime(n: Int): Boolean {
    if (n <= 1) return false
    if (n == 2) return true

    for (i in 2 until n) {
        if (n % i == 0) {
            return false
        }
    }
    return true
}