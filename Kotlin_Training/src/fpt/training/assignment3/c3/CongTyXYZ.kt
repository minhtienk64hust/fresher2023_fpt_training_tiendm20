package src.fpt.training.assignment3.c3

fun main() {
    val danhSachKhachHang = ArrayList<KhachHang>()

    danhSachKhachHang.add(KhachHangBinhThuong(10, 100.toDouble()))
    danhSachKhachHang.add(KhachHangThanThiet(5, 200.toDouble(), 3))
    danhSachKhachHang.add(KhachHangDacBiet(20, 50.toDouble()))
    var tongTienCtyThuDuoc = 0.0

    for (khachHang in danhSachKhachHang) {
        val tienPhaiTra = khachHang.tinhTienPhaiTra()
        println("Khách hàng phải trả: $tienPhaiTra")
        tongTienCtyThuDuoc += tienPhaiTra
    }
    println("Tổng số tiền công ty thu được: $tongTienCtyThuDuoc")
}
