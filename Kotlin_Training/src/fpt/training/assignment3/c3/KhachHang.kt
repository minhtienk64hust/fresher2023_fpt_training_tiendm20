package src.fpt.training.assignment3.c3

abstract class KhachHang(protected var soLuongHang: Int, protected var donGiaHang: Double) : TinhTien

class KhachHangBinhThuong(soLuongHang: Int, donGiaHang: Double) : KhachHang(soLuongHang, donGiaHang) {
    override fun tinhTienPhaiTra(): Double {
        val thueVAT = 0.1
        return soLuongHang * donGiaHang * (1 + thueVAT)
    }
}

class KhachHangDacBiet(soLuongHang: Int, donGiaHang: Double) : KhachHang(soLuongHang, donGiaHang) {
    override fun tinhTienPhaiTra(): Double {
        val thueVAT = 0.1
        return soLuongHang * donGiaHang * 0.5 * (1 + thueVAT)
    }
}

class KhachHangThanThiet(soLuongHang: Int, donGiaHang: Double, var soNamThanThiet: Int) : KhachHang(soLuongHang, donGiaHang) {
    override fun tinhTienPhaiTra(): Double {
        val thueVAT = 0.1
        val phanTramKhuyenMai = Math.max(soNamThanThiet * 0.05, 0.5)
        return soLuongHang * donGiaHang * (1 - phanTramKhuyenMai) * (1 + thueVAT)
    }
}


