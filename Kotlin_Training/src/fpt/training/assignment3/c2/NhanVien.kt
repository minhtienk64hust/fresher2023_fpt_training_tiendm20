package src.fpt.training.assignment3.c2

abstract class NhanVien(var hoTen: String, var ngaySinh: String, var luong: Double) {
    constructor(hoTen: String, ngaySinh: String) : this(hoTen, ngaySinh, 0.toDouble())

    open abstract fun tinhLuong()
}

class NhanVienSanXuat(
    hoTen: String,
    ngaySinh: String,
    var soSanPham: Int,
    var luongCanBan: Double
): NhanVien(hoTen, ngaySinh) {

    override fun tinhLuong() {
        luong = luongCanBan + (soSanPham * 5000)
    }
}

class NhanVienVanPhong(
    hoTen: String,
    ngaySinh: String,
    var soNgayLamViec: Int
): NhanVien(hoTen, ngaySinh) {


    override fun tinhLuong() {
        luong = (100000 * soNgayLamViec).toDouble()
    }
}