package src.fpt.training.assignment3.c2

fun main() {
    var nhanVienSanXuat = NhanVienSanXuat("Nguyen Van A", "01/01/1990", 100, 3000000.toDouble())

    var nhanVienVanPhong = NhanVienVanPhong("Tran Thi B", "15/05/1985", 20)

    nhanVienSanXuat.tinhLuong();
    nhanVienVanPhong.tinhLuong();

    println("Thong tin nhan vien san xuat:");
    println("Ho Ten: " + nhanVienSanXuat.hoTen);
    println("Ngay Sinh: " + nhanVienSanXuat.ngaySinh);
    println("Luong: " + nhanVienSanXuat.luong);

    println("\nThong tin nhan vien van phong:");
    println("Ho Ten: " + nhanVienVanPhong.hoTen);
    println("Ngay Sinh: " + nhanVienVanPhong.ngaySinh);
    println("Luong: " + nhanVienVanPhong.luong);


}