package src.fpt.training.assignment3.c1

fun main() {
    val student = Student()
    student.inputInfo()
    val teacher = Teacher()
    teacher.inputInfo()

    student.showInfo()
    teacher.showInfo()

    print(student.setScholarship())
    print(teacher.calculateSalary())
}