package src.fpt.training.assignment3.c1

import java.io.IOException

open class Person(var name: String, var gender: String, var dateOfBirth: String, var address: String) {
    constructor(): this("", "", "", "");

    open fun showInfo() {
        println(String.format("%-15s %-15s %-15s %-15s", "Tên", "Giới tính", "Ngày sinh", "Địa chỉ"));
        println(String.format("%-15s %-15s %-15s %-15s", name, gender, dateOfBirth, address));
    }

    open fun inputInfo() {
        println("Nhập tên: ")
        name = checkInputName()

        println("Nhập giới tính: ")
        gender = checkInputGender()

        println("Nhập ngày sinh: ")
        dateOfBirth = readLine()!!

        println("Nhập địa chỉ: ")
        address = readLine()!!
    }
}

fun Person.checkInputName(): String {
    while (true) {
        try {
            val name: String? = readLine()?.trim()
            val regex = "^[a-zA-Z ]+$"
            if (name != null) {
                if (name.matches(Regex(regex))) {
                    return name
                } else {
                    println("Please input a valid name!")
                }
            }

        } catch (e: IOException) {
            println("An error occurred while reading user input: " + e.message)
        }
    }
}

fun Person.checkInputGender(): String {
    while (true) {
        try {
            val gender: String? = readLine()?.trim()
            val regex = "^(Male|Female|Other)$"
            if (gender != null) {
                if (gender.matches(Regex(regex))) {
                    return gender
                } else {
                    println("Please input a valid gender (Male, Female, or Other)!")
                }
            }

        } catch (e: IOException) {
            println("An error occurred while reading user input: " + e.message)
        }
    }
}
