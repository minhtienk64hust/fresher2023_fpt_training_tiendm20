package src.fpt.training.assignment3.c1

import java.io.IOException

class Teacher(
    name: String,
    gender: String,
    dateOfBirth: String,
    address: String,
    var className: String,
    var hourlyWage: Int,
    var numberOfHoursPerMonth: Int
) : Person(name, gender, dateOfBirth, address) {

    constructor(): this("", "", "", "", "", 0, 0)

    override fun inputInfo() {
        super.inputInfo()

        println("Nhập tên lớp: ")
        className = checkInputClassName()

        println("Nhập lương một giờ dạy: ")
        hourlyWage = readLine()!!.toInt()

        println("Nhập số giờ dạy mỗi tháng: ")
        numberOfHoursPerMonth = readLine()!!.toInt()

    }

    override fun showInfo() {
        super.showInfo()
        println(String.format("%-15s %-25s %-15s", "Tên lớp", "Lương 1 giờ dạy", "Số giờ dạy"));
        println(String.format("%-15s %-25s %-15s", className, hourlyWage, numberOfHoursPerMonth));
    }

    fun calculateSalary(): Int {
        val isEveningClass = className.startsWith("L") || className.startsWith("M")

        val salary = if (isEveningClass) {
            this.hourlyWage * this.numberOfHoursPerMonth + 500000
        } else {
            this.hourlyWage * this.numberOfHoursPerMonth
        }

        return salary;
    }
}

fun Teacher.checkInputClassName(): String {
    while (true) {
        try {
            val className: String? = readLine()?.trim()
            val regex = "^[GHIKLM]{1}[0-9]{2}$"
            if (className != null) {
                if (className.matches(Regex(regex))) {
                    return className
                } else {
                    println("Please input a valid class name!")
                }
            }

        } catch (e: IOException) {
            println("An error occurred while reading user input: " + e.message)
        }
    }
}
