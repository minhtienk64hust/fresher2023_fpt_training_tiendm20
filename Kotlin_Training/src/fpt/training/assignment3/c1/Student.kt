package src.fpt.training.assignment3.c1

import java.io.IOException

class Student(
    name: String,
    gender: String,
    dateOfBirth: String,
    address: String,
    var studentId: String,
    var gpa: Double,
    var email: String
) : Person(name, gender, dateOfBirth, address) {

    constructor(): this("", "", "", "", "", 0.0, "")

    override fun inputInfo() {
        super.inputInfo()

        println("Nhập mã sinh viên: ")
        studentId = checkInputStudentId()

        println("Nhập điểm trung bình: ")
        gpa = checkInputGpa()

        println("Nhập email: ")
        email = checkInputEmail()

    }

    override fun showInfo() {
        super.showInfo()
        println(String.format("%-15s %-15s %-15s", "Mã sinh viên", "Điểm trung bình", "Email"));
        println(String.format("%-15s %-15s %-15s", studentId, gpa, email));
    }

    fun setScholarship() {
        if (gpa >= 8.0) {
            println("Sinh viên đạt học bổng")
        } else {
            println("Sinh viên không đạt học bổng")
        }
    }
}

fun Student.checkInputStudentId(): String {
    while (true) {
        try {
            val studentId: String? = readLine()?.trim()
            val regex = "^SV[0-9]{2,9}$"
            if (studentId != null) {
                if (studentId.matches(Regex(regex))) {
                    return studentId
                } else {
                    println("Please input a valid student ID!")
                }
            }

        } catch (e: IOException) {
            println("An error occurred while reading user input: " + e.message)
        }
    }
}

fun Student.checkInputGpa(): Double {
    while (true) {
        try {
            val grade: Double? = readLine()?.toDouble()
            if (grade != null) {
                if (grade in 0.0..10.0) {
                    return grade
                } else {
                    println("Please input a valid grade (0.0 - 10.0)!")
                }
            }

        } catch (e: IOException) {
            println("An error occurred while reading user input: " + e.message)
        }
    }
}

fun Student.checkInputEmail(): String {
    while (true) {
        try {
            val email: String? = readLine()?.trim()
            val regex = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$"
            if (email != null) {
                if (email.matches(Regex(regex))) {
                    return email
                } else {
                    println("Please input a valid email!")
                }
            }

        } catch (e: IOException) {
            println("An error occurred while reading user input: " + e.message)
        }
    }
}