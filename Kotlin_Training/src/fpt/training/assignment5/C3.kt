package src.fpt.training.assignment5

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

suspend fun calculateSum(n: Int): Int {
    var sum = 0
    for (i in 0..n) {
        sum += i
    }
    return sum
}

fun generateResultFlow(): Flow<Int> = flow {
    for (i in 0..10) {
        val sum = calculateSum(i)
        emit(sum)
        delay(500)
    }
}

fun main() = runBlocking {
    val resultFlow = generateResultFlow()
    resultFlow.collect { sum ->
        println("$sum")
    }
}

