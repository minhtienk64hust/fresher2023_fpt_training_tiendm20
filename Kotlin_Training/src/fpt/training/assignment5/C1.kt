package src.fpt.training.assignment5

import kotlinx.coroutines.*

fun calculateFibonacci(n: Int): Int = if (n <= 1) {
    n
} else {
    calculateFibonacci(n - 1) + calculateFibonacci(n - 2)
}

fun main() = runBlocking {
    val n = 10 // Số Fibonacci thứ N muốn tính

    val result = calculateFibonacciAsync(n)
    println("Fibonacci số thứ $n là: $result")
}

suspend fun calculateFibonacciAsync(n: Int): Int = coroutineScope {
    val deferred = async(Dispatchers.Default) {
        calculateFibonacci(n)
    }
    deferred.await()
}