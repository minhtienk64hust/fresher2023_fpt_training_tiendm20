package src.fpt.training.assignment5

import kotlinx.coroutines.*

class InvalidUsernameException: Exception("Username invalid")
class InvalidNumberException : Exception("Username contains invalid numbers")
class InvalidSpecialCharacterException : Exception("Username contains invalid special characters")
class InvalidLengthException : Exception("Username length is invalid")

suspend fun validateUsername(username: String): Boolean {
    return try {
        coroutineScope {
            val job1 = launch(Dispatchers.Default) {
                checkNumbers(username)
            }
            val job2 = launch(Dispatchers.Default) {
                checkSpecialCharacters(username)
            }
            val job3 = launch(Dispatchers.Default) {
                checkLength(username)
            }

            job1.join()
            job2.join()
            job3.join()

            true
        }
    } catch (e: Exception) {
        false
    }
}

private suspend fun checkNumbers(username: String) {
    if (username.any { it.isDigit() }) {
        throw InvalidNumberException()
    }
}

private suspend fun checkSpecialCharacters(username: String) {
    if (username.any { it in "!@#\$%^&*()_" }) {
        throw InvalidSpecialCharacterException()
    }
}

private suspend fun checkLength(username: String) {
    if (username.length > 16) {
        throw InvalidLengthException()
    }
}

fun main() {
    val validUsername = "minhtien"
    val invalidUsername = "minhtien123@!"

    runBlocking {
        val isValid1 = validateUsername(validUsername)
        val isValid2 = validateUsername(invalidUsername)

        println("Is '$validUsername' valid? $isValid1")
        println("Is '$invalidUsername' valid? $isValid2")
    }
}