package src.fpt.training.assignment2

fun Int.toHexString(): String {
    return Integer.toHexString(this)
}

fun main() {
    val number = 200
    val hexStr = number.toHexString()
    println("Chuyển đổi số nguyên $number sang nhị phân là: $hexStr")
}