package src.fpt.training.assignment2

fun main(args: Array<String>) {

    val number1 = readLine()!!.toInt()
    val number2 = readLine()!!.toInt()

    val primeNumbers = generateSequence(number1) { it + 1 }
            .filter { it.isPrime() }
            .takeWhile { it in number1..number2 }
            .toList()

    println(primeNumbers)
}

fun Int.isPrime(): Boolean {
    if (this < 2) {
        return false
    }

    for (i in 2..(Math.sqrt(this.toDouble()) + 1).toInt()) {
        if (this % i == 0) {
            return false
        }
    }
    return true
}