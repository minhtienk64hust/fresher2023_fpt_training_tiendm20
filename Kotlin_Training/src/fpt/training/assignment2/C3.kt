package src.fpt.training.assignment2

fun findPrimeSumPairs(target: Int, checkIsPrime: (Int) -> Boolean) {
    println("Cặp tổng số nguyên tố có tổng là $target:")
    for (i in 2 until target/2) {
        if (checkIsPrime(i)) {
            val complement = target - i
            if (checkIsPrime(complement)) {
                println("$target = $i + $complement")
            }
        }
    }
}

fun isPrime(n: Int): Boolean {
    if (n <= 1) return false

    for (i in 2..Math.sqrt(n.toDouble()).toInt()) if (n % i == 0) return false
    return true
}

fun main() {
    print("Nhập vào một số nguyên: ")
    val number = readLine()?.toIntOrNull()

    number?.let {
        findPrimeSumPairs(number, ::isPrime)
    }
}