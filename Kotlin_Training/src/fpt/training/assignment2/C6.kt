package src.fpt.training.assignment2

fun sumOfDigits(number: Int): Int {
    var sum = 0
    var temp = number
    while (temp > 0) {
        sum += temp % 10
        temp /= 10
    }
    return sum
}

fun main() {
    print("Nhập vào một số tự nhiên: ")
    val number = readLine()!!.toInt()

    println("Tổng các chữ số của số $number là: ${sumOfDigits(number)}")
}