package src.fpt.training.assignment2

fun String.toBinaryString(): String {
    return this.toBigInteger(16).toString(2)
}

fun main() {
    val hexByte = "C8"
    val binaryStr = hexByte.toBinaryString()
    println("Chuyển đổi một byte của chuỗi thập lục phân $hexByte sang nhị phân: $binaryStr")
}