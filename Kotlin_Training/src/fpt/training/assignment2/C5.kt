package src.fpt.training.assignment2

fun sum(number: Int): Int {
    return if (number <= 0) {
        0
    } else {
        number + sum(number - 1)
    }
}

fun main() {
    print("Nhập vào một số: ")
    val number = readLine()!!.toInt()

    println("Tổng các số tự nhiên nhỏ hơn hoặc bằng $number là: ${sum(number)}")

}