package src.fpt.training.assignment1

fun main() {
    var inputNumber: Int?

    do {
        print("Nhập một số nguyên có hai chữ số: ")
        inputNumber = readLine()?.toIntOrNull()
    } while ((inputNumber == null) || (inputNumber !in (10..99)))

    println("Giá trị nhị phân: ${Integer.toBinaryString(inputNumber)}")
    println("Giá trị thập lục phân: ${Integer.toHexString(inputNumber)}")
}
