package src.fpt.training.assignment1

fun main() {
    var numbers = mutableListOf<Int>()

    for (i in 100..200) {
        if (i % 7 == 0 && i % 5 != 0) {
            numbers.add(i)
        }
    }

    println(numbers.joinToString(", "))
}
