package src.fpt.training.assignment1

fun main() {
    println("Nhập tháng: ")
    val month = readLine()!!.toInt()
    println("Nhập năm: ")
    val year = readLine()!!.toInt()

    val isLeapYear = year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)

    val daysOfMonth = when (month) {
        1, 3, 5, 7, 8, 10, 12 -> 31
        4, 6, 9, 11 -> 30
        2 -> if (isLeapYear) 29 else 28
        else -> throw IllegalArgumentException("Tháng không hợp lệ")
    }

    println("Tháng $month năm $year có $daysOfMonth ngày")
}