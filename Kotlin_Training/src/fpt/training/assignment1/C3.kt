package src.fpt.training.assignment1

import java.util.*

fun main() {
    print("Nhập số phần tử của mảng: ")
    val n = readLine()?.toIntOrNull() ?: return

    val array = IntArray(n)

    for (i in 0 until n) {
        print("Nhập phần tử thứ $i: ")
        array[i] = readLine()?.toInt() ?: 0
    }

    array.sort()

    println("Mảng theo thứ tự tăng dần:")
    print(Arrays.toString(array));
}