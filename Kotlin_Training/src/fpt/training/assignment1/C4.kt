package src.fpt.training.assignment1

fun main() {
    val input = readLine()!!

    val words = input.split(" ").toMutableList()
    val count = words.size

    for (i in 0 until count) {
        if (i == 0 || words[i - 1].endsWith(".")) {
            words[i] = words[i].capitalize()
        }
    }

    println(words.joinToString(" "))
}
