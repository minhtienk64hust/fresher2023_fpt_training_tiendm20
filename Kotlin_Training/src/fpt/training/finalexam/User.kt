package src.fpt.training.finalexam

class User(var name: String, var age: Int, var email: String) {
    fun displayInfo() {
        println(String.format("%-15s %-15s %-15s", name, age, email))
    }

}