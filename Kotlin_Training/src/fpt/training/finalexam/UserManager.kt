package src.fpt.training.finalexam

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import kotlin.system.exitProcess

class UserManager(var users: MutableList<User>) {

    constructor(): this(mutableListOf<User>())

    fun displayMenu() {
        println("1.  Add user")
        println("2.  Display user list")
        println("3.  Find users by name")
        println("4.  Sort user list")
        println("5.  Random add user")
        println("6.  Exit")
        println("Enter your choice: ")
        val choice: Int = Validate().checkIntLimit(1, 6)
        when (choice) {
            1 -> addUser()
            2 -> displayInfo()
            3 -> findUsersByName()
            4 -> sortUsersByName()
            5 -> randomAddUsers()
            6 -> exitProcess(0)
            else -> println("Please input from 1 to 6!")
        }
    }

    fun addUser() {
        println("Enter name: ")
        val name: String = Validate().checkInputName()
        println("Enter age: ")
        val age: Int = Validate().checkInputAge()
        println("Enter email: ")
        val email: String = Validate().checkInputEmail()
        processAddUser(name, age, email)
    }

    private fun processAddUser(name: String, age: Int, email: String) {
        val user = User(name, age, email)
        users.add(user)
        println("Contact added successfully")
    }

    fun displayInfo() {
        if (users.isEmpty()) {
            println("List is empty!")
        } else {
            println(
                "--------------------------------------------------Display imformation--------------------------------------------------"
            )
            println(String.format("%-15s %-15s %-15s", "Name", "Age", "Email"))
            for (user in users) {
                user.displayInfo()
            }
        }
    }

    fun findUsersByName() {
        println("Enter name: ")
        val name: String = readLine() ?: ""
        processFindUsersByName(name)
    }

    fun processFindUsersByName(name: String) {
        val foundedUsers: List<User> = users.filter { it.name.contains(name, ignoreCase = true) }
        if (foundedUsers.isEmpty()) {
            println("List is empty!")
        } else {
            println(
                "--------------------------------------------------Display imformation--------------------------------------------------"
            )
            println(String.format("%-15s %-15s %-15s", "Name", "Age", "Email"))
            for (user in foundedUsers) {
                user.displayInfo()
            }
        }
    }

    fun sortUsersByName() {
        users.sortBy { it.name.lowercase(Locale.getDefault()) }
    }


    fun randomAddUsers() {
        repeat(10) { index ->
            val user = User("User$index", (18 + index), "user$index@example.com")
            GlobalScope.launch {
                val delayTime = (500..2000).random().toLong()
                delay(delayTime)
                users.add(user)
                println("Added user: $user")
            }
        }
    }
}

