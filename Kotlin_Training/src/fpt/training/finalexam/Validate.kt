package src.fpt.training.finalexam

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

class Validate {
    fun checkIntLimit(min: Int, max: Int): Int {
        while (true) {
            try {
                val result = Integer.parseInt(readLine()!!.trim())
                if (result < min || result > max) {
                    println("Please input number in range: $min to $max")
                } else {
                    return result
                }
            } catch (e: IOException) {
                println("An error occurred while reading user input: " + e.message)
            } catch (e: NumberFormatException) {
                println("Please input number in range: $min to $max")
            }
        }
    }

    fun checkInputName(): String {
        while (true) {
            try {
                val name = readLine()?.trim()
                val regex = "^[a-zA-Z ]+$"
                if (name != null && name.matches(Regex(regex))) {
                    return name
                } else {
                    println("Please input a valid name!")
                }
            } catch (e: IOException) {
                println("An error occurred while reading user input: " + e.message)
            }
        }
    }

    fun checkInputAge(): Int {
        while (true) {
            try {
                val input = readLine()?.trim()
                val age = input?.toIntOrNull()

                if (age != null && age >= 0) {
                    return age
                } else {
                    println("Please input a valid age!")
                }
            } catch (e: NumberFormatException) {
                println("Please input a valid age!")
            }
        }
    }

    fun checkInputEmail(): String {
        while (true) {
            try {
                val email = readLine()!!.trim()
                val regex = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}\$"
                if (email.matches(Regex(regex))) {
                    return email
                } else {
                    println("Please input a valid email!")
                }
            } catch (e: IOException) {
                println("An error occurred while reading user input: " + e.message)
            }
        }
    }

}