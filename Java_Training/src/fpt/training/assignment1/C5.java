package fpt.training.assignment1;

import java.util.Scanner;

public class C5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Nhập một số nguyên dương: ");
        int n = scanner.nextInt();

        if (n < 0) {
            System.out.println("Số không hợp lệ. Vui lòng nhập một số nguyên dương.");
            return;
        }

        long giaiThua = tinhGiaiThua(n);

        System.out.println(n + "! = " + giaiThua);
        scanner.close();
    }

    public static long tinhGiaiThua(int n) {
        if (n == 0 || n == 1) {
            return 1;
        } else {
            long giaiThua = 1;
            for (int i = 2; i <= n; i++) {
                giaiThua *= i;
            }
            return giaiThua;
        }
    }
}
