package fpt.training.assignment1;

import java.util.Scanner;

public class C2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập số a: ");
        double a = scanner.nextDouble();
        System.out.print("Nhập số b: ");
        double b = scanner.nextDouble();
        double max = (a > b) ? a : b;
        System.out.println("Số lớn nhất là: " + max);
        scanner.close();
    }
}
