package fpt.training.assignment1;

import java.util.Scanner;

class Calculator {
    public double add(double a, double b) {
        return a + b;
    }

    public double subtract(double a, double b) {
        return a - b;
    }

    public double multiply(double a, double b) {
        return a * b;
    }

    public double divide(double a, double b) {
        if (b != 0) {
            return a / b;
        } else {
            throw new ArithmeticException("Lỗi: Số b không thể là 0 trong phép chia.");
        }
    }
}

public class C3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Calculator calculator = new Calculator();
        System.out.print("Nhập số a: ");
        double a = scanner.nextDouble();
        System.out.print("Nhập số b: ");
        double b = scanner.nextDouble();
        System.out.print("Nhập phép toán (+, -, *, /): ");
        char operation = scanner.next().charAt(0);
        double result = 0;

        switch (operation) {
            case '+':
                result = calculator.add(a, b);
                break;
            case '-':
                result = calculator.subtract(a, b);
                break;
            case '*':
                result = calculator.multiply(a, b);
                break;
            case '/':
                try {
                    result = calculator.divide(a, b);
                } catch (ArithmeticException e) {
                    System.out.println("Lỗi phép chia");
                    System.exit(1);
                }
                break;
            default:
                System.out.println("Lỗi: Phép toán không hợp lệ.");
                System.exit(1);
        }

        System.out.println("Kết quả: " + result);

        scanner.close();
    }
}
