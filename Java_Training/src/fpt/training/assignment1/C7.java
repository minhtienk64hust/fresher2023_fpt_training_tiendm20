package fpt.training.assignment1;

import java.util.Scanner;

public class C7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Nhập một số nguyên dương n: ");
        int n = scanner.nextInt();

        if (n <= 0) {
            System.out.println("Số không hợp lệ. Vui lòng nhập một số nguyên dương.");
            return;
        }

        int tong = tinhTongSoChan(n);

        System.out.println("Tổng các số chẵn nhỏ hơn hoặc bằng " + n + " là: " + tong);

        scanner.close();
    }

    public static int tinhTongSoChan(int n) {
        int tong = 0;
        for (int i = 2; i <= n; i += 2) {
            tong += i;
        }
        return tong;
    }
}
