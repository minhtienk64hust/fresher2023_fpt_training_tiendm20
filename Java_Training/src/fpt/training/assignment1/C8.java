package fpt.training.assignment1;

import java.util.Scanner;

public class C8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Nhập số a: ");
        int a = scanner.nextInt();

        System.out.print("Nhập số b: ");
        int b = scanner.nextInt();

        int uocChungLonNhat = timUocChungLonNhat(a, b);

        System.out.println("Ước số chung lớn nhất của " + a + " và " + b + " là: " + uocChungLonNhat);

        scanner.close();
    }

    public static int timUocChungLonNhat(int a, int b) {
        while (b != 0) {
            int temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }
}
