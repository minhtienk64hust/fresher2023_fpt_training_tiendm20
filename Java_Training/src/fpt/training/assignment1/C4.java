package fpt.training.assignment1;

import java.util.Scanner;

public class C4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Nhập tháng (1-12): ");
        int thang = scanner.nextInt();

        int quy = thang % 3 == 0 ? thang/3 : thang/ 3 + 1;

        System.out.println("Tháng " + thang + " thuộc quý " + quy);

        scanner.close();
    }
}
