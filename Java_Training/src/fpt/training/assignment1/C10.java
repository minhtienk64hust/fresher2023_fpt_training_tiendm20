package fpt.training.assignment1;

import java.util.Scanner;

public class C10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int nam;

        System.out.print("Nhập một năm (năm >= 0): ");
        nam = scanner.nextInt();

        if (nam >= 0) {
            if (kiemTraNamNhuan(nam)) {
                System.out.println(nam + " là năm nhuận.");
            } else {
                System.out.println(nam + " không phải là năm nhuận.");
            }
        } else {
            System.out.println("Năm không hợp lệ.");
        }

        scanner.close();
    }

    public static boolean kiemTraNamNhuan(int nam) {
        if ((nam % 4 == 0 && nam % 100 != 0) || nam % 400 == 0) {
            return true;
        }
        return false;
    }
}
