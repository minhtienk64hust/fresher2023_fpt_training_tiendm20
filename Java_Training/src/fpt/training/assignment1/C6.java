package fpt.training.assignment1;

import java.util.Scanner;

public class C6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Nhập một số nguyên dương n: ");
        int n = scanner.nextInt();

        if (n <= 0) {
            System.out.println("Số không hợp lệ. Vui lòng nhập một số nguyên dương.");
            return;
        }

        int tong = tinhTongTu1DenN(n);

        System.out.println("Tổng từ 1 đến " + n + " là: " + tong);

        scanner.close();
    }

    public static int tinhTongTu1DenN(int n) {
        int tong = 0;
        for (int i = 1; i <= n; i++) {
            tong += i;
        }
        return tong;
    }
}
