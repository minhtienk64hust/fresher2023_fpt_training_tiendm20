package fpt.training.assignment1;

import java.util.Scanner;

public class C1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập điểm trung bình: ");
        double diemTrungBinh = scanner.nextDouble();

        if (diemTrungBinh >= 5.0) {
            System.out.println("Đậu");
        } else {
            System.out.println("Rớt");
        }

        scanner.close();
    }
}
