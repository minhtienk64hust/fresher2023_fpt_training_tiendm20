package fpt.training.assignment2;

import java.util.HashMap;
import java.util.Scanner;

public class C2 {

    public static void main(String[] args) {
        HashMap<String, Student> students = new HashMap<>();

        // Hiển thị menu chương trình
        int choice;
        do {
            System.out.println("** Menu chương trình **");
            System.out.println("1. Nhập N sinh viên");
            System.out.println("2. In thông tin sinh viên");
            System.out.println("3. Tìm kiếm sinh viên");
            System.out.println("4. Thoát");

            System.out.print("Nhập lựa chọn của bạn: ");
            Scanner scanner = new Scanner(System.in);
            choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    // Nhập n sinh viên
                    System.out.println("Nhập số lượng sinh viên cần nhập: ");
                    int n;
                    n = scanner.nextInt();
                    students = new C2().input(n);
                    break;
                case 2:
                    // In thông tin sinh viên
                    System.out.println("Danh sách sinh viên:");
                    if (students.isEmpty()) {
                        System.out.println("Không có thông tin lớp sinh viên");
                        break;
                    }
                    for (Student student : students.values()) {
                        student.displayInfo();
                    }
                    break;
                case 3:
                    // Tìm kiếm sinh viên
                    System.out.print("Nhập RollNo của sinh viên cần tìm kiếm: ");
                    String rollNo = scanner.next();
                    if (students.containsKey(rollNo)) {
                        Student student = students.get(rollNo);
                        student.displayInfo();
                    } else {
                        System.out.println("Không tìm thấy sinh viên có RollNo " + rollNo);
                    }
                    break;
                case 4:
                    // Thoát
                    System.out.println("Thoát chương trình");
                    break;
                default:
                    System.out.println("Lựa chọn không hợp lệ");
                    break;
            }
        } while (choice != 4);
    }

    HashMap<String, Student> input(int n) {
        HashMap<String, Student> students = new HashMap<>();
        for (int i = 0; i < n; i++) {
            System.out.println("Nhập thông tin sinh viên thứ " + (i + 1) + ":");
            Scanner scanner = new Scanner(System.in);
            System.out.print("RollNo: ");
            String rollNo = scanner.next();
            System.out.print("Name: ");
            String name = scanner.next();
            System.out.print("Sex: ");
            String sex = scanner.next();
            System.out.print("Age: ");
            int age = scanner.nextInt();
            System.out.print("Email: ");
            String email = scanner.next();
            System.out.print("Address: ");
            scanner.nextLine(); // Đọc dấu Enter còn lại
            String address = scanner.nextLine();

            Student student = new Student(rollNo, name, sex, age, email, address);

            // Lưu thông tin sinh viên vào HashMap
            students.put(rollNo, student);
        }
        return students;
    }

}

class Student {
    private String rollNo;
    private String name;
    private String sex;
    private int age;
    private String email;
    private String address;

    public Student(String rollNo, String name, String sex, int age, String email, String address) {
        this.rollNo = rollNo;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.email = email;
        this.address = address;
    }

    public String getRollNo() {
        return rollNo;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void displayInfo() {
        System.out.println("Thông tin sinh viên:");
        System.out.println("RollNo: " + getRollNo());
        System.out.println("Name: " + getName());
        System.out.println("Sex: " + getSex());
        System.out.println("Age: " + getAge());
        System.out.println("Email: " + getEmail());
        System.out.println("Address: " + getAddress());
    }
}