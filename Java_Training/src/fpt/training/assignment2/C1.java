package fpt.training.assignment2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class C1 {
    public static void main(String[] args) {
        List<String> colors = List.of("red", "green", "blue");
        List<String> colors2 = List.of("yellow", "orange", "purple");

        // Tạo mới 2 List list và list2 có các phần tử là phần tử của 2 mảng vừa khai báo
        List<String> list = new ArrayList<String>();
        List<String> list2 = new ArrayList<String>();
        list.addAll(colors);
        list2.addAll(colors2);

        // Thêm các phần tử có trong list2 vào trong list, sau đó đưa list2 trở về 1 danh sách rỗng (không có phần tử nào)
        list.addAll(list2);
        list2.clear();

        // Hiển thị list
        System.out.println("List sau khi thêm các phần tử của list2 vào: " + list);
        System.out.println("List2 sau đó là: " + list2);

        // Chuyển các phần tử có trong list thành ký tự in hoa và hiển thị lại list đó
        list = list.stream().map((e) -> e.toUpperCase()).collect(Collectors.toList());;
        System.out.println("List sau khi chuyển các phần tử thành ký tự in hoa: " + list);

        // Xóa các phần tử trong list từ phần tử thứ 4 đến phần tử thứ 6
        list.subList(3, 6).clear();

        // Hiển thị list sau khi xóa
        System.out.println("List sau khi xóa các phần tử từ phần tử thứ 4 đến phần tử thứ 6: " + list);

        // Đảo ngược list và hiển thị lại list sau khi đảo ngược
        Collections.reverse(list);
        System.out.println("List sau khi đảo ngược: " + list);
    }
}
