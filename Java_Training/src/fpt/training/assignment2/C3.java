package fpt.training.assignment2;

import java.util.ArrayList;
import java.util.Scanner;

class Employee {
    private static int nextId = 1;
    private int id;
    private String name;
    private String gender;
    private String birthdate;
    private String phoneNumber;
    private String educationLevel;

    public Employee(String name, String gender, String birthdate, String phoneNumber, String educationLevel) {
        this.id = nextId++;
        this.name = name;
        this.gender = gender;
        this.birthdate = birthdate;
        this.phoneNumber = phoneNumber;
        this.educationLevel = educationLevel;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEducationLevel() {
        return educationLevel;
    }

    public void displayInfo() {
        System.out.println("ID: " + id);
        System.out.println("Name: " + name);
        System.out.println("Gender: " + gender);
        System.out.println("Birthdate: " + birthdate);
        System.out.println("Phone Number: " + phoneNumber);
        System.out.println("Education Level: " + educationLevel);
    }

    public void updateInfo(String name, String gender, String birthdate, String phoneNumber, String educationLevel) {
        if(!name.isBlank()) this.name = name;
        if(!gender.isBlank()) this.gender = gender;
        if (!birthdate.isBlank()) this.birthdate = birthdate;
        if (!phoneNumber.isBlank()) this.phoneNumber = phoneNumber;
        if (!educationLevel.isBlank()) this.educationLevel = educationLevel;
    }
}

class EmployeeManagementSystem  {
    ArrayList<Employee> employees;

    public EmployeeManagementSystem() {
        employees = new ArrayList<>();
    }

    public void excute() {
        while (true) {
            System.out.println("Menu:");
            System.out.println("1. Add Employee");
            System.out.println("2. Display Employees");
            System.out.println("3. Update Employee");
            System.out.println("4. Find Employee");
            System.out.println("5. Quit");
            System.out.println("Enter your choice: ");
            Scanner scanner = new Scanner(System.in);
            String choice = scanner.nextLine();

            switch (choice) {
                case "1":
                    addEmployee();
                    break;
                case "2":
                    displayEmployees();
                    break;
                case "3":
                    updateEmployee();
                    break;
                case "4":
                    findEmployee();
                    break;
                case "5":
                    System.out.println("Quit!");
                    System.exit(0);
                default:
                    System.out.println("Lựa chọn không hợp lệ");
            }
        }
    }

    public void addEmployee() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter name: ");
        String name = scanner.nextLine();
        System.out.println("Enter gender (Male/Female): ");

        String gender = scanner.nextLine();
        System.out.println("Enter birthdate (dd/mm/yyyy): ");

        String birthdate = scanner.nextLine();
        System.out.println("Enter phone number: ");

        String phoneNumber = scanner.nextLine();
        System.out.println("Enter education level (Trung cấp/Cao đẳng/Đại học): ");

        String educationLevel = scanner.nextLine();

        Employee employee = new Employee(name, gender, birthdate, phoneNumber, educationLevel);
        employees.add(employee);
        System.out.println("Employee added with ID: " + employee.getId());
    }

    public void displayEmployees() {
        if (employees.isEmpty()) {
            System.out.println("Not found information");
            return;
        }
        for (Employee employee : employees) {
            employee.displayInfo();
        }
    }

    public void updateEmployee() {
        System.out.println("Enter the ID of the employee you want to update: ");
        Scanner scanner = new Scanner(System.in);
        int idToUpdate = scanner.nextInt();
        scanner.nextLine(); // Consume the newline

        for (Employee employee : employees) {
            if (employee.getId() == idToUpdate) {
                System.out.println("Here is the information of the employee that needs to be updated: ");
                employee.displayInfo();
                System.out.println("Updated:");
                System.out.println("Enter updated name: ");
                String updatedName = scanner.nextLine();
                System.out.println("Enter updated gender (Male/Female): ");

                String updatedGender = scanner.nextLine();
                System.out.println("Enter updated birthdate (dd/mm/yyyy): ");

                String updatedBirthdate = scanner.nextLine();
                System.out.println("Enter updated phone number: ");

                String updatedPhoneNumber = scanner.nextLine();
                System.out.println("Enter updated education level (Trung cấp/Cao đẳng/Đại học): ");

                String updatedEducationLevel = scanner.nextLine();

                employee.updateInfo(updatedName, updatedGender, updatedBirthdate, updatedPhoneNumber, updatedEducationLevel);
                System.out.println("Employee with ID " + idToUpdate + " updated.");
                return;
            }
        }
        System.out.println("Employee with ID " + idToUpdate + " not found.");
    }

    public void findEmployee() {
        System.out.println("Enter the ID or name of the employee you want to find: ");
        Scanner scanner = new Scanner(System.in);

        String searchInput;

        do {
            searchInput = scanner.nextLine();
        } while (searchInput.isBlank());

        boolean found = false;
        for (Employee employee : employees) {
            if (Integer.toString(employee.getId()).equals(searchInput) || employee.getName().equalsIgnoreCase(searchInput)) {
                employee.displayInfo();
                found = true;
            }
        }
        if (!found) {
            System.out.println("Not found information");
        }
    }
}

class C3 {
    public static void main(String[] args) {
        EmployeeManagementSystem ems = new EmployeeManagementSystem();
        ems.excute();
    }
}