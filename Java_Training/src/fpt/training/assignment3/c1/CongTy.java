package fpt.training.assignment3.c1;

public class CongTy {
    public static void main(String[] args) {
        NhanVienSanXuat nhanVienSanXuat = new NhanVienSanXuat("Nguyen Van A", "01/01/1990", 100, 3000000);
        NhanVienVanPhong nhanVienVanPhong = new NhanVienVanPhong("Tran Thi B", "15/05/1985", 20);

        nhanVienSanXuat.tinhLuong();
        nhanVienVanPhong.tinhLuong();

        System.out.println("Thong tin nhan vien san xuat:");
        System.out.println("Ho Ten: " + nhanVienSanXuat.getHoTen());
        System.out.println("Ngay Sinh: " + nhanVienSanXuat.getNgaySinh());
        System.out.println("Luong: " + nhanVienSanXuat.getLuong());

        System.out.println("\nThong tin nhan vien van phong:");
        System.out.println("Ho Ten: " + nhanVienVanPhong.getHoTen());
        System.out.println("Ngay Sinh: " + nhanVienVanPhong.getNgaySinh());
        System.out.println("Luong: " + nhanVienVanPhong.getLuong());
    }
}