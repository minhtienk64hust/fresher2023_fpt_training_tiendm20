package fpt.training.assignment3.c1;

public class NhanVienSanXuat extends NhanVien {
    private int soSanPham;
    private double luongCanBan;

    public NhanVienSanXuat(String hoTen, String ngaySinh, int soSanPham, double luongCanBan) {
        super(hoTen, ngaySinh);
        this.soSanPham = soSanPham;
        this.luongCanBan = luongCanBan;
    }

    public void tinhLuong() {
        double luong = luongCanBan + (soSanPham * 5000);
        setLuong(luong);
    }
}