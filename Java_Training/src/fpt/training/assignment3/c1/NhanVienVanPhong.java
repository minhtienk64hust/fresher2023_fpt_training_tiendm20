package fpt.training.assignment3.c1;

public class NhanVienVanPhong extends NhanVien {
    private int soNgayLamViec;

    public NhanVienVanPhong(String hoTen, String ngaySinh, int soNgayLamViec) {
        super(hoTen, ngaySinh);
        this.soNgayLamViec = soNgayLamViec;
    }

    public void tinhLuong() {
        double luong = 100000 * soNgayLamViec;
        setLuong(luong);
    }
}