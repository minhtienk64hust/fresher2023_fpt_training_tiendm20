package fpt.training.assignment3.c1;

public class NhanVien {
    private String hoTen;
    private String ngaySinh;
    private double luong;

    public NhanVien(String hoTen, String ngaySinh) {
        this.hoTen = hoTen;
        this.ngaySinh = ngaySinh;
    }

    public void setLuong(double luong) {
        this.luong = luong;
    }

    public double getLuong() {
        return luong;
    }

    public String getHoTen() {
        return hoTen;
    }

    public String getNgaySinh() {
        return ngaySinh;
    }
}