package fpt.training.assignment3.c2;

public class KhachHangBinhThuong extends KhachHang {
    public KhachHangBinhThuong(int soLuongHang, double donGiaHang) {
        super(soLuongHang, donGiaHang);
    }

    @Override
    public double tinhTienPhaiTra() {
        double thueVAT = 0.1;
        return (soLuongHang * donGiaHang) * (1 + thueVAT);
    }
}