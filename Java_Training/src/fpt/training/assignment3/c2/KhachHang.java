package fpt.training.assignment3.c2;

public abstract class KhachHang implements TinhTien {
    protected int soLuongHang;
    protected double donGiaHang;

    public KhachHang(int soLuongHang, double donGiaHang) {
        this.soLuongHang = soLuongHang;
        this.donGiaHang = donGiaHang;
    }
}
