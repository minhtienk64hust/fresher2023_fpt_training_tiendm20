package fpt.training.assignment3.c2;

public class KhachHangDacBiet extends KhachHang {
    public KhachHangDacBiet(int soLuongHang, double donGiaHang) {
        super(soLuongHang, donGiaHang);
    }

    @Override
    public double tinhTienPhaiTra() {
        double thueVAT = 0.1;
        return (soLuongHang * donGiaHang * 0.5) * (1 + thueVAT);
    }
}
