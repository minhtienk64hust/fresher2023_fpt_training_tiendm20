package fpt.training.assignment3.c2;

import java.util.ArrayList;
import java.util.List;

public class CongTyXYZ {
    public static void main(String[] args) {
        List<KhachHang> danhSachKhachHang = new ArrayList<>();

        // Thêm các khách hàng vào danh sách
        danhSachKhachHang.add(new KhachHangBinhThuong(10, 100));
        danhSachKhachHang.add(new KhachHangThanThiet(5, 200, 3));
        danhSachKhachHang.add(new KhachHangDacBiet(20, 50));

        double tongTienCtyThuDuoc = 0;

        // Tính tổng số tiền mỗi khách hàng phải trả và tổng số tiền công ty thu được
        for (KhachHang khachHang : danhSachKhachHang) {
            double tienPhaiTra = khachHang.tinhTienPhaiTra();
            System.out.println("Khách hàng phải trả: " + tienPhaiTra);
            tongTienCtyThuDuoc += tienPhaiTra;
        }

        System.out.println("Tổng số tiền công ty thu được: " + tongTienCtyThuDuoc);
    }
}