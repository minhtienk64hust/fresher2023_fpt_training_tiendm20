package fpt.training.assignment3.c2;

public class KhachHangThanThiet extends KhachHang {
    private int soNamThanThiet;

    public KhachHangThanThiet(int soLuongHang, double donGiaHang, int soNamThanThiet) {
        super(soLuongHang, donGiaHang);
        this.soNamThanThiet = soNamThanThiet;
    }

    @Override
    public double tinhTienPhaiTra() {
        double thueVAT = 0.1;
        double phanTramKhuyenMai = Math.max(soNamThanThiet * 0.05, 0.5);
        return (soLuongHang * donGiaHang) * (1 - phanTramKhuyenMai) * (1 + thueVAT);
    }
}