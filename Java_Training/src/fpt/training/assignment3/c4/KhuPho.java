package fpt.training.assignment3.c4;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class KhuPho {
    public static void main(String[] args) {
        List<HoGiaDinh> danhSachHoGiaDinh = new ArrayList<>();

//        HoGiaDinh hoGiaDinh1 = new HoGiaDinh(1);
//        hoGiaDinh1.themNguoi(new Nguoi("Nguyễn Văn A", 25, "Làm ăn", "123456789"));
//        hoGiaDinh1.themNguoi(new Nguoi("Nguyễn Văn B", 26, "Làm công nhân", "987654321"));
//        danhSachHoGiaDinh.add(hoGiaDinh1);
//
//        HoGiaDinh hoGiaDinh2 = new HoGiaDinh(2);
//        hoGiaDinh2.themNguoi(new Nguoi("Nguyễn Văn C", 27, "Làm giáo viên", "543219876"));
//        danhSachHoGiaDinh.add(hoGiaDinh2);

        Scanner sc = new Scanner(System.in);

        System.out.print("Nhập số hộ dân: ");
        int n = sc.nextInt();

        for (int i = 0; i < n; i++) {
            System.out.println("Nhập thông tin hộ dân thứ " + (i + 1) + ": ");

            System.out.print("Nhập thông tin số nhà: ");
            int soNha = sc.nextInt();
            System.out.print("Nhập thông tin số thành viên: ");
            int soThanhVien = sc.nextInt();

            sc.nextLine();

            HoGiaDinh hoGiaDinh = new HoGiaDinh(soNha);

            for (int j = 0; j < soThanhVien; j++) {
                System.out.println("Nhập thông tin người thứ " + (j + 1) + ": ");

                System.out.print("Họ tên: ");
                String hoTen = sc.nextLine();
                System.out.print("Tuổi: ");
                int tuoi = sc.nextInt();
                System.out.print("Nghề nghiệp: ");
                String ngheNghiep = sc.nextLine();
                sc.nextLine();
                System.out.print("Số CMND: ");
                String soCMND = sc.nextLine();

                Nguoi nguoi = new Nguoi(hoTen, tuoi, ngheNghiep, soCMND);
                hoGiaDinh.themNguoi(nguoi);
            }

            danhSachHoGiaDinh.add(hoGiaDinh);
        }

        for (HoGiaDinh hoGiaDinh : danhSachHoGiaDinh) {
            System.out.println(hoGiaDinh);
        }
    }

}