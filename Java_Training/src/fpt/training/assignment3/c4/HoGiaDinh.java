package fpt.training.assignment3.c4;

import java.util.ArrayList;
import java.util.List;

public class HoGiaDinh {
    private int soNha;
    private List<Nguoi> danhSachNguoi;

    public HoGiaDinh(int soNha) {
        this.soNha = soNha;
        danhSachNguoi = new ArrayList<>();
    }

    public int getSoNha() {
        return soNha;
    }

    public void setSoNha(int soNha) {
        this.soNha = soNha;
    }

    public List<Nguoi> getDanhSachNguoi() {
        return danhSachNguoi;
    }

    public void setDanhSachNguoi(List<Nguoi> danhSachNguoi) {
        this.danhSachNguoi = danhSachNguoi;
    }

    public void themNguoi(Nguoi nguoi) {
        danhSachNguoi.add(nguoi);
    }

    @Override
    public String toString() {
        return String.format("Hộ gia đình số nhà %d, có %d người:\n%s", soNha, danhSachNguoi.size(), danhSachNguoi);
    }
}