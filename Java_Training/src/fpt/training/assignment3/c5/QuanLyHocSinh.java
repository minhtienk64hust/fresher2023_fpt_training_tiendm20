package fpt.training.assignment3.c5;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class QuanLyHocSinh {

    public static void main(String[] args) {
        // Khởi tạo danh sách học sinh
        List<HocSinh> danhSachHocSinh = new ArrayList<>();

        // Nhập số lượng học sinh
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập số lượng học sinh: ");
        int n = scanner.nextInt();

        scanner.nextLine(); // Remove enter

        // Nhập thông tin học sinh
        for (int i = 0; i < n; i++) {
            System.out.println("Nhập thông tin học sinh thứ " + (i + 1) + ":");

            // Nhập thông tin lớp học
            System.out.print("Lớp: ");
            String lop = scanner.next();

            scanner.nextLine(); // Remove enter

            // Nhập thông tin học sinh
            System.out.print("Họ tên: ");
            String hoTen = scanner.nextLine();

            System.out.print("Tuổi: ");
            int tuoi = scanner.nextInt();

            scanner.nextLine(); // Remove enter

            System.out.print("Quê quán: ");
            String queQuan = scanner.nextLine();

            // Tạo học sinh mới
            HocSinh hocSinh = new HocSinh(new Lop(lop), hoTen, tuoi, queQuan);

            // Thêm học sinh vào danh sách
            danhSachHocSinh.add(hocSinh);
        }

        // Hiển thị thông tin học sinh
        danhSachHocSinh.stream().forEach(System.out::println);

        // Hiển thị học sinh 20 tuổi
        System.out.println("Danh sách học sinh 20 tuổi:");
        danhSachHocSinh.stream()
                .filter(hocSinh -> hocSinh.getTuoi() == 20)
                .forEach(System.out::println);

        // Hiển thị số lượng học sinh 23 tuổi và quê ở Hà Nội
        System.out.println("Số lượng học sinh 23 tuổi và quê ở Hà Nội: " +
                danhSachHocSinh.stream()
                        .filter(hocSinh -> hocSinh.getTuoi() == 23 && hocSinh.getQueQuan().equals("Hà Nội"))
                        .count());
    }
}