package fpt.training.assignment3.c3;

public abstract class PhongKhachSan implements TinhDoanhThu {
    protected String loaiPhong;
    protected int soDem;

    public PhongKhachSan(String loaiPhong, int soDem) {
        this.loaiPhong = loaiPhong;
        this.soDem = soDem;
    }

    public String getLoaiPhong() {
        return loaiPhong;
    }
}