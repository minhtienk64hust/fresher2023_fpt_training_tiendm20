package fpt.training.assignment3.c3;

public class PhongBusiness extends PhongKhachSan {
    public PhongBusiness(int soDem) {
        super("Business", soDem);
    }

    @Override
    public double tinhDoanhThu() {
        return soDem * 300000;
    }
}