package fpt.training.assignment3.c3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KhachSanSOFITEL {
    public static void main(String[] args) {
        List<ThangDoanhThu> listOfThangDoanhThu = new ArrayList<>();

        ThangDoanhThu thang1 = new ThangDoanhThu("Tháng 1");
        thang1.themPhong(new PhongDeluxe(5, 100000));
        thang1.themPhong(new PhongPremium(7, 150000));
        thang1.themPhong(new PhongBusiness(10));
        listOfThangDoanhThu.add(thang1);

        ThangDoanhThu thang2 = new ThangDoanhThu("Tháng 2");
        thang2.themPhong(new PhongDeluxe(6, 100000));
        thang2.themPhong(new PhongDeluxe(3, 100000));
        thang2.themPhong(new PhongDeluxe(4, 100000));
        thang2.themPhong(new PhongPremium(14, 150000));
        thang2.themPhong(new PhongBusiness(12));
        listOfThangDoanhThu.add(thang2);

        ThangDoanhThu thang3 = new ThangDoanhThu("Tháng 3");
        thang3.themPhong(new PhongDeluxe(7, 100000));
        thang3.themPhong(new PhongPremium(6, 150000));
        thang3.themPhong(new PhongBusiness(10));
        thang3.themPhong(new PhongBusiness(6));
        listOfThangDoanhThu.add(thang3);

        // liệt kê doanh thu các tháng
        for (ThangDoanhThu thang : listOfThangDoanhThu) {
            thang.tinhDoanhThu();
        }

        // tính tổng doanh thu tất cả các tháng theo loại phòng
        Map<String, Double> tongDoanhThuTheoLoaiPhong = new HashMap<>();
        for (ThangDoanhThu thang : listOfThangDoanhThu) {
            for (PhongKhachSan phong : thang.getDanhSachPhong()) {
                double doanhThuPhong = phong.tinhDoanhThu();
                String loaiPhong = phong.getLoaiPhong();
                tongDoanhThuTheoLoaiPhong.put(loaiPhong, tongDoanhThuTheoLoaiPhong.getOrDefault(loaiPhong, 0.0) + doanhThuPhong);
            }
        }

        for (String loaiPhong : tongDoanhThuTheoLoaiPhong.keySet()) {
            double tongDoanhThu = tongDoanhThuTheoLoaiPhong.get(loaiPhong);
            System.out.println("Loại " + loaiPhong + ": " + tongDoanhThu);
        }


        // tìm các phòng có doanh thu vượt trội trong tháng
        for (int i = 1; i < listOfThangDoanhThu.size(); i++) {
            ThangDoanhThu thangHienTai = listOfThangDoanhThu.get(i);
            ThangDoanhThu thangTruoc = listOfThangDoanhThu.get(i - 1);
            Map<String, Double> doanhThuThangTruoc = thangTruoc.tinhDoanhThuTheoLoaiPhong();
            Map<String, Double> doanhThuThangHienTai = thangHienTai.tinhDoanhThuTheoLoaiPhong();
            for (String loaiPhong: doanhThuThangHienTai.keySet()) {
                if (doanhThuThangHienTai.get(loaiPhong) >= doanhThuThangTruoc.get(loaiPhong) * 1.25) {
                    System.out.println("Tháng " + (i+1) + " có loại phòng " + loaiPhong + " có doanh thu vượt trội");
                }
            }

        }

    }
}