package fpt.training.assignment3.c3;

public class PhongDeluxe extends PhongKhachSan {
    private double phiDichVu;

    public PhongDeluxe(int soDem, double phiDichVu) {
        super("Deluxe", soDem);
        this.phiDichVu = phiDichVu;
    }

    @Override
    public double tinhDoanhThu() {
        return (soDem * 750000 + phiDichVu) * 1.05;
    }
}