package fpt.training.assignment3.c3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ThangDoanhThu {
    private String thang;
    private List<PhongKhachSan> danhSachPhong;

    public ThangDoanhThu(String thang) {
        this.thang = thang;
        danhSachPhong = new ArrayList<>();
    }

    public void themPhong(PhongKhachSan phong) {
        danhSachPhong.add(phong);
    }

    public void tinhDoanhThu() {
        double doanhThuThang = 0;
        for (PhongKhachSan phong : danhSachPhong) {
            double doanhThu = phong.tinhDoanhThu();
            doanhThuThang += doanhThu;
        }
        System.out.println("Tổng doanh thu tháng " + thang + ": " + doanhThuThang);
    }


    public Map<String, Double> tinhDoanhThuTheoLoaiPhong() {
        Map<String, Double> doanhThuTheoLoaiPhong = new HashMap<>();
        for (PhongKhachSan phong : danhSachPhong) {
            String loaiPhong = phong.getLoaiPhong();
            double doanhThu = phong.tinhDoanhThu();
            doanhThuTheoLoaiPhong.put(loaiPhong, doanhThuTheoLoaiPhong.getOrDefault(loaiPhong, 0.0) + doanhThu);
        }
        return doanhThuTheoLoaiPhong;
    }

    public List<PhongKhachSan> getDanhSachPhong() {
        return danhSachPhong;
    }

}