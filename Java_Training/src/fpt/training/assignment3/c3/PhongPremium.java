package fpt.training.assignment3.c3;

public class PhongPremium extends PhongKhachSan {
    private double phiDichVu;

    public PhongPremium(int soDem, double phiDichVu) {
        super("Premium", soDem);
        this.phiDichVu = phiDichVu;
    }

    @Override
    public double tinhDoanhThu() {
        return (soDem * 500000 + phiDichVu) * 1.05;
    }
}
