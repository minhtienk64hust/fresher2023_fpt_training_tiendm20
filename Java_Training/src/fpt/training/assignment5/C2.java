package fpt.training.assignment5;

import java.util.Scanner;

class TerminateInputException extends Exception {
    public TerminateInputException(String message) {
        super(message);
    }
}

public class C2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Nhập số lượng phần tử của mảng: ");
        int n = scanner.nextInt();

        int[] arr = new int[n];

        try {
            inputArrayElements(arr, scanner);
        } catch (TerminateInputException e) {
            System.out.println("\nKết thúc chương trình.");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("\nLỗi: Không thể thêm phần tử nữa. Mảng đã đầy.");
        } finally {
            System.out.println("Các phần tử đã nhập:");
            for (int i : arr) {
                if (i == 0) break;
                System.out.print(i + " ");
            }
            scanner.close();
        }
    }

    public static void inputArrayElements(int[] arr, Scanner scanner) throws TerminateInputException {
        int index = 0;
        System.out.println("Nhập các phần tử cho mảng (Nhập 100 để kết thúc):");
        while (index < arr.length) {
            System.out.print("Nhập phần tử thứ " + (index + 1) + ": ");
            int num = scanner.nextInt();

            if (num == 100) {
                throw new TerminateInputException("Nhập giá trị 100. Kết thúc chương trình.");
            }

            arr[index] = num;
            index++;
        }
    }
}