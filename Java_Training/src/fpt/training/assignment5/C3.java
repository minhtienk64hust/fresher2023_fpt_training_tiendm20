package fpt.training.assignment5;

public class C3 {
    public static void main(String[] args) {
        int[] arr = new int[5];

        try {
            // Cố gắng truy cập phần tử thứ 7 (chỉ mục 6)
            int value = arr[6];
            System.out.println("Giá trị phần tử thứ 7: " + value);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Lỗi: Không thể truy cập phần tử ngoài chỉ mục của mảng.");
        }
    }
}
