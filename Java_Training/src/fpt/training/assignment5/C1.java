package fpt.training.assignment5;

import java.util.InputMismatchException;
import java.util.Scanner;

public class C1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        try {
            System.out.print("Nhập số thực thứ nhất: ");
            double num1 = scanner.nextDouble();

            System.out.print("Nhập số thực thứ hai: ");
            double num2 = scanner.nextDouble();

            divide(num1, num2);
        } catch (InputMismatchException e) {
            System.out.println("Lỗi: Vui lòng nhập số thực.");
        } catch (IllegalArgumentException e) {
            System.out.println("Lỗi: " + e.getMessage());
        } finally {
            scanner.close();
        }
    }

    public static void divide(double x, double y) {
        if (y == 0) {
            throw new IllegalArgumentException("Phép chia không hợp lệ (số chia bằng 0). Kết thúc chương trình.");
        }

        double result = x / y;
        System.out.println("Kết quả phép chia: " + result);
    }
}
