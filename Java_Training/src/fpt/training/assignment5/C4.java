package fpt.training.assignment5;

import java.text.NumberFormat;
import java.text.ParseException;

public class C4 {
    public static void main(String[] args) {
        int number = parseNumber("123", Integer.class);
        System.out.println(number); // 123

        long number1 = parseNumber("123l", Long.class);
        System.out.println(number1); // 123

        double number2 = parseNumber("123.456", Double.class);
        System.out.println(number2); // 123.456

        try {
            float number3 = parseNumber("abc", Float.class);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage()); // Chuỗi không phải là số!
        }
    }

    public static <T extends Number> T parseNumber(String str, Class<T> clazz) throws NumberFormatException {

        // Thử ép kiểu chuỗi sang kiểu dữ liệu số học được chỉ định
        T number = null;
        try {
            number = (T) NumberFormat.getInstance().parse(str);
        } catch (ParseException e) {
            throw new NumberFormatException("Chuỗi không phải là số!");
        }

        // Chuyển định dạng về kiểu dữ liệu phù hợp
        if (clazz == Integer.class) {
            number = (T) Integer.valueOf(number.intValue());
        } else if (clazz == Double.class) {
            number = (T) Double.valueOf(number.doubleValue());
        } else if (clazz == Long.class) {
            number = (T) Long.valueOf(number.longValue());
        }

        // Nếu ép kiểu thành công, thì trả về giá trị số học
        if (number != null) {
            return number;
        }

        // Nếu ép kiểu thất bại, thì ném ra ngoại lệ
        throw new NumberFormatException("Chuỗi không phải là số!");
    }
}