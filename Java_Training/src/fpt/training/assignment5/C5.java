package fpt.training.assignment5;

public class C5 {
    public static void main(String[] args) {
        String str = null;

        try {
            handleNullPointerException(str);
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        }

        Person person = null;

        try {
            handleNullPointerException(person);
        } catch (NullPointerException e) {
            System.out.println(e.getMessage()); // Đối tượng person rỗng!
        }
    }

    static class Person {
        private String name;

        public Person(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public static void handleNullPointerException(Object obj) {
        if (obj == null) {
            throw new NullPointerException("Đối tượng rỗng!");
        }
    }
}