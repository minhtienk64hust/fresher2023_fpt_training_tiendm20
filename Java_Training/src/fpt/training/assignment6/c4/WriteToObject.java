package fpt.training.assignment6.c4;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

class NhanVien implements Serializable {
    private String maNV;
    private String hoTen;
    private int tuoi;
    private float luong;

    public NhanVien(String maNV, String hoTen, int tuoi, float luong) {
        this.maNV = maNV;
        this.hoTen = hoTen;
        this.tuoi = tuoi;
        this.luong = luong;
    }

    @Override
    public String toString() {
        return "Mã NV: " + maNV + ", Họ tên: " + hoTen + ", Tuổi: " + tuoi + ", Lương: " + luong;
    }
}

public class WriteToObject {
    public static void main(String[] args) {
        NhanVien[] nhanViens = {
                new NhanVien("NV001", "Nguyen Van A", 30, 1000.0f),
                new NhanVien("NV002", "Nguyen Van B", 35, 1200.0f),
                new NhanVien("NV003", "Nguyen Van C", 28, 900.0f)
        };
        String filePath = "src/fpt/training/assignment6/c4/nhanvien.bin";
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filePath))) {
            oos.writeObject(nhanViens);
            System.out.println("Đã ghi thông tin nhân viên vào file nhanvien.bin.");
        } catch (IOException e) {
            System.err.println("Lỗi khi ghi vào file: " + e.getMessage());
        }
    }
}
