package fpt.training.assignment6.c4;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class WriteToFile {
    public static void main(String[] args) {
        // Ghi thông tin khách hàng vào file nhanvien.txt
        String filePath = "src/fpt/training/assignment6/c4/nhanvien.txt";
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            Scanner scanner = new Scanner(System.in);

            for (int i = 1; i <= 3; i++) {
                System.out.println("Nhập thông tin nhân viên thứ " + i);
                System.out.print("Mã nhân viên: ");
                String maNV = scanner.nextLine();
                System.out.print("Họ tên: ");
                String hoTen = scanner.nextLine();
                System.out.print("Tuổi: ");
                int tuoi = scanner.nextInt();
                System.out.print("Lương: ");
                float luong = scanner.nextFloat();
                scanner.nextLine();

                // Ghi thông tin nhân viên vào file
                writer.write(maNV + " " + hoTen + " " + tuoi + " " + luong + "\n");
            }

            System.out.println("Thông tin nhân viên đã được ghi vào file nhanvien.txt.");
        } catch (IOException e) {
            System.err.println("Lỗi khi ghi vào file: " + e.getMessage());
        }
    }
}

