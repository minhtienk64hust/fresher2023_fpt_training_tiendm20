package fpt.training.assignment6.c4;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class ReadFromObject {
    public static void main(String[] args) {
        String filePath = "src/fpt/training/assignment6/c4/nhanvien.bin";
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filePath))) {
            NhanVien[] nhanViens = (NhanVien[]) ois.readObject();

            for (NhanVien nv : nhanViens) {
                System.out.println(nv);
            }
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Lỗi khi đọc file: " + e.getMessage());
        }
    }
}
