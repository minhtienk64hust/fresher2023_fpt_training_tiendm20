package fpt.training.assignment6;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class C1 {

    public static void main(String[] args) throws IOException {
        String filePath = "src/fpt/training/assignment6/file1";

        // Tạo một Map để lưu trữ các từ và số lần xuất hiện của chúng
        Map<String, Integer> wordCountMap = new HashMap<>();

//        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF-8"));
        String line;
        while ((line = reader.readLine()) != null) {
            String[] words = line.toLowerCase().split("\\s+");
            for (String word : words) {
                wordCountMap.put(word, wordCountMap.getOrDefault(word, 0) + 1);
            }
        }

        // In ra số lượng từ
        System.out.println("Số lượng từ trong file1 là: " + wordCountMap.size());

        // In ra số lượng mỗi từ đếm được
        for (Map.Entry<String, Integer> entry : wordCountMap.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }
}
