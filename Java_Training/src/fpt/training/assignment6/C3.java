package fpt.training.assignment6;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class C3 {
    private static Student findStudentByMaSV(String maSV) {
        String filePath = "src/fpt/training/assignment6/file3.txt";
        Student foundStudent = null;

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(" ");
                if (parts.length == 3) {
                    String maSinhVien = parts[0];
                    String hoTen = parts[1];
                    double diemTrungBinh = Double.parseDouble(parts[2]);

                    if (maSinhVien.equals(maSV)) {
                        foundStudent = new Student(maSinhVien, hoTen, diemTrungBinh);
                        break;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return foundStudent;
    }

    public static void main(String[] args) {
//        String maSinhVienToSearch = "NghiaTT25";  // Mã sinh viên cần tìm

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Nhập mã sinh viên: ");
        String maSinhVienToSearch = null;
        try {
            maSinhVienToSearch = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        Student student = findStudentByMaSV(maSinhVienToSearch);

        if (student != null) {
            System.out.println(student.toString());
        } else {
            System.out.println("Không tìm thấy sinh viên có mã: " + maSinhVienToSearch);
        }
    }
}

class Student {
    private String maSinhVien;
    private String hoTen;
    private double diemTrungBinh;

    public Student(String maSinhVien, String hoTen, double diemTrungBinh) {
        this.maSinhVien = maSinhVien;
        this.hoTen = hoTen;
        this.diemTrungBinh = diemTrungBinh;
    }

    @Override
    public String toString() {
        return maSinhVien + " " + hoTen + " " + diemTrungBinh;
    }
}
