package fpt.training.assignment6;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class C2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Nhập số lượng khách hàng: ");
        int n = scanner.nextInt();
        scanner.nextLine();
        List<Customer> customers = new ArrayList<>();
        // Nhập thông tin cho từng khách hàng
        for (int i = 0; i < n; i++) {
            System.out.println("Nhập thông tin cho khách hàng thứ " + (i + 1));
            System.out.print("Nhập mã khách hàng: ");
            String maKhachHang = scanner.nextLine();
            System.out.print("Nhập họ và tên: ");
            String hoTen = scanner.nextLine();
            System.out.print("Nhập số điện thoại: ");
            String soDienThoai = scanner.nextLine();

            // Tạo đối tượng Customer từ thông tin nhập vào và thêm vào danh sách
            Customer customer = new Customer(maKhachHang, hoTen, soDienThoai);
            customers.add(customer);
        }

        // Ghi thông tin khách hàng vào file file2.txt
        String filePath = "src/fpt/training/assignment6/file2.txt";
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath, false))) {
            for (Customer customer : customers) {
                writer.write(customer.toString() + "\n");
            }
            System.out.println("Thông tin khách hàng đã được ghi vào file file2.txt.");
        } catch (IOException e) {
            System.err.println("Lỗi khi ghi vào file: " + e.getMessage());
        }
    }
}

class Customer {
    private String maKhachHang;
    private String hoTen;
    private String soDienThoai;

    public Customer(String maKhachHang, String hoTen, String soDienThoai) {
        this.maKhachHang = maKhachHang;
        this.hoTen = hoTen;
        this.soDienThoai = soDienThoai;
    }

    @Override
    public String toString() {
        return maKhachHang + " " + hoTen + " " + soDienThoai;
    }
}