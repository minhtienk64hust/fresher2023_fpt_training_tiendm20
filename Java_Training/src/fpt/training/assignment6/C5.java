package fpt.training.assignment6;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class SinhVien implements Serializable {
    private String maSV;
    private String hoTen;
    private int tuoi;
    private String diaChi;
    private double diemTB;


    public SinhVien(String maSV, String hoTen, int tuoi, String diaChi, double diemTB) {
        this.maSV = maSV;
        this.hoTen = hoTen;
        this.tuoi = tuoi;
        this.diaChi = diaChi;
        this.diemTB = diemTB;
    }

    public String getMaSV() {
        return maSV;
    }

    public void setMaSV(String maSV) {
        this.maSV = maSV;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public int getTuoi() {
        return tuoi;
    }

    public void setTuoi(int tuoi) {
        this.tuoi = tuoi;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public double getDiemTB() {
        return diemTB;
    }

    public void setDiemTB(float diemTB) {
        this.diemTB = diemTB;
    }

    @Override
    public String toString() {
        return maSV + " " + hoTen + " " + tuoi + " " + diaChi + " " + diemTB;
    }
}

public class C5 {
    private static List<SinhVien> danhSachSinhVien = new ArrayList<>();
    private static final String FILE_PATH = "src/fpt/training/assignment6/file5.txt";
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) {
        loadDataFromFile();

        int choice = -1;
        do {
            displayMenu();
            try {
                choice = Integer.parseInt(reader.readLine());
            } catch (NumberFormatException | IOException e) {
                System.err.println("Lựa chọn không hợp lệ. Vui lòng chọn lại.");
                continue;
            }

            switch (choice) {
                case 1:
                    themSinhVien();
                    break;
                case 2:
                    suaSinhVien();
                    break;
                case 3:
                    xoaSinhVien();
                    break;
                case 4:
                    sapXepSinhVien(true);
                    break;
                case 5:
                    sapXepSinhVien(false);
                    break;
                case 6:
                    hienThiDanhSachSinhVien();
                    break;
                case 0:
                    System.out.println("Thoát chương trình.");
                    break;
                default:
                    System.out.println("Lựa chọn không hợp lệ. Vui lòng chọn lại.");
            }
        } while (choice != 0);

        saveDataToFile();
    }

    private static void displayMenu() {
        System.out.println("\n===== Menu =====");
        System.out.println("1. Thêm sinh viên.");
        System.out.println("2. Sửa sinh viên theo mã.");
        System.out.println("3. Xóa sinh viên theo mã.");
        System.out.println("4. Sắp xếp sinh viên theo họ tên.");
        System.out.println("5. Sắp xếp sinh viên theo điểm trung bình.");
        System.out.println("6. Hiển thị thông tin tất cả sinh viên.");
        System.out.println("0. Thoát chương trình.");
        System.out.print("Chọn: ");
    }

    private static void loadDataFromFile() {
        try (BufferedReader reader = new BufferedReader(new FileReader(FILE_PATH))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(" ");
                if (parts.length == 5) {
                    String maSinhVien = parts[0];
                    String hoTen = parts[1];
                    int tuoi = Integer.parseInt(parts[2]);
                    String diaChi = parts[3];
                    double diemTrungBinh = Double.parseDouble(parts[4]);
                    SinhVien sv = new SinhVien(maSinhVien, hoTen, tuoi, diaChi, diemTrungBinh);
                    danhSachSinhVien.add(sv);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void saveDataToFile() {
        try (FileWriter writer = new FileWriter(FILE_PATH)) {
            for (SinhVien sv: danhSachSinhVien) {
                writer.write(sv.toString());
                writer.write("\n");
            }
            System.out.println("Dữ liệu đã được lưu vào file " + FILE_PATH);
        } catch (IOException e) {
            System.err.println("Không thể ghi vào file: " + e.getMessage());
        }
    }

    private static void themSinhVien() {
        try {
            System.out.print("Nhập mã sinh viên: ");
            String maSV = reader.readLine();

            System.out.print("Nhập họ tên: ");
            String hoTen = reader.readLine();

            System.out.print("Nhập tuổi: ");
            int tuoi = Integer.parseInt(reader.readLine());

            System.out.print("Nhập địa chỉ: ");
            String diaChi = reader.readLine();

            System.out.print("Nhập điểm trung bình: ");
            float diemTB = Float.parseFloat(reader.readLine());

            SinhVien sinhVien = new SinhVien(maSV, hoTen, tuoi, diaChi, diemTB);
            danhSachSinhVien.add(sinhVien);
            System.out.println("Sinh viên đã được thêm.");
        } catch (IOException | NumberFormatException e) {
            System.err.println("Lỗi khi đọc dữ liệu: " + e.getMessage());
        }
    }

    private static void suaSinhVien() {
        try {
            System.out.print("Nhập mã sinh viên cần sửa: ");
            String maSV = reader.readLine();

            SinhVien sinhVien = timSinhVienTheoMa(maSV);
            if (sinhVien != null) {
                System.out.print("Nhập họ tên mới: ");
                String hoTenMoi = reader.readLine();
                sinhVien.setHoTen(hoTenMoi);

                System.out.print("Nhập tuổi mới: ");
                int tuoiMoi = Integer.parseInt(reader.readLine());
                sinhVien.setTuoi(tuoiMoi);

                System.out.print("Nhập địa chỉ mới: ");
                String diaChiMoi = reader.readLine();
                sinhVien.setDiaChi(diaChiMoi);

                System.out.print("Nhập điểm trung bình mới: ");
                float diemTBMoi = Float.parseFloat(reader.readLine());
                sinhVien.setDiemTB(diemTBMoi);

                System.out.println("Sinh viên đã được sửa.");
            } else {
                System.out.println("Không tìm thấy sinh viên có mã " + maSV);
            }
        } catch (IOException | NumberFormatException e) {
            System.err.println("Lỗi khi đọc dữ liệu: " + e.getMessage());
        }
    }

    private static void xoaSinhVien() {
        try {
            System.out.print("Nhập mã sinh viên cần xóa: ");
            String maSV = reader.readLine();

            SinhVien sinhVien = timSinhVienTheoMa(maSV);
            if (sinhVien != null) {
                danhSachSinhVien.remove(sinhVien);
                System.out.println("Sinh viên đã được xóa.");
            } else {
                System.out.println("Không tìm thấy sinh viên có mã " + maSV);
            }
        } catch (IOException e) {
            System.err.println("Lỗi khi đọc dữ liệu: " + e.getMessage());
        }
    }

    private static SinhVien timSinhVienTheoMa(String maSV) {
        for (SinhVien sinhVien : danhSachSinhVien) {
            if (sinhVien.getMaSV().equals(maSV)) {
                return sinhVien;
            }
        }
        return null;
    }

    private static void sapXepSinhVien(boolean sortByHoTen) {
        Collections.sort(danhSachSinhVien, new Comparator<SinhVien>() {
            @Override
            public int compare(SinhVien sv1, SinhVien sv2) {
                if (sortByHoTen) {
                    return sv1.getHoTen().compareTo(sv2.getHoTen());
                } else {
                    return Double.compare(sv1.getDiemTB(), sv2.getDiemTB());
                }
            }
        });
    }

    private static void hienThiDanhSachSinhVien() {
        if (danhSachSinhVien.isEmpty()) {
            System.out.println("Danh sách sinh viên trống.");
            return;
        }

        System.out.println("Danh sách sinh viên:");
        for (SinhVien sinhVien : danhSachSinhVien) {
            System.out.println(sinhVien.toString());
        }
    }
}
