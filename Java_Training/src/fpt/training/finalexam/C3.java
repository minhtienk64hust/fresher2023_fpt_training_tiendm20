package fpt.training.finalexam;

import java.util.Arrays;
import java.util.Scanner;

public class C3 {

    public static int[] sortArrayAndInsert(int[] arr, int x) {
        // ascending order - increase order
        Arrays.sort(arr);

        int insertPosition = findInsertPosition(arr, x);
        int[] newArray = insertElement(arr, x, insertPosition);

       return newArray;
    }

    public static void display(int[] arr) {
        System.out.println(Arrays.toString(arr));
    }

    public static int findInsertPosition(int[] arr, int x) {
        int i = 0;
        while (i < arr.length && arr[i] < x) {
            i++;
        }
        return i;
    }

    public static int[] insertElement(int[] arr, int x, int insertPosition) {
        int[] newArray = new int[arr.length + 1];
        int i = 0;
        int j = 0;

        while (i < arr.length) {
            if (j == insertPosition) {
                newArray[j] = x;
                j++;
            }
            newArray[j] = arr[i];
            i++;
            j++;
        }

        // If the insert position is at the end, insert the new element here
        if (j == insertPosition) {
            newArray[j] = x;
        }

        return newArray;
    }

    public static void main(String[] args) {
        int[] arr;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input size of array: ");
        int size = scanner.nextInt();
        arr = new int[size];

        System.out.println("Input element:");
        for (int i = 0; i < size; i++) {
            System.out.print("Element-" + (i + 1) + ": ");
            arr[i] = scanner.nextInt();
        }

        System.out.print("input = ");
        display(arr);

        int x;
        System.out.print("Input element to be insert: ");
        x = scanner.nextInt();
        System.out.println("The element to be inserted is: " + x);

        arr = sortArrayAndInsert(arr, x);
        System.out.print("after doing the sort = ");
        System.out.print("output = ");
        display(arr);
    }
}