package fpt.training.finalexam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class C1 {

    // ý a)
    public static int calculateDigitSum(int n) {
        int digitSum = 0;
        while (n > 0) {
            digitSum += n % 10;
            n /= 10;
        }
        return digitSum;
    }

    // ý b)
    public static void primeFactorization(int n) {
        System.out.print(n + " = ");

        if (n < 2) {
            System.out.println("Invalid input, please enter a positive integer greater than or equal to 2.");
            return;
        }

        Map<Integer, Integer> factors = new HashMap<>();
        int divisor = 2;

        while (n > 1) {
            if (n % divisor == 0) {
                n /= divisor;
                factors.put(divisor, factors.getOrDefault(divisor, 0) + 1);
            } else {
                divisor++;
            }
        }

        boolean first = true;

        for (Map.Entry<Integer, Integer> entry : factors.entrySet()) {
            int factor = entry.getKey();
            int count = entry.getValue();

            if (!first) {
                System.out.print(" * ");
            }

            if (count > 1) {
                System.out.print(factor + "^" + count);
            } else {
                System.out.print(factor);
            }

            first = false;
        }
        System.out.println();
    }

    // ý c)
    public static ArrayList<Integer> listDivisors(int n) {
        ArrayList<Integer> divisors = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            if (n % i == 0) {
                divisors.add(i);
            }
        }
        return divisors;
    }

    // ý d)
    public static ArrayList<Integer> listPrimeDivisors(int n) {
        ArrayList<Integer> primeDivisors = new ArrayList<>();
        for (int i = 2; i <= n; i++) {
            if (n % i == 0 && isPrime(i)) {
                primeDivisors.add(i);
            }
        }
        return primeDivisors;
    }

    public static boolean isPrime(int num) {
        if (num < 2) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n;
        while (true) {
            System.out.println("Enter a positive integer:");
            n = scanner.nextInt();
            if (n > 0) {
                break;
            } else {
                System.out.println("Invalid input. Please enter a positive integer.");
            }
        }

        int digitSum = calculateDigitSum(n);
        System.out.println("Sum of digits: " + digitSum);

        primeFactorization(n);

        ArrayList<Integer> divisors = listDivisors(n);
        System.out.println("Divisors: " + divisors);

        ArrayList<Integer> primeDivisors = listPrimeDivisors(n);
        System.out.println("Prime divisors: " + primeDivisors);
    }
}