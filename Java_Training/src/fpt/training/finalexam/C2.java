package fpt.training.finalexam;

import java.util.ArrayList;
import java.util.List;

public class C2 {

    private static final int MIN_LENGTH = 5;
    private static final int MAX_LENGTH = 7;
    private static final int[] PRIME_DIGITS = {2, 3, 5, 7};

    public static void main(String[] args) {
        List<Integer> palindromePrimes = new ArrayList<>();
        for (int i = MIN_LENGTH; i <= MAX_LENGTH; i++) {
            int start = (int) Math.pow(10, i - 1);
            int end = (int) Math.pow(10, i);
            for (int j = start; j < end; j++) {
                if (isPalindrome(j) && isPrime(j) && hasPrimeDigits(j) && hasPrimeDigitSum(j)) {
                    palindromePrimes.add(j);
                }
            }
        }

        System.out.println("Palindrome primes with 5 to 7 digits:");
        for (Integer palindromePrime : palindromePrimes) {
            System.out.println(palindromePrime);
        }
    }

    private static boolean isPalindrome(int number) {
        String numberString = String.valueOf(number);
        int length = numberString.length();
        for (int i = 0; i < length / 2; i++) {
            if (numberString.charAt(i) != numberString.charAt(length - i - 1)) {
                return false;
            }
        }
        return true;
    }

    private static boolean isPrime(int number) {
        if (number < 2) {
            return false;
        }
        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    private static boolean hasPrimeDigits(int number) {
        while (number > 0) {
            int digit = number % 10;
            boolean isPrimeDigit = false;
            for (int primeDigit : PRIME_DIGITS) {
                if (digit == primeDigit) {
                    isPrimeDigit = true;
                    break;
                }
            }
            if (!isPrimeDigit) {
                return false;
            }
            number /= 10;
        }
        return true;
    }

    private static boolean hasPrimeDigitSum(int number) {
        int digitSum = 0;
        while (number > 0) {
            digitSum += number % 10;
            number /= 10;
        }
        return isPrime(digitSum);
    }
}