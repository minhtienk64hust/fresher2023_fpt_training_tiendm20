package fpt.training.finalexam.c4.validation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Validate {
    private BufferedReader br;

    public Validate() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    public int checkIntLimit(int min, int max) {
        while (true) {
            try {
                int result = Integer.parseInt(br.readLine().trim());
                if (result < min || result > max) {
                    System.out.println("Please input number in range: " + min + " to " + max);
                } else {
                    return result;
                }
            } catch (IOException e) {
                System.out.println("An error occurred while reading user input: " + e.getMessage());
            } catch (NumberFormatException e) {
                System.out.println("Please input number in range: " + min + " to " + max);
            }
        }
    }

    public String checkInputName() {
        while (true) {
            try {
                String name = br.readLine().trim();
                String regex = "^[a-zA-Z ]+$";
                if (name.matches(regex)) {
                    return name;
                } else {
                    System.out.println("Please input a valid name!");
                }
            } catch (IOException e) {
                System.out.println("An error occurred while reading user input: " + e.getMessage());
            }
        }
    }

    public String checkInputPhoneNumber() {
        while (true) {
            try {
                String phoneNumber = br.readLine().trim();

                // Regex pattern for a valid phone number (digits only, at least 10 digits)
                String regex = "^[0-9]{9,11}$";

                if (phoneNumber.matches(regex)) {
                    return phoneNumber;
                } else {
                    System.out.println("Please input a valid phone number (digits only, at least 10 digits)!");
                }
            } catch (IOException e) {
                System.out.println("An error occurred while reading user input: " + e.getMessage());
            }
        }
    }


}
