package fpt.training.finalexam.c4.view;

import fpt.training.finalexam.c4.model.Contact;
import fpt.training.finalexam.c4.validation.Validate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Management {
    private ArrayList<Contact> contacts;
    Validate validate;

    public Management() {
        this.contacts = new ArrayList<>();
        validate = new Validate();
    }

    public void displayMenu() {
        System.out.println("1.  Add contact");
        System.out.println("2.  Edit contact");
        System.out.println("3.  Search");
        System.out.println("4.  Display information contact");
        System.out.println("5.  Sort contact");
        System.out.println("6.  Exit");
        System.out.println("Enter your choice: ");
        int choice = validate.checkIntLimit(1, 6);
        if (choice == 1) {
            addContact();
        } else if (choice == 2) {
            editContact();
        } else if (choice == 3) {
            searchContact();
        } else if (choice == 4) {
            displayInfo();
        } else if (choice == 5) {
            sortContacts();
        } else if (choice == 6) {
            System.exit(0);
        }  else {
            System.out.println("Please input from 1 to 5!");
        }
    }

    private boolean processCheckPhoneNumberExisted(String phoneNumber) {
        for (Contact contact: contacts) {
            if (contact.getPhoneNumber().equals(phoneNumber)) {
                return true;
            }
        }
        return false;
    }

    public void addContact() {
        System.out.println("Enter phone number: ");
        String phoneNumber = validate.checkInputPhoneNumber();
        while (processCheckPhoneNumberExisted(phoneNumber)) {
            System.out.println("Contact already exists!");
            System.out.println("Input again: ");
            phoneNumber = validate.checkInputPhoneNumber();
        }

        System.out.println("Enter name: ");
        String name = validate.checkInputName();

        processAddContact(name, phoneNumber);
    }

    private void processAddContact(String name, String phoneNumber) {
        Contact newContact = new Contact(name, phoneNumber);
        contacts.add(newContact);
        System.out.println("Contact added successfully");
    }

    public void displayInfo() {
        if (contacts.isEmpty()) {
            System.out.println("List is empty!");
        } else {
            System.out.println(
                    "--------------------------------------------------Display imformation--------------------------------------------------");
            System.out.printf("%-15s%-15s\n", "Name", "Phone number");
            for (Contact contact: contacts) {
                contact.displayInfo();
            }
        }

    }

    public void editContact() {
        System.out.println("Enter old phone number to find and edit: ");
        String oldPhoneNumber = validate.checkInputPhoneNumber();
        while (!processCheckPhoneNumberExisted(oldPhoneNumber)) {
            System.out.println("Contact not existed!");
            System.out.println("Input again: ");
            oldPhoneNumber = validate.checkInputPhoneNumber();
        }

        System.out.println("Enter new phone number: ");
        String newPhoneNumber = validate.checkInputPhoneNumber();
        while (processCheckPhoneNumberExisted(newPhoneNumber)) {
            System.out.println("Contact already exists!");
            System.out.println("Input again: ");
            newPhoneNumber = validate.checkInputPhoneNumber();
        }

        System.out.println("Enter name: ");
        String newName = validate.checkInputName();
        processEditContact(newName, oldPhoneNumber, newPhoneNumber);
    }

    private void processEditContact(String newName, String oldPhoneNumber, String newPhoneNumber) {
        for (Contact contact : contacts) {
            if (contact.getPhoneNumber().equals(oldPhoneNumber)) {
                contact.setPhoneNumber(newPhoneNumber);
                contact.setName(newName);
                System.out.println("Contact updated successfully.");
                return;
            }
        }
        System.out.println("Edit contact successfully");
    }

    public void searchContact() {
        System.out.println("Enter name to search: ");
        String searchString = validate.checkInputName();
        processSearchContact(searchString);
    }

    private void processSearchContact(String searchString) {
        List<Contact> foundContacts = new ArrayList<>();
        for (Contact contact : contacts) {
            if (contact.getName().toLowerCase().contains(searchString.toLowerCase())) {
                foundContacts.add(contact);
            }
        }
        if (foundContacts.isEmpty()) {
            System.out.println("Not found contact.");
            return;
        }
        System.out.println(
                "--------------------------------------------------Contact founded--------------------------------------------------");
        for(Contact contact: foundContacts) {
            contact.displayInfo();
        }
    }

    public void sortContacts(){
        processSortContacts();
    }

    private void processSortContacts(){
        Collections.sort(contacts, new Comparator<Contact>() {
            @Override
            public int compare(Contact contact1, Contact contact2) {
                return contact1.getName().compareTo(contact2.getName());
            }
        });
        System.out.println("Sort contact alphabetically successfully!");
    }

    public static void main(String[] args) {
        Management mn = new Management();
        do {
            mn.displayMenu();
        } while (true);
    }
}
