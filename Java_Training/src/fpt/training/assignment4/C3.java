package fpt.training.assignment4;

import java.util.Random;

class Data {
    private int total;

    public Data() {
        total = 0;
    }

    public int getTotal() {
        return total;
    }

    public void addToTotal(int value) {
        total += value;

        System.out.println("Tổng = " + total);
    }
}

class NegativeNumberThread extends Thread {
    private Data data;

    public NegativeNumberThread(Data data) {
        this.data = data;
    }

    @Override
    public void run() {
        while (true) {
            // Sinh ngẫu nhiên một số từ -100 đến 0
            int random = (int) (Math.random() * (-100 + 1));
            System.out.println("NegativeNumberThread: " + random);
            // Thêm số ngẫu nhiên vào total
            data.addToTotal(random);

            // Kiểm tra tổng
            if (data.getTotal() >= 100 || data.getTotal() <= -100) {
                // Dừng thread
                break;
            }
        }
    }
}

class PositiveNumberThread extends Thread {
    private Data data;

    public PositiveNumberThread(Data data) {
        this.data = data;
    }

    @Override
    public void run() {
        while (true) {
            // Sinh ngẫu nhiên một số từ 0 đến 100
            int random = (int) (Math.random() * (100 + 1));
            System.out.println("PositiveNumberThread: " + random);
            // Thêm số ngẫu nhiên vào total
            data.addToTotal(random);

            // Kiểm tra tổng
            if (data.getTotal() >= 100 || data.getTotal() <= -100) {
                // Dừng thread
                break;
            }
        }
    }
}

public class C3 {
    public static void main(String[] args) {
        Data data = new Data();

        NegativeNumberThread negativeNumberThread = new NegativeNumberThread(data);
        PositiveNumberThread positiveNumberThread = new PositiveNumberThread(data);

        negativeNumberThread.start();
        positiveNumberThread.start();
    }
}
