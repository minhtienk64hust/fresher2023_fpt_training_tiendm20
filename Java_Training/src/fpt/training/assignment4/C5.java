package fpt.training.assignment4;

import java.util.Random;

class NameThread extends Thread {
    private static String[] names = {"Nguyen Van Huan", "Nguyen Linh Duc", "Nguyen Van Tan", "Vu Viet Tung", "Tran Trung Nghia"};
    private Random random = new Random();

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            // Chọn ngẫu nhiên một người trong danh sách tên
            String randomName = names[random.nextInt(names.length)];
            System.out.println("Thread 1: " + randomName);

            synchronized (PlaceThread.class) {
                PlaceThread.setRandomName(randomName);
                PlaceThread.class.notify(); // Notify PlaceThread
            }

            try {
                Thread.sleep(1000); // Chờ 1 giây
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class PlaceThread extends Thread {
    private static String[] names = {"Nguyen Van Huan", "Nguyen Linh Duc", "Nguyen Van Tan", "Vu Viet Tung", "Tran Trung Nghia"};
    private static String[] places = {"Ha Noi", "Hung Yen", "Thanh Hoa", "Ha Tinh", "Quang Ninh"};

    private static String randomName;

    public static void setRandomName(String name) {
        randomName = name;
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            synchronized (PlaceThread.class) {
                try {
                    PlaceThread.class.wait(); // Chờ nhận thông báo từ NameThread
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            // Tìm quê quán tương ứng với người đã chọn
            int index = -1;
            for (int j = 0; j < names.length; j++) {
                if (names[j].equals(randomName)) {
                    index = j;
                    break;
                }
            }

            // Hiển thị quê quán
            if (index != -1 && index < places.length) {
                System.out.println("Thread 2: Quê quán của " + randomName + " là " + places[index]);
            }

            try {
                Thread.sleep(1000); // Chờ 1 giây
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            synchronized (NameThread.class) {
                NameThread.class.notify(); // Báo cho NameThread chạy tiếp
            }
        }
    }
}

public class C5 {
    public static void main(String[] args) {
        NameThread nameThread = new NameThread();
        PlaceThread placeThread = new PlaceThread();

        nameThread.start();
        placeThread.start();
    }
}
