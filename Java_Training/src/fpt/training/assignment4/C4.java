package fpt.training.assignment4;

import java.util.ArrayList;
import java.util.Random;

class Thread1 extends Thread {
    public ArrayList<Integer> list1;

    public Thread1() {
        list1 = new ArrayList<>();
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            int random = (int) (Math.random() * 100);
            System.out.println("Thread 1: " + random);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            list1.add(random);
        }
    }
}

class Thread2 extends Thread {
    public ArrayList<Character> list2;

    public Thread2() {
        list2 = new ArrayList<>();
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            char random = (char) ('a' + (int) (Math.random() * 26));
            System.out.println("Thread 2: " + random);

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            list2.add(random);
        }
    }
}

public class C4 {
    public static void main(String[] args) {
        Thread1 thread1 = new Thread1();
        Thread2 thread2 = new Thread2();

        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // In kết quả của thread1
        System.out.println("Kết quả của thread1: " + thread1.list1);

        // In kết quả của thread2
        System.out.println("Kết quả của thread2: " + thread2.list2);
    }
}
