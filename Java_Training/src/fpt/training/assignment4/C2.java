package fpt.training.assignment4;

import java.util.Random;

class RandomLowerCaseThread extends Thread {
    private Random random = new Random();

    @Override
    public void run() {
        long startTime = System.currentTimeMillis();
        while (true) {
            char ch = (char) ('a' + random.nextInt(26));
            System.out.println("Thread 1: Random kí tự: " + ch);

            // Đồng bộ với thread 2
            synchronized (UpperCaseThread.class) {
                UpperCaseThread.setChar(ch);
                UpperCaseThread.class.notify();  // Notify UpperCaseThread to convert to upper case
            }

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            long currentTime = System.currentTimeMillis();
            if (currentTime - startTime > 20000) {
                // Dừng hai thread
                System.out.println("Dừng hai thread sau 20s");
                RandomLowerCaseThread.class.notifyAll();
                UpperCaseThread.class.notifyAll();
                return;
            }
        }
    }
}

class UpperCaseThread extends Thread {
    private static char ch;

    public static void setChar(char c) {
        ch = c;
    }

    @Override
    public void run() {
        long startTime = System.currentTimeMillis();
        while (true) {
            synchronized (UpperCaseThread.class) {
                try {
                    UpperCaseThread.class.wait();  // Wait for new char to be set
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                char upperCaseChar = (char) ('A' + (ch - 'a'));

                System.out.println("Thread 2: Ký tự hoa là: " + upperCaseChar);
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            long currentTime = System.currentTimeMillis();
            if (currentTime - startTime > 20000) {
                // Dừng hai thread
                System.out.println("Dừng hai thread sau 20s");
                RandomLowerCaseThread.class.notifyAll();
                UpperCaseThread.class.notifyAll();
                return;
            }
        }
    }
}

public class C2 {
    public static void main(String[] args) {
        RandomLowerCaseThread randomLowerCaseThread = new RandomLowerCaseThread();
        UpperCaseThread upperCaseThread = new UpperCaseThread();

        randomLowerCaseThread.start();
        upperCaseThread.start();
    }
}
