package fpt.training.assignment4;

import java.util.Random;

class RandomNumberThread extends Thread {
    private Random random = new Random();

    @Override
    public void run() {
        while (true) {
            int randomNumber = random.nextInt(20) + 1;
            System.out.println("Thread 1: Random number generated: " + randomNumber);

            synchronized (SquareThread.class) {
                SquareThread.setNumber(randomNumber);
                SquareThread.class.notify();  // Notify SquareThread to calculate square
            }

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class SquareThread extends Thread {
    private static int number;

    public static void setNumber(int num) {
        number = num;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (SquareThread.class) {
                try {
                    SquareThread.class.wait();  // Wait for new number to be set
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                int square = number * number;
                System.out.println("Thread 2: Square of " + number + " is: " + square);
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

public class C1 {
    public static void main(String[] args) {
        RandomNumberThread randomNumberThread = new RandomNumberThread();
        SquareThread squareThread = new SquareThread();

        randomNumberThread.start();
        squareThread.start();
    }
}
