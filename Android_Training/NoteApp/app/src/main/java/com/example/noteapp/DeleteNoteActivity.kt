package com.example.noteapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.noteapp.databinding.ActivityAddNoteBinding
import com.example.noteapp.databinding.ActivityDeleteNoteBinding
import com.example.noteapp.databinding.ActivityUpdateNoteBinding

class DeleteNoteActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDeleteNoteBinding
    private val noteDatabase = NoteDatabaseHelper(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDeleteNoteBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.btnDelete.setOnClickListener {
            val id = binding.edtDeletedId.text.toString().toLong()
            val deletedRows = noteDatabase.deletedNoteById(id)

            finish()
        }
    }
}