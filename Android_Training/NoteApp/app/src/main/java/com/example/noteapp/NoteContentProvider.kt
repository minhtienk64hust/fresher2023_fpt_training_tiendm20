package com.example.noteapp

import android.content.ContentProvider
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.net.Uri
import android.util.Log

class NoteContentProvider : ContentProvider() {
    private lateinit var noteDataBase: NoteDatabaseHelper

    companion object {
        private const val PATH_NOTES = "notes"
        private const val AUTHORITY = "com.example.noteapp.provider"
        private const val NOTES_URI = "content://$AUTHORITY/$PATH_NOTES"
        private const val NOTES = 1

        private val uriMatcher = UriMatcher(UriMatcher.NO_MATCH)

        init {
            uriMatcher.addURI(AUTHORITY, PATH_NOTES, NOTES)
        }
    }
    override fun onCreate(): Boolean {
        noteDataBase = NoteDatabaseHelper(context!!)
        return true
    }

    override fun query(uri: Uri, projection: Array<out String>?, selection: String?, selectionArgs: Array<out String>?, sortOrder: String?): Cursor? {
        val db = noteDataBase.readableDatabase
        val cursor: Cursor?
        when (uriMatcher.match(uri)) {
            NOTES -> {
                cursor = db.query("notes", projection, selection, selectionArgs, null, null, sortOrder)
            }
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }

        cursor.setNotificationUri(context!!.contentResolver, uri)
        return cursor
    }

    override fun getType(uri: Uri): String? {
        return null
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        val db = noteDataBase.writableDatabase
        val id = db.insert("notes", null, values)
        return Uri.parse("$NOTES_URI/$id")
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<out String>?): Int {
        val db = noteDataBase.writableDatabase
        return when (uriMatcher.match(uri)) {
            NOTES -> db.delete("notes", selection, selectionArgs)
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
    }

    override fun update(uri: Uri, values: ContentValues?, selection: String?, selectionArgs: Array<out String>?): Int {
        val db = noteDataBase.writableDatabase
        return when (uriMatcher.match(uri)) {
            NOTES -> db.update("notes", values, selection, selectionArgs)
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
    }
}