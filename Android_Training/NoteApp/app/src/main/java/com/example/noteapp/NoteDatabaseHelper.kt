package com.example.noteapp

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class NoteDatabaseHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private const val DATABASE_NAME = "note.db"
        private const val DATABASE_VERSION = 1

        private const val TABLE_NOTE = "note"
        private const val COLUMN_ID = "id"
        const val COLUMN_TITLE = "title"
        const val COLUMN_CONTENT = "content"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val createTable = ("CREATE TABLE $TABLE_NOTE ($COLUMN_ID INTEGER PRIMARY KEY AUTOINCREMENT, $COLUMN_TITLE TEXT, $COLUMN_CONTENT TEXT)")

        db?.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        val dropTable = ("DROP TABLE IF EXISTS $TABLE_NOTE")
        db?.execSQL(dropTable)
    }

    fun addNote(title: String?, content: String?): Long {
        val values = ContentValues()
        values.put(COLUMN_TITLE, title)
        values.put(COLUMN_CONTENT, content)
        val db = this.writableDatabase
        val id = db.insert(TABLE_NOTE, null, values)
        db.close()
        return id
    }

    fun updateNoteById(id: Long, title: String?, content: String?): Int {
        val values = ContentValues()
        values.put(COLUMN_TITLE, title)
        values.put(COLUMN_CONTENT, content)
        val db = this.writableDatabase
        val updatedRows = db.update(TABLE_NOTE, values, "$COLUMN_ID=?", arrayOf(id.toString()))
        db.close()
        return updatedRows
    }

    fun deletedNoteById(id: Long): Int {
        val db = this.writableDatabase
        val deletedRows = db.delete(TABLE_NOTE, "$COLUMN_ID=?", arrayOf(id.toString()))
        db.close()
        return deletedRows
    }

    @SuppressLint("Range")
    fun getListNote(): MutableList<Note> {
        val db = readableDatabase
        val cursor = db.rawQuery("SELECT * FROM $DATABASE_NAME", null)
        val notes = mutableListOf<Note>()
        if(cursor == null || !cursor.moveToFirst()) return notes

        if (cursor.moveToNext()) {
            val title = cursor.getString(cursor.getColumnIndex(COLUMN_TITLE))
            val content = cursor.getString(cursor.getColumnIndex(COLUMN_CONTENT))
            notes.add(Note(title, content))
        }

        cursor.close()
        db.close()
        return notes
    }
}