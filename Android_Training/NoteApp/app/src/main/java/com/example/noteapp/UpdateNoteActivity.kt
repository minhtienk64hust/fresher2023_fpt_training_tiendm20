package com.example.noteapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.noteapp.databinding.ActivityUpdateNoteBinding

class UpdateNoteActivity : AppCompatActivity() {

    private lateinit var binding: ActivityUpdateNoteBinding
    private val noteDatabase = NoteDatabaseHelper(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUpdateNoteBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.btnUpdate.setOnClickListener {
            val id = binding.edtId.text.toString().toLong()
            val title = binding.edtUpdateTitle.text.toString()
            val content = binding.edtUpdateContent.text.toString()
            val updatedRows = noteDatabase.updateNoteById(id, title, content)

            finish()
        }
    }
}