package com.example.noteapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.noteapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var noteAdapter: NoteAdapter
    private val noteDatabase = NoteDatabaseHelper(this)
    private var notes: MutableList<Note> = mutableListOf(
        Note("Title1", "Content1")
    )

    var addNoteLauncher = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == RESULT_OK) {
            val title = result.data?.getStringExtra("title")
            val content = result.data?.getStringExtra("content")

            notes.add(Note(title, content))
            noteAdapter.setData(notes)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.btnAdd.setOnClickListener {
            val intent = Intent(this, AddNoteActivity::class.java)
            addNoteLauncher.launch(intent)
        }

        binding.btnUpdate.setOnClickListener {
            val intent = Intent(this, UpdateNoteActivity::class.java)
            startActivity(intent)
        }

        binding.btnDelete.setOnClickListener {
            val intent = Intent(this, DeleteNoteActivity::class.java)
            startActivity(intent)
        }

        binding.rvNotes.layoutManager = LinearLayoutManager(this)
        noteAdapter = NoteAdapter(notes)
        binding.rvNotes.adapter = noteAdapter

        loadNotes()
    }

    private fun loadNotes() {
        noteAdapter.notes = notes
        noteAdapter.notifyItemChanged(0)
    }
}