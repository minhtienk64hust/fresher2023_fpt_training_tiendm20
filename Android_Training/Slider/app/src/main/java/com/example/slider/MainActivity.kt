package com.example.slider

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewPager: ViewPager2 = findViewById<ViewPager2>(R.id.viewPager);
        val adapter = ImageSliderAdapter()
        viewPager.adapter = adapter

        val pageIndicator = findViewById<TextView>(R.id.pageIndicator)
        val pageCount: Int = adapter.getImageCount()
        pageIndicator.text = getString(R.string.page_indicator, 1, pageCount)

        viewPager.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)

                val pageIndicatorText = getString(R.string.page_indicator, position + 1, pageCount)
                pageIndicator.text = pageIndicatorText
            }
        })
    }
}