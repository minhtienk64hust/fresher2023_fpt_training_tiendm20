package com.example.slider

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView


class ImageSliderAdapter : RecyclerView.Adapter<ImageSliderAdapter.SliderViewHolder>() {
    private val imageList = mutableListOf<Int>()

    init {
        imageList.apply {
            add(R.drawable.cat1)
            add(R.drawable.cat2)
            add(R.drawable.cat3)
            add(R.drawable.cat4)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SliderViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_image_slider, parent, false)
        return SliderViewHolder(view)
    }

    override fun onBindViewHolder(holder: SliderViewHolder, position: Int) {
        val imageResId = imageList[position]
        holder.imageView.setImageResource(imageResId)
    }

    override fun getItemCount(): Int {
        return imageList.size
    }

    inner class SliderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView: ImageView

        init {
            imageView = itemView.findViewById<ImageView>(R.id.imageView)
        }
    }

    fun getImageCount(): Int = imageList.size
}
