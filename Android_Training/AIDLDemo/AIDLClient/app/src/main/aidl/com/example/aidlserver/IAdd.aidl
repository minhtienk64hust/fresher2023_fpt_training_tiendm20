package com.example.aidlserver;
import com.example.aidlserver.MyPerson;

interface IAdd {
    int addNumbers(int num1, int num2);
    List<String> getStringList();
    List<MyPerson> getPersonList();
}