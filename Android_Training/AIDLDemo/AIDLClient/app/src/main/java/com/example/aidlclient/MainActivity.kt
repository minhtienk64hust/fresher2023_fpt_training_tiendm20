package com.example.aidlclient

import android.app.Service
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.os.RemoteException
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.aidlserver.IAdd
import com.example.aidlserver.MyPerson


class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var num1: EditText
    private lateinit var num2: EditText
    private lateinit var btnAdd: Button
    private lateinit var btnNonPremitive: Button
    private var total: TextView? = null
    protected var addService: IAdd? = null
    private val Tag = "Client Application"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        num1 = findViewById<View>(R.id.num1) as EditText
        num2 = findViewById<View>(R.id.num2) as EditText
        btnAdd = findViewById<View>(R.id.btnAdd) as Button
        btnAdd.setOnClickListener(this)

        btnNonPremitive = findViewById<Button>(R.id.btnNonPremitive) as Button
        btnNonPremitive.setOnClickListener(this)

        total = findViewById<View>(R.id.total) as TextView
        initConnection()
    }

    override fun onResume() {
        super.onResume()
        if (addService == null) {
            initConnection()
        }
    }

    private fun initConnection() {
        if (addService == null) {
            val intent = Intent(IAdd::class.java.getName())
            /*this is service name which has been declared in the server's manifest file in service's intent-filter*/
            intent.action = "service.calc"
            /*From 5.0 annonymous intent calls are suspended so replacing with server app's package name*/
            intent.setPackage("com.example.aidlserver")

            // binding to remote service
            bindService(intent, serviceConnection, Service.BIND_AUTO_CREATE)
        }
    }

    private val serviceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            Log.d(Tag, "Service Connected")
            addService = IAdd.Stub.asInterface(iBinder)
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            Log.d(Tag, "Service Disconnected")
            addService = null
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindService(serviceConnection)
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.btnAdd -> {
                Log.d(Tag, "${addService != null}")
                if (num1.length() > 0 && num2.length() > 0 && addService != null) {
                    try {
                        total!!.text = ""
                        Log.d(Tag, "${num1.text.toString().toInt()}")
                        total!!.text = "Result: " + addService!!.addNumbers(num1.text.toString().toInt(), num2.text.toString().toInt())
                    } catch (e: RemoteException) {
                        e.printStackTrace()
                        Log.d(Tag, "Connection cannot be establish")
                    }
                }
            }

            R.id.btnNonPremitive -> {
                try {
                    val list = addService!!.stringList
                    for (i in list.indices) {
                        Log.d("List Data: ", list[i])
                    }
                    val person: List<MyPerson> = addService!!.personList
                    total!!.text = "Custom Object Data\n".trimIndent()
                    for (i in person.indices) {
                        total!!.append("Person Data(Name:${person[i].name} Age:${person[i].age})\n")
                    }
                } catch (e: RemoteException) {
                    e.printStackTrace()
                    Log.d(Tag, "Connection cannot be establish")
                }
            }
        }
    }

}