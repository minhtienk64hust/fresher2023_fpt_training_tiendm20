package com.example.aidlserver

import android.os.Parcel
import android.os.Parcelable

class MyPerson(var name: String, var age: Int) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString().toString(),
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeInt(age)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MyPerson> {
        override fun createFromParcel(parcel: Parcel): MyPerson {
            return MyPerson(parcel)
        }

        override fun newArray(size: Int): Array<MyPerson?> {
            return arrayOfNulls(size)
        }
    }
}