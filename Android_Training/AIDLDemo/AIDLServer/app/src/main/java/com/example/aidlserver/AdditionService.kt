package com.example.aidlserver

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.example.aidlserver.MyPerson

class AdditionService : Service() {
    override fun onBind(intent: Intent): IBinder? {
        return mBinder
    }

    private val mBinder: IAdd.Stub = object : IAdd.Stub() {

        override fun addNumbers(num1: Int, num2: Int): Int {
            return num1 + num2
        }

        override fun getStringList(): List<String> {
            val country: MutableList<String> = mutableListOf()
            country.add("India")
            country.add("Bhutan")
            country.add("Nepal")
            country.add("USA")
            country.add("Canada")
            country.add("China")
            return country


        }

        override fun getPersonList(): List<MyPerson> {
            val person: MutableList<MyPerson> = mutableListOf()
            person.add(MyPerson("A", 10))
            person.add(MyPerson("B", 20))
            person.add(MyPerson("C", 30))
            person.add(MyPerson("D", 40))
            person.add(MyPerson("E", 50))
            person.add(MyPerson("F", 60))
            return person
        }


    }
}