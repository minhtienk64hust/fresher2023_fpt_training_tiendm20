package com.example.intentlistdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.util.Log

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        val greetingsParcelable = intent.extras?.getByteArray("greetings")
        if (greetingsParcelable != null) {
            val greetings = deserializeListOfStrings(greetingsParcelable)
            Log.d("TAG", greetings.toString())
        }

        var greetings2 = intent.getStringArrayListExtra("greetings2")
        Log.d("TAG", greetings2.toString())


    }
}