package com.example.intentlistdemo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.intentlistdemo.databinding.ActivityMainBinding
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream

class MainActivity : AppCompatActivity() {
    private lateinit var activityMainBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        val greetings = arrayListOf(
            "Hello!",
            "Hi!",
            "Salut!",
            "Hallo!",
            "Ciao!",
            "Ahoj!",
            "YAH sahs!",
            "Bog!",
            "Hej!",
            "Czesc!",
            "Ní hảo!",
            "Kon’nichiwa!",
            "Annyeonghaseyo!",
            "Shalom!",
            "Sah-wahd-dee-kah!",
            "Merhaba!",
            "Hujambo!",
            "Olá!"
        )

        super.onCreate(savedInstanceState)
        activityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(activityMainBinding.root)

        val intent = Intent(this@MainActivity, MainActivity2::class.java)
        intent.putExtra("greetings", serializeListOfStrings(greetings))
        intent.putStringArrayListExtra("greetings2", greetings)

        activityMainBinding.transfer.setOnClickListener { startActivity(intent) }

    }
}

fun serializeString(string: String): ByteArray {
    val byteArrayOutputStream = ByteArrayOutputStream()
    val objectOutputStream = ObjectOutputStream(byteArrayOutputStream)

    objectOutputStream.writeObject(string)
    objectOutputStream.flush()
    objectOutputStream.close()

    return byteArrayOutputStream.toByteArray()
}

fun deserializeString(bytes: ByteArray): String {
    val byteArrayInputStream = ByteArrayInputStream(bytes)
    val objectInputStream = ObjectInputStream(byteArrayInputStream)

    val string = objectInputStream.readObject() as String

    objectInputStream.close()

    return string
}

fun serializeListOfStrings(listOfStrings: List<String>): ByteArray {
    val byteArrayOutputStream = ByteArrayOutputStream()
    val objectOutputStream = ObjectOutputStream(byteArrayOutputStream)

    objectOutputStream.writeObject(listOfStrings)
    objectOutputStream.flush()
    objectOutputStream.close()

    return byteArrayOutputStream.toByteArray()
}

fun deserializeListOfStrings(bytes: ByteArray): List<String> {
    val byteArrayInputStream = ByteArrayInputStream(bytes)
    val objectInputStream = ObjectInputStream(byteArrayInputStream)

    val listOfStrings = objectInputStream.readObject() as List<String>

    objectInputStream.close()

    return listOfStrings
}