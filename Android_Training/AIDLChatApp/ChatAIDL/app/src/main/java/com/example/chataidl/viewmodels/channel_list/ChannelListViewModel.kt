package com.example.chataidl.viewmodels.channel_list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.aidlchat.models.UserMessage
import com.example.chataidl.models.ChatChannel

class ChannelListViewModel(): ViewModel() {
    private val _chatChannels = MutableLiveData<List<ChatChannel>>(mutableListOf<ChatChannel>())
    val chatChannels: LiveData<List<ChatChannel>>
        get() = _chatChannels

    // process from server
    // username to filter
    fun getAllChannels(chatChannelsAsUserMessage: List<UserMessage>?, username: String) {
        val localChatChannels: MutableList<ChatChannel> = mutableListOf<ChatChannel>()

        chatChannelsAsUserMessage?.forEach { userMessage ->
            if (userMessage.sender == username) {
                val chatChannel = ChatChannel(
                    user = userMessage.receiver,
                    message = "Bạn: ${userMessage.message}",
                    lastTime = userMessage.sendTime
                )

                // check trong localChatChannel đã có lưu trữ cuộc hội thoại mình và nó chưa,
                // xảy ra khi (sender=user2, receiver=user1), (sender=user1, receiver=user2) -> ko biết ai là người nhắn cuối
                var existDuplicateChatChannel: ChatChannel? = localChatChannels.firstOrNull{ it.user == userMessage.receiver }
                if(existDuplicateChatChannel!= null) {
                    if (existDuplicateChatChannel.lastTime < userMessage.sendTime) {
                        val indexOfExistDuplicateChatChannel = localChatChannels.indexOf(existDuplicateChatChannel)
                        localChatChannels[indexOfExistDuplicateChatChannel] = chatChannel
                    }
                } else {
                    localChatChannels.add(chatChannel)
                }


            } else if (userMessage.receiver == username) {
                val chatChannel = ChatChannel(
                    user = userMessage.sender,
                    message = "${userMessage.message}",
                    lastTime = userMessage.sendTime
                )

                var existDuplicateChatChannel: ChatChannel? = localChatChannels.firstOrNull{ it.user == userMessage.sender }
                if(existDuplicateChatChannel!= null) {
                    if (existDuplicateChatChannel.lastTime < userMessage.sendTime) {
                        val indexOfExistDuplicateChatChannel = localChatChannels.indexOf(existDuplicateChatChannel)
                        localChatChannels[indexOfExistDuplicateChatChannel] = chatChannel
                    }
                } else {
                    localChatChannels.add(chatChannel)
                }
            }

        }
        _chatChannels.postValue(localChatChannels)

    }
}
