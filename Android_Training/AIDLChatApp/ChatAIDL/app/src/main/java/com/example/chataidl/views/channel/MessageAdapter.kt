package com.example.chataidl.views.channel

import android.os.Build
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.chataidl.R
import com.example.aidlchat.models.UserMessage
import com.example.aidlchat.models.UserMessageProvider


class MessageAdapter(val sender: String, val receiver: String) : RecyclerView.Adapter<MessageAdapter.MessageViewHolder>() {
    val messageList: MutableList<UserMessage>

    init {
        @RequiresApi(Build.VERSION_CODES.O)
        // dữ liệu static
        // tránh trả về reference gốc
        messageList = UserMessageProvider.getMessagesList().toMutableList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_message, parent, false)
        return MessageViewHolder(view)
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        val message = messageList[position]

        if (message.sender == "${receiver}" && message.receiver == "${sender}") {
            holder.senderNameTextView.text = message.sender
            holder.messageTextLayout.gravity = Gravity.START

        } else if (message.sender == "${sender}" && message.receiver == "${receiver}") {  // là bản thân mình
            holder.senderNameTextView.text = message.sender
            holder.messageTextLayout.gravity = Gravity.END
        }

        if (position + 1 < messageList.size) {
            if(isLastMessageFromUser(messageList[position], messageList[position+1]))
                holder.senderNameTextView.visibility = View.VISIBLE
            else
                holder.senderNameTextView.visibility = View.INVISIBLE
        } else {
            holder.senderNameTextView.visibility = View.VISIBLE
        }
        holder.messageTextView.text = message.message
    }

    override fun getItemCount(): Int {
        return messageList.size
    }

    inner class MessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val senderNameTextView: TextView = itemView.findViewById(R.id.senderName)
        val messageTextView: TextView = itemView.findViewById(R.id.messageText)
        val messageTextLayout: LinearLayout = itemView.findViewById(R.id.messageTextLayout)
    }


    fun updateMessages(newMessages: List<UserMessage>) {
        val diffCallback = MessageDiffCallback(messageList, newMessages)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        messageList.clear()
        messageList.addAll(newMessages)
        diffResult.dispatchUpdatesTo(this)
    }


    private fun isLastMessageFromUser(userMessageCurrent: UserMessage, userMessageNext: UserMessage?): Boolean {
        if(userMessageNext == null) return true
        else return userMessageCurrent.sender != userMessageNext.sender
    }


    inner class MessageDiffCallback(
        private val oldList: List<UserMessage>,
        private val newList: List<UserMessage>
    ) : DiffUtil.Callback() {

        override fun getOldListSize(): Int = oldList.size
        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].sender == newList[newItemPosition].sender

        }

        // areContentsTheSame is called only when the areItemsTheSame return true
        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }
    }

}
