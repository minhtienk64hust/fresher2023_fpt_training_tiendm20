package com.example.chataidl.views.channel_list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.chataidl.R
import com.example.chataidl.models.ChatChannel
import java.text.SimpleDateFormat
import java.util.Locale

interface ChannelClickListener {
    fun onChannelClick(channel: ChatChannel)
}

class ChannelAdapter(val channelList: List<ChatChannel>) : RecyclerView.Adapter<ChannelAdapter.ChannelViewHolder>() {
    var channelClickListener: ChannelClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChannelViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_channel, parent, false)
        return ChannelViewHolder(view)
    }

    override fun onBindViewHolder(holder: ChannelViewHolder, position: Int) {
        val channel = channelList[position]

        holder.chatChannelName.text = channel.user
        holder.message.text = channel.message
        holder.lastTime.text = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(channel.lastTime)

        holder.itemView.setOnClickListener {
            channelClickListener?.onChannelClick(channel)
        }

    }

    override fun getItemCount(): Int {
        return channelList.size
    }

    inner class ChannelViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val chatChannelName: TextView = itemView.findViewById(R.id.chatChannelName)
        val message: TextView = itemView.findViewById(R.id.message)
        val lastTime: TextView = itemView.findViewById(R.id.lastTime)
    }
}
