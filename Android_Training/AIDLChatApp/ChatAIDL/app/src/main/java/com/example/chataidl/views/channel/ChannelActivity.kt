package com.example.chataidl.views.channel

import android.app.Service
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.util.Log
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.lifecycle.coroutineScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.aidlchat.IChat
import com.example.aidlchat.models.UserMessage
import com.example.chataidl.databinding.ActivityChannelBinding
import com.example.chataidl.services.ChatService
import com.example.chataidl.services.MessagePushListener
import com.example.chataidl.viewmodels.channel.ChannelViewModel
import kotlinx.coroutines.launch
import java.time.Instant
import java.util.Date

class ChannelActivity : AppCompatActivity() {
    private lateinit var activityChannelBinding: ActivityChannelBinding
    private lateinit var messageAdapter: MessageAdapter
    private val channelViewModel: ChannelViewModel by viewModels()
    // remote service from server
    var chatService: IChat? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityChannelBinding = ActivityChannelBinding.inflate(layoutInflater)
        setContentView(activityChannelBinding.root)

        val receiver: String? = intent.getStringExtra("receiver")
        activityChannelBinding.toolbarChannel.title = receiver.toString()
        activityChannelBinding.toolbarChannel.setNavigationOnClickListener {
            finish()
        }

        // bind remote service from server (other app)
        if (chatService == null) {
            initConnection()
        }

        Handler().postDelayed({
            getMessagesFromServer(receiver!!)

        }, 1000)


        // bind local service
        val localChatServiceIntent = Intent(this, ChatService::class.java)
        bindService(localChatServiceIntent, localConnection, Context.BIND_AUTO_CREATE)

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getMessagesFromServer(receiver: String) {
        val messages = chatService?.getMessagesBetween("user2", receiver)

        // lấy messages from server và cập nhật vào viewmodel, tinh chỉnh nếu cần
        if(messages!=null) channelViewModel.getMessagesBetween(messages)
        messageAdapter = MessageAdapter("user2", receiver)
        activityChannelBinding.rcvMessages.layoutManager = LinearLayoutManager(this)
        activityChannelBinding.rcvMessages.adapter = messageAdapter
        channelViewModel.messages.observe(this) { messages ->
            Log.d("TAG", "Update khi gửi tin nhắn mới")
            messageAdapter.updateMessages(messages)
        }

        activityChannelBinding.sendMessage.setOnClickListener {
            val newMessage = UserMessage(
                sender = "user2",
                receiver = "${receiver}",
                message = activityChannelBinding.inputMessage.editText?.text.toString(),
                sendTime = Date.from(Instant.now())
            )
            chatService?.sendMessage(newMessage)

            // update UI for sending a new message from this client
            // chèn dữ liệu và cập nhật UI server để bên server giải quyết

            // get kiểu không trả về reference gốc, nếu không sẽ lỗi index
            val oldMessages = messageAdapter.messageList.toList().toMutableList()
            oldMessages.add(newMessage)
            messageAdapter.updateMessages(oldMessages)

            // dùng get lại từ server như này không cập nhật được
//            val updatedMessages = chatService?.getMessagesBetween("user2", receiver)
//            if (updatedMessages!= null) channelViewModel.getMessagesBetween(updatedMessages)


            // bạn đầu khi không dùng local
//            UserMessageProvider.messages.add(userMessage)
//            // cách không dùng diffutil, dùng diffutil sẽ bị lỗi tùm lum do cache
//            // messageAdapter.messageList.add(userMessage)
//            // messageAdapter.notifyItemInserted(messageAdapter.messageList.size - 1)
//
//            messageAdapter.updateMessages(UserMessageProvider.getMessagesList())


            activityChannelBinding.inputMessage.editText?.text?.clear()

        }
    }

    // bind remote service from server (other app)
    private fun initConnection() {
        if (chatService == null) {
            val intent = Intent(IChat::class.java.getName())
            /*this is service name which has been declared in the server's manifest file in service's intent-filter*/
            intent.action = "service.chat"
            /*From 5.0 annonymous intent calls are suspended so replacing with server app's package name*/
            // thêm .services vào com.example.aidlchat.services không được
            intent.setPackage("com.example.aidlchat")

            // binding to remote service
            bindService(intent, serviceConnection, Service.BIND_AUTO_CREATE)
        }
    }

    private val serviceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            Log.d("TAG", "Service Connected")
            chatService = IChat.Stub.asInterface(iBinder)
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            Log.d("TAG", "Service Disconnected")
            chatService = null
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        // unbind remote service from server
        if(chatService != null) {
            unbindService(serviceConnection)
        }

        // unbind local service
        if (localChatService != null) {
            unbindService(localConnection)
        }
    }


    // -------------------------------------------------------------------------------------------- \\
    // bind local service from client (this app)
    private var localChatService: ChatService? = null
    private val localConnection: ServiceConnection = object: ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val chatServiceBinder = service as ChatService.ChatServiceBinder
            localChatService = chatServiceBinder.chatService

            // nhận từ server
            val messagePushListener = object : MessagePushListener {
                override fun onMessagePush(message: UserMessage) {
                    val oldMessages = messageAdapter.messageList.toList().toMutableList()
                    oldMessages.add(message)
                    messageAdapter.updateMessages(oldMessages)
                }
            }
            localChatService?.messagePushListener = messagePushListener

        }

        override fun onServiceDisconnected(name: ComponentName?) {
            localChatService = null
        }

    }



}