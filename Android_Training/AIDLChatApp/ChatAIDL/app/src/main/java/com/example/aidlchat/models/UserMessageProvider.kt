package com.example.aidlchat.models

import android.os.Build
import androidx.annotation.RequiresApi
import java.text.SimpleDateFormat
import java.util.Locale

object UserMessageProvider {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())

    @RequiresApi(Build.VERSION_CODES.O)
    val messages: MutableList<UserMessage> = mutableListOf(
        UserMessage(sender = "user2", receiver = "user1", message = "Hi, user1!", sendTime = dateFormat.parse("2023-07-28 21:08:30")!!),
        UserMessage(sender = "user1", receiver = "user2", message = "How are you?", sendTime = dateFormat.parse("2023-07-28 21:08:36")!!),
        UserMessage(sender = "user2", receiver = "user1", message = "I'm doing well, thanks for asking!", sendTime = dateFormat.parse("2023-07-28 21:08:42")!!),
        UserMessage(sender = "user1", receiver = "user2", message = "That is good to hear!", sendTime = dateFormat.parse("2023-07-28 21:08:48")!!),
        UserMessage(sender = "user1", receiver = "user2", message = "I am hearing!", sendTime = dateFormat.parse("2023-07-28 21:08:54")!!),
        UserMessage(sender = "user1", receiver = "user2", message = "What else!", sendTime = dateFormat.parse("2023-07-28 21:09:00")!!),
        UserMessage(sender = "user2", receiver = "user1", message = "Oh, I am...", sendTime = dateFormat.parse("2023-07-28 21:09:06")!!),
        UserMessage(sender = "user2", receiver = "user1", message = "..doing", sendTime = dateFormat.parse("2023-07-28 21:09:12")!!),

        UserMessage(sender = "user3", receiver = "user1", message = "Hello, user1", sendTime = dateFormat.parse("2023-07-28 21:09:18")!!),
        UserMessage(sender = "user1", receiver = "user4", message = "Hello, user4", sendTime = dateFormat.parse("2023-07-28 21:09:24")!!)
    )

    @RequiresApi(Build.VERSION_CODES.O)
    fun getMessagesList(): List<UserMessage> {
        return messages.toList()
    }


}