package com.example.chataidl.services

import android.app.Service
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.annotation.RequiresApi
import com.example.aidlchat.IChat
import com.example.aidlchat.models.UserMessage

interface MessagePushListener {
    fun onMessagePush(message: UserMessage)
}

class ChatService : Service() {
    val chatServiceBinder: ChatServiceBinder = ChatServiceBinder()
    var messagePushListener: MessagePushListener? = null

    override fun onCreate() {
        super.onCreate()
        Log.d("TAG", "Client service is live")
    }

    override fun onBind(intent: Intent): IBinder {
        return chatServiceBinder
    }

    inner class ChatServiceBinder: IChat.Stub() {
        val chatService: ChatService
            get() = this@ChatService

        override fun testConnect(params: String?) {
            Log.d("TAG", "OK: ${params}")
        }

        // phục vụ gửi từ server sang client
        override fun sendMessage(message: UserMessage?) {
            Log.d("TAG", "Receive message from server to client: ${message?.message}")
            if (message != null) {
                messagePushListener?.onMessagePush(message)
            }
        }

        // không implement for client
        @RequiresApi(Build.VERSION_CODES.O)
        override fun getAllChatChannels(username: String): MutableList<UserMessage> {
            return mutableListOf<UserMessage>()
        }

        // không implement for client
        override fun getMessagesBetween(sender: String?, receiver: String?): MutableList<UserMessage> {
            return mutableListOf<UserMessage>()
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("TAG", "Client service is destroy")
    }
}
