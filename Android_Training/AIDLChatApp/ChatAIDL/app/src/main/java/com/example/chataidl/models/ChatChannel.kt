package com.example.chataidl.models

import java.util.Date

data class ChatChannel(val user: String, val message: String, val lastTime: Date)