package com.example.chataidl.views.channel_list

import android.app.Service
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.util.Log
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.lifecycle.coroutineScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.aidlchat.IChat
import com.example.aidlchat.models.UserMessage
import com.example.chataidl.databinding.ActivityChannelListBinding
import com.example.chataidl.models.ChatChannel
import com.example.chataidl.viewmodels.channel_list.ChannelListViewModel
import com.example.chataidl.views.channel.ChannelActivity
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.time.Instant
import java.util.Date

class ChannelListActivity : AppCompatActivity() {
    private lateinit var activityChannelListBinding: ActivityChannelListBinding
    var chatService: IChat? = null
    private val channelListViewModel: ChannelListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityChannelListBinding = ActivityChannelListBinding.inflate(layoutInflater)
        setContentView(activityChannelListBinding.root)

        if (chatService == null) {
            initConnection()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onResume() {
        super.onResume()

        Handler().postDelayed({
            getChannelsFromServer()

        }, 1000)

    }

    private fun getChannelsFromServer() {
        val chatChannelsAsUserMessage: List<UserMessage>? = chatService?.getAllChatChannels("user2")

        channelListViewModel.getAllChannels(chatChannelsAsUserMessage, username = "user2")
        channelListViewModel.chatChannels.observe(this) { chatChannels ->
            val adapter = ChannelAdapter(chatChannels)
            adapter.channelClickListener = object : ChannelClickListener {
                override fun onChannelClick(channel: ChatChannel) {
                    val intent = Intent(this@ChannelListActivity, ChannelActivity::class.java)
                    intent.putExtra("receiver", channel.user)
                    startActivity(intent)
                }

            }
            activityChannelListBinding.rcvChannels.layoutManager = LinearLayoutManager(this)
            activityChannelListBinding.rcvChannels.adapter = adapter
        }
    }

    private fun initConnection() {
        if (chatService == null) {
            val intent = Intent(IChat::class.java.getName())
            /*this is service name which has been declared in the server's manifest file in service's intent-filter*/
            intent.action = "service.chat"
            /*From 5.0 annonymous intent calls are suspended so replacing with server app's package name*/
            intent.setPackage("com.example.aidlchat")

            // binding to remote service
            bindService(intent, serviceConnection, Service.BIND_AUTO_CREATE)
        }
    }

    private val serviceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            Log.d("TAG", "Service Connected")
            chatService = IChat.Stub.asInterface(iBinder)
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            Log.d("TAG", "Service Disconnected")
            chatService = null
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindService(serviceConnection)
    }
}