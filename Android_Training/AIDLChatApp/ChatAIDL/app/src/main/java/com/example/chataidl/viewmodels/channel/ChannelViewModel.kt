package com.example.chataidl.viewmodels.channel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.aidlchat.models.UserMessage
import kotlinx.coroutines.flow.collect

class ChannelViewModel(): ViewModel() {
    private val _messages = MutableLiveData<List<UserMessage>>(mutableListOf<UserMessage>())
    val messages: LiveData<List<UserMessage>>
        get() = _messages

    fun getMessagesBetween(messages: List<UserMessage>) {
        _messages.postValue(messages)
    }
}
