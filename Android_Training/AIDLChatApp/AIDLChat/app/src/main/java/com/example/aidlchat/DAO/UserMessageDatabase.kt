package com.example.aidlchat.DAO

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.example.aidlchat.models.UserMessage
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

@Database(entities = arrayOf(UserMessage::class), version = 1)
@TypeConverters(Converters::class)
abstract class UserMessageDatabase: RoomDatabase() {
    abstract fun userMessageDAO(): UserMessageDao

    companion object {
        @Volatile
        private var INSTANCE: UserMessageDatabase? = null

        fun getDatabase(context: Context): UserMessageDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(context, UserMessageDatabase::class.java, "app_database")
                    .createFromAsset("database/usermessage.db")
                    .build()
                INSTANCE = instance

                instance
            }
        }
    }
}

class Converters {
    @TypeConverter
    fun fromDateString(value: String?): Date? {
        return SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(value)
    }

    @TypeConverter
    fun toDateString(date: Date?): String? {
        return SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(date)
    }
}
