package com.example.aidlchat.models
import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date

@Entity
data class UserMessage(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @NonNull @ColumnInfo(name = "sender") val sender: String,
    @NonNull @ColumnInfo(name = "receiver") val receiver: String,
    @NonNull @ColumnInfo(name = "message") val message: String,
    @NonNull @ColumnInfo(name = "sendTime") val sendTime: Date
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        Date(parcel.readLong())
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(sender)
        parcel.writeString(receiver)
        parcel.writeString(message)
        parcel.writeLong(sendTime.time)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserMessage> {
        override fun createFromParcel(parcel: Parcel): UserMessage {
            return UserMessage(parcel)
        }

        override fun newArray(size: Int): Array<UserMessage?> {
            return arrayOfNulls(size)
        }
    }
}