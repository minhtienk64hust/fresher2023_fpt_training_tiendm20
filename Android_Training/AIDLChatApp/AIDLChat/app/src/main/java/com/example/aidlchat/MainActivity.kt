package com.example.aidlchat

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.aidlchat.views.channel.ChannelActivity
import com.example.aidlchat.views.channel_list.ChannelListActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startActivity(Intent(this, ChannelListActivity::class.java))
    }
}