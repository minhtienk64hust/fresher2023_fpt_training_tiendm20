package com.example.aidlchat.views.channel_list

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.coroutineScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.aidlchat.AIDLChatApplication
import com.example.aidlchat.databinding.ActivityChannelListBinding
import com.example.aidlchat.models.ChatChannel
import com.example.aidlchat.viewmodels.channel_list.ChannelListViewModel
import com.example.aidlchat.viewmodels.channel_list.ChannelListViewModelFactory
import com.example.aidlchat.views.channel.ChannelActivity
import kotlinx.coroutines.launch

class ChannelListActivity : AppCompatActivity() {
    private lateinit var activityChannelListBinding: ActivityChannelListBinding
    private val channelListViewModel: ChannelListViewModel by viewModels {
        ChannelListViewModelFactory(
            (application as AIDLChatApplication)
        )
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityChannelListBinding = ActivityChannelListBinding.inflate(layoutInflater)
        setContentView(activityChannelListBinding.root)

    }


    override fun onResume() {
        super.onResume()
        lifecycle.coroutineScope.launch {
            channelListViewModel.getAllChannels("user1")
        }

        channelListViewModel.chatChannels.observe(this) { chatChannels ->
            val adapter = ChannelAdapter(chatChannels)
            adapter.channelClickListener = object : ChannelClickListener {
                override fun onChannelClick(channel: ChatChannel) {
                    val intent = Intent(this@ChannelListActivity, ChannelActivity::class.java)
                    intent.putExtra("receiver", channel.user)
                    startActivity(intent)
                }

            }
            activityChannelListBinding.rcvChannels.layoutManager = LinearLayoutManager(this)
            activityChannelListBinding.rcvChannels.adapter = adapter

        }
    }

}