package com.example.aidlchat.viewmodels.channel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.aidlchat.AIDLChatApplication
import com.example.aidlchat.models.UserMessage
import com.example.aidlchat.repository.UserMessageRepository

class ChannelViewModel(private val userMessageRepository: UserMessageRepository): ViewModel() {
    private val _messages = MutableLiveData<List<UserMessage>>(mutableListOf<UserMessage>())
    val messages: LiveData<List<UserMessage>>
        get() = _messages

    suspend fun getMessagesBetween(sender: String, receiver: String) {
        val localMessages: MutableList<UserMessage> = mutableListOf<UserMessage>()
        userMessageRepository.getMessagesBetween(sender, receiver)
            .collect { userMessage ->
                localMessages.add(userMessage)

            }
        _messages.postValue(localMessages)

    }

    suspend fun sendMessage(userMessage: UserMessage, currentActiveUsername: String) {
        userMessageRepository.sendMessage(userMessage)

        // update UI for receiving a new message from client
        if (userMessage.sender == currentActiveUsername) { // nếu là mình
            getMessagesBetween(sender = currentActiveUsername, receiver = userMessage.receiver)
        } else { // từ client gửi lên
            getMessagesBetween(sender = currentActiveUsername, receiver = userMessage.sender)
        }


    }

}

class ChannelViewModelFactory(val application: Application): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ChannelViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ChannelViewModel(UserMessageRepository.getRepository((application as AIDLChatApplication).database.userMessageDAO())) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}