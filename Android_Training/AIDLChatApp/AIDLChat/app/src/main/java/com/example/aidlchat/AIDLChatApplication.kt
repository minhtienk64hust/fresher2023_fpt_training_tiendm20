package com.example.aidlchat

import android.app.Application
import com.example.aidlchat.DAO.UserMessageDatabase

class AIDLChatApplication: Application()  {
    val database: UserMessageDatabase by lazy { UserMessageDatabase.getDatabase(this) }
}