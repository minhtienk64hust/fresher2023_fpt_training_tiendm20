package com.example.aidlchat.repository

import com.example.aidlchat.DAO.UserMessageDao
import com.example.aidlchat.models.UserMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.withContext

class UserMessageRepository(val userMessageDao: UserMessageDao) {

    suspend fun getAllChannels(username: String): Flow<UserMessage> = flow {
        val userMessages: List<UserMessage> = userMessageDao.getAllChatChannels(username)
        for (um in userMessages) {
            emit(um)
        }
    }.flowOn(Dispatchers.IO)

    suspend fun getMessagesBetween(sender: String, receiver: String): Flow<UserMessage> = flow {
        val messages: List<UserMessage> = userMessageDao.getMessagesBetween(sender, receiver)
        for (m in messages)
            emit(m)
    }.flowOn(Dispatchers.IO)

    fun getAllChannelsForClient(username: String): List<UserMessage> {
        val userMessages: List<UserMessage> = userMessageDao.getAllChatChannels(username)
        return userMessages
    }

    suspend fun sendMessage(userMessage: UserMessage) {
        withContext(Dispatchers.IO) {
            userMessageDao.insert(userMessage)
        }

    }

    companion object {
        @Volatile
        private var INSTANCE: UserMessageRepository? = null

        fun getRepository(userMessageDao: UserMessageDao): UserMessageRepository {
            if (INSTANCE != null) return INSTANCE!!
            else {
                synchronized(this) {
                    INSTANCE = UserMessageRepository(userMessageDao)
                    return INSTANCE!!
                }
            }
        }
    }

}