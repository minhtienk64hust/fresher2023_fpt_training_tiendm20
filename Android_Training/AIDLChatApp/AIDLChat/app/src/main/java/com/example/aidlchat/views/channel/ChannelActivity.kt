package com.example.aidlchat.views.channel

import android.app.Service
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.lifecycle.coroutineScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.aidlchat.AIDLChatApplication
import com.example.aidlchat.IChat
import com.example.aidlchat.models.UserMessage
import com.example.aidlchat.databinding.ActivityChannelBinding
import com.example.aidlchat.services.ChatService
import com.example.aidlchat.services.MessagePushListener
import com.example.aidlchat.viewmodels.channel.ChannelViewModel
import com.example.aidlchat.viewmodels.channel.ChannelViewModelFactory
import kotlinx.coroutines.launch
import java.time.Instant
import java.util.Date

class ChannelActivity : AppCompatActivity() {
    private lateinit var activityChannelBinding: ActivityChannelBinding
    private lateinit var messageAdapter: MessageAdapter
    private val channelViewModel: ChannelViewModel by viewModels {
        ChannelViewModelFactory(
            (application as AIDLChatApplication)
        )
    }
    // remote service from client
    var chatService: IChat? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityChannelBinding = ActivityChannelBinding.inflate(layoutInflater)
        setContentView(activityChannelBinding.root)

        val receiver: String? = intent.getStringExtra("receiver")
        activityChannelBinding.toolbarChannel.title = receiver.toString()
        activityChannelBinding.toolbarChannel.setNavigationOnClickListener {
            finish()
        }


        lifecycle.coroutineScope.launch {
            channelViewModel.getMessagesBetween("user1", receiver ?: "user2")
        }
        messageAdapter = MessageAdapter("user1", receiver ?: "user2")
        activityChannelBinding.rcvMessages.layoutManager = LinearLayoutManager(this)
        activityChannelBinding.rcvMessages.adapter = messageAdapter
        channelViewModel.messages.observe(this) { messages ->
            messageAdapter.updateMessages(messages)
        }

        activityChannelBinding.sendMessage.setOnClickListener {
            val newMessage = UserMessage(
                sender = "user1",
                receiver = "${receiver}",
                message = activityChannelBinding.inputMessage.editText?.text.toString(),
                sendTime = Date.from(Instant.now())

            )

            // gửi message cho client
            chatService?.sendMessage(newMessage)
            lifecycle.coroutineScope.launch {
                // user1 is fixed for this app implies my user1 account
                channelViewModel.sendMessage(newMessage, currentActiveUsername = "user1")
            }



//            UserMessageProvider.messages.add(userMessage)
//            // cách không dùng diffutil, dùng diffutil sẽ bị lỗi tùm lum do cache
//            // messageAdapter.messageList.add(userMessage)
//            // messageAdapter.notifyItemInserted(messageAdapter.messageList.size - 1)
//
//            messageAdapter.updateMessages(UserMessageProvider.getMessagesList())


            activityChannelBinding.inputMessage.editText?.text?.clear()

        }


    }

    override fun onStart() {
        super.onStart()

        // bind local service
        val localChatServiceIntent = Intent(this, ChatService::class.java)
        // startService(localChatServiceIntent)
        // binding a service is an asynchronous method
        bindService(localChatServiceIntent, localConnection, Context.BIND_AUTO_CREATE)


        // bind remote service
        if (chatService == null) {
            initConnection()
        }
    }

    // bind local service from server (this app)
    private var localChatService: ChatService? = null
    private val localConnection: ServiceConnection = object: ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val chatServiceBinder = service as ChatService.ChatServiceBinder
            localChatService = chatServiceBinder.chatService

            // nhận từ client
            val messagePushListener = object : MessagePushListener {
                override fun onMessagePush(message: UserMessage) {
                    // Log.d("TAG", message.message)
                    lifecycle.coroutineScope.launch {
                        // user1 is fixed for this app implies my user1 account
                        channelViewModel.sendMessage(message, currentActiveUsername = "user1")
                    }

                }
            }
            localChatService?.messagePushListener = messagePushListener

        }

        override fun onServiceDisconnected(name: ComponentName?) {
            localChatService = null
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        // Log.d("TAG", "${chatService == null}")
        // unbind local service
        if (localChatService != null) {
            unbindService(localConnection)
        }

        // unbind remote service from client
        if(chatService != null) {
            unbindService(serviceConnection)
        }
    }



    // -------------------------------------------------------------------------------------------- \\




    // bind remote service from client (other app)
    // dù ChatAIDL client chưa khởi động ứng dụng mà client service được chạy khi bind từ server
    private fun initConnection() {
        if (chatService == null) {
            val intent = Intent(IChat::class.java.getName())
            intent.action = "clientservice.chat"
            intent.setPackage("com.example.chataidl")

            // binding remote client service
            bindService(intent, serviceConnection, Service.BIND_AUTO_CREATE)
        }
    }

    private val serviceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            Log.d("TAG", "Client service Connected")
            chatService = IChat.Stub.asInterface(iBinder)
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            Log.d("TAG", "Client service Disconnected")
            chatService = null
        }
    }










}