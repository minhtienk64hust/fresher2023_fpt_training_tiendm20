package com.example.aidlchat.DAO

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.aidlchat.models.UserMessage

@Dao
interface UserMessageDao {
    @Insert
    fun insert(userMessage: UserMessage)


    @Query("SELECT * FROM usermessage WHERE sender = :sender AND receiver = :receiver " +
            "UNION " +
            "SELECT * FROM usermessage WHERE sender = :receiver AND receiver = :sender " +
            "ORDER BY sendTime ASC")
    fun getMessagesBetween(sender: String, receiver: String): List<UserMessage>


    @Query("SELECT id, sender, receiver, message, MAX(sendTime) as sendTime FROM usermessage WHERE sender = :username OR receiver = :username GROUP BY sender, receiver ORDER BY sendTime DESC")
    fun getAllChatChannels(username: String): List<UserMessage>
}