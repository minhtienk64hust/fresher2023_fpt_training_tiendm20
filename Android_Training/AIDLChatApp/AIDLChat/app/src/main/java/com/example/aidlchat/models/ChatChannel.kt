package com.example.aidlchat.models

import android.os.Parcel
import android.os.Parcelable
import java.util.Date

data class ChatChannel(val user: String, val message: String, val lastTime: Date) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        Date(parcel.readLong())
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(user)
        parcel.writeString(message)
        parcel.writeLong(lastTime.time)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ChatChannel> {
        override fun createFromParcel(parcel: Parcel): ChatChannel {
            return ChatChannel(parcel)
        }

        override fun newArray(size: Int): Array<ChatChannel?> {
            return arrayOfNulls(size)
        }
    }
}