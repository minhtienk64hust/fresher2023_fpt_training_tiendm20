package com.example.aidlchat.viewmodels.channel_list

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.aidlchat.AIDLChatApplication
import com.example.aidlchat.models.ChatChannel
import com.example.aidlchat.repository.UserMessageRepository

class ChannelListViewModel(private val userMessageRepository: UserMessageRepository): ViewModel() {
    private val _chatChannels = MutableLiveData<List<ChatChannel>>(mutableListOf<ChatChannel>())
    val chatChannels: LiveData<List<ChatChannel>>
        get() = _chatChannels

    suspend fun getAllChannels(username: String) {
        val localChatChannels: MutableList<ChatChannel> = mutableListOf<ChatChannel>()

        userMessageRepository.getAllChannels(username)
            .collect { userMessage ->
                if (userMessage.sender == username) {
                    val chatChannel = ChatChannel(
                        user = userMessage.receiver,
                        message = "Bạn: ${userMessage.message}",
                        lastTime = userMessage.sendTime
                    )

                    // check trong localChatChannel đã có lưu trữ cuộc hội thoại mình và nó chưa,
                    // xảy ra khi (sender=user2, receiver=user1), (sender=user1, receiver=user2) -> ko biết ai là người nhắn cuối
                    var existDuplicateChatChannel: ChatChannel? = localChatChannels.firstOrNull{ it.user == userMessage.receiver }
                    if(existDuplicateChatChannel!= null) {
                        if (existDuplicateChatChannel.lastTime < userMessage.sendTime) {
                            val indexOfExistDuplicateChatChannel = localChatChannels.indexOf(existDuplicateChatChannel)
                            localChatChannels[indexOfExistDuplicateChatChannel] = chatChannel
                        }
                    } else {
                        localChatChannels.add(chatChannel)
                    }


                } else if (userMessage.receiver == username) {
                    val chatChannel = ChatChannel(
                        user = userMessage.sender,
                        message = "${userMessage.message}",
                        lastTime = userMessage.sendTime
                    )

                    var existDuplicateChatChannel: ChatChannel? = localChatChannels.firstOrNull{ it.user == userMessage.sender }
                    if(existDuplicateChatChannel!= null) {
                        if (existDuplicateChatChannel.lastTime < userMessage.sendTime) {
                            val indexOfExistDuplicateChatChannel = localChatChannels.indexOf(existDuplicateChatChannel)
                            localChatChannels[indexOfExistDuplicateChatChannel] = chatChannel
                        }
                    } else {
                        localChatChannels.add(chatChannel)
                    }
                }

            }
        _chatChannels.postValue(localChatChannels)
    }
}

class ChannelListViewModelFactory(val application: Application): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ChannelListViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ChannelListViewModel(UserMessageRepository.getRepository((application as AIDLChatApplication).database.userMessageDAO())) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}