package com.example.aidlchat.services

import android.app.Service
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.annotation.RequiresApi
import com.example.aidlchat.AIDLChatApplication
import com.example.aidlchat.IChat
import com.example.aidlchat.models.UserMessage
import com.example.aidlchat.models.UserMessageProvider
import com.example.aidlchat.repository.UserMessageRepository

interface MessagePushListener {
    fun onMessagePush(message: UserMessage)
}

class ChatService : Service() {
    val chatServiceBinder: ChatServiceBinder = ChatServiceBinder()
    var messagePushListener: MessagePushListener? = null

    override fun onCreate() {
        super.onCreate()
        Log.d("TAG", "Service is live")
    }

    override fun onBind(intent: Intent): IBinder {
        return chatServiceBinder
    }

    inner class ChatServiceBinder: IChat.Stub() {
        val chatService: ChatService
            get() = this@ChatService

        override fun testConnect(params: String?) {
            Log.d("TAG", "OK: ${params}")
        }

        override fun sendMessage(message: UserMessage?) {
            if (message != null) {
                messagePushListener?.onMessagePush(message)
            }
        }

        @RequiresApi(Build.VERSION_CODES.O)
        override fun getAllChatChannels(username: String): MutableList<UserMessage> {
            val userMessageDao = (application as AIDLChatApplication).database.userMessageDAO()
            val otherUserMessages = userMessageDao.getAllChatChannels(username)
            // Log.d("TAG", "Tra ve thanh cong: ${otherUserMessages.size}")
            return otherUserMessages.toMutableList()

//            val userMessageRepository = UserMessageRepository.getRepository((application as AIDLChatApplication).database.userMessageDAO())
//            val userMessages = userMessageRepository.getAllChannelsForClient(username)
//            Log.d("TAG", "Tra ve thanh cong: ${userMessages.size}")
//            return userMessages.toMutableList()

            // static
            // return UserMessageProvider.getMessagesList().toMutableList()
        }

        override fun getMessagesBetween(sender: String?, receiver: String?): MutableList<UserMessage> {
            val userMessageDao = (application as AIDLChatApplication).database.userMessageDAO()
            val messages = userMessageDao.getMessagesBetween(sender!!, receiver!!)
            return messages.toMutableList()
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("TAG", "Service is destroy")
    }
}
