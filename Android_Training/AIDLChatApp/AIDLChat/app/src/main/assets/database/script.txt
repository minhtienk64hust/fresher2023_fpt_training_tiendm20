CREATE TABLE IF NOT EXISTS usermessage (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    sender TEXT NOT NULL,
    receiver TEXT NOT NULL,
    message TEXT NOT NULL,
    sendTime DATETIME NOT NULL
);

INSERT INTO usermessage (sender, receiver, message, sendtime)
VALUES
    ('user1', 'user2', 'Hello, user2!', '2023-07-28 21:08:24'),
    ('user2', 'user1', 'Hi, user1!', '2023-07-28 21:08:30'),
    ('user1', 'user2', 'How are you?', '2023-07-28 21:08:36'),
    ('user2', 'user1', 'Im doing well, thanks for asking!', '2023-07-28 21:08:42'),
    ('user1', 'user2', 'That is good to hear!', '2023-07-28 21:08:48'),
    ('user1', 'user2', 'I am hearing!', '2023-07-28 21:08:54'),
    ('user1', 'user2', 'What else!', '2023-07-28 21:09:00'),
    ('user2', 'user1', 'Oh, I am...', '2023-07-28 21:09:06'),
    ('user2', 'user1', '..doing', '2023-07-28 21:09:12'),
    ('user3', 'user1', 'Hello, user1', '2023-07-28 21:09:18'),
    ('user1', 'user4', 'Hello, user4', '2023-07-28 21:09:24');


SELECT sender,receiver, message, MAX(sendtime) as sendtime
FROM usermessage
WHERE sender = 'user1' OR receiver = 'user1'
GROUP BY sender, receiver
ORDER BY sendtime DESC