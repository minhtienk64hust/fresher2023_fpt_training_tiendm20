// IChat.aidl
package com.example.aidlchat;
import com.example.aidlchat.models.UserMessage;
import com.example.aidlchat.models.ChatChannel;

interface IChat {
    void testConnect(String params);
    void sendMessage(in UserMessage message);
    List<UserMessage> getAllChatChannels(String username);
    List<UserMessage> getMessagesBetween(String sender, String receiver);
}