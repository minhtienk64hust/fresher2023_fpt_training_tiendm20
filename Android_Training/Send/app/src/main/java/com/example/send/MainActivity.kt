package com.example.send

import android.content.ComponentName
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.start1).setOnClickListener {
            val intent = Intent()
            // using one of below approach
//            intent.component = ComponentName("com.example.receiver", "com.example.receiver.MainActivity")
            intent.setClassName("com.example.receiver", "com.example.receiver.MainActivity")
            startActivity(intent)
        }

        findViewById<Button>(R.id.start2).setOnClickListener {
            val intent = Intent()
            intent.action = "ANDROID.DEMO.INTENT.ACTION.SEND"
            sendBroadcast(intent)
        }
    }
}