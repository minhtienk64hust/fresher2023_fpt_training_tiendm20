package com.example.animationdemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class SongAdapter() : RecyclerView.Adapter<SongAdapter.SongViewHolder>() {
    private var songs = mutableListOf("Unstoppable", "Faded", "Strong")

    class SongViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvSong: TextView = itemView.findViewById(R.id.tvSong)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_song, parent, false)

        return SongViewHolder(view)
    }

    override fun onBindViewHolder(holder: SongViewHolder, position: Int) {
        val song = songs[position]
        holder.tvSong.text = song
    }

    override fun getItemCount() = songs.count()


}