package com.example.animationdemo

import android.animation.ObjectAnimator
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class FragmentA : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_a, container, false)
        val rvSong: RecyclerView = view.findViewById(R.id.rvSong)
        rvSong.layoutManager = LinearLayoutManager(requireContext())
        val songAdapter = SongAdapter()
        rvSong.adapter = songAdapter

        rvSong.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    animateSongsUp(rvSong)
                } else if (dy < 0) {
                    animateSongsDown(rvSong)
                }
            }
        })

        return view
    }

    private fun animateSongsUp(rvSong: RecyclerView) {
        for (i in 0 until rvSong.childCount) {
            val child: View = rvSong.getChildAt(i)
            val animator = ObjectAnimator.ofFloat(child, "translationY", -200f, 0f)
            animator.duration = 300
            animator.start()
            animateAlpha(child, 0f, 1f)
        }
    }

    private fun animateSongsDown(rvSong: RecyclerView) {
        for (i in 0 until rvSong.childCount) {
            val child: View = rvSong.getChildAt(i)
            val animator = ObjectAnimator.ofFloat(child, "translationY", 0f, 200f)
            animator.duration = 300
            animator.start()
            animateAlpha(child, 1f, 0f)
        }
    }

    private fun animateAlpha(child: View, start: Float, end: Float) {
        val animator = ObjectAnimator.ofFloat(view, "alpha", start, end)
        animator.duration = 300
        animator.start()
    }
}