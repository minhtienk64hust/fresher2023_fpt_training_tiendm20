package com.example.animationdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.animationdemo.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        addFragment()
        binding.btnSong.setOnClickListener {
            replaceFragment()
        }
    }

    private fun replaceFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, FragmentA())
            .commit()
    }

    private fun addFragment() {
        val fragmentB = FragmentB()

        supportFragmentManager.beginTransaction()
            .add(R.id.fragmentContainer, fragmentB)
            .commit()
    }
}