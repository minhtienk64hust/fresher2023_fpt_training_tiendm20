package com.example.animationdemo

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageButton
import android.widget.ImageView

class FragmentB : Fragment() {
    private lateinit var songAnimation: Animation
    private lateinit var ivSong: ImageView
    private lateinit var ibPlay: ImageButton
    private var isPlaying = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_b, container, false)
        songAnimation = AnimationUtils.loadAnimation(requireContext(), R.anim.rotate)

        ivSong = view.findViewById(R.id.ivSong)
        ibPlay = view.findViewById(R.id.ibPlay)

        ibPlay.setOnClickListener {
            if (isPlaying) {
                pauseSong()
            } else {
                playSong()
            }
        }

        return view
    }

    private fun playSong() {
        ivSong.startAnimation(songAnimation)
        ibPlay.setImageResource(R.drawable.ic_play)
        isPlaying = true
    }

    private fun pauseSong() {
        ivSong.clearAnimation()
        ibPlay.setImageResource(R.drawable.ic_pause)
        isPlaying = false
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) = FragmentB()
    }
}