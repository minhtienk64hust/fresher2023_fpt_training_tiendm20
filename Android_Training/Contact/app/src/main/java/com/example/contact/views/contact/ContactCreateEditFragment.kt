package com.example.contact.views.contact

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.contact.R
import com.example.contact.databinding.FragmentContactCreateEditBinding
import com.example.contact.models.Contact
import com.example.contact.viewmodels.ContactViewModel
import kotlin.math.log

class ContactCreateEditFragment : Fragment() {
    private lateinit var frC_Ct_EdBinding: FragmentContactCreateEditBinding
    private lateinit var toolbar: Toolbar
    private var typeFragment: String? = null
    private var existContact: Contact? = null // to update

    // trả về một ViewModel được chia sẻ giữa tất cả các fragment trong cùng một activity.
    private val contactViewModel: ContactViewModel by activityViewModels()

    fun setExistContact(contact: Contact) {
        this.existContact = contact
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        frC_Ct_EdBinding = FragmentContactCreateEditBinding.inflate(inflater, container, false)

        // Log.d("TAG", contactViewModel.toString()) // cùng instance cũ
        return frC_Ct_EdBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar = frC_Ct_EdBinding.toolbarCreateEdit

        typeFragment = arguments?.getString("TYPE_FRAGMENT")

        when (typeFragment) {
            "CREATE_CONTACT" -> {
                toolbar.menu.findItem(R.id.deleteContact).title = "Trợ giúp và phản hồi"
                frC_Ct_EdBinding.toolbarCreateEdit.title = "Tạo người liên hệ"
            }
            "EDIT_CONTACT" -> {
                toolbar.menu.findItem(R.id.deleteContact).title = "Xoá"
                frC_Ct_EdBinding.toolbarCreateEdit.title = "Chỉnh sửa người liên hệ"

                frC_Ct_EdBinding.inputName.editText?.setText(existContact?.name)
                frC_Ct_EdBinding.inputPhone.editText?.setText(existContact?.phone)
                frC_Ct_EdBinding.inputEmail.editText?.setText(existContact?.email)
            }

        }

        toolbar.setNavigationOnClickListener {
            // Log.d("TAG", "close: ")
            requireActivity().supportFragmentManager.popBackStack()
        }

        frC_Ct_EdBinding.saveContact.setOnClickListener {
            val name: String? = frC_Ct_EdBinding.inputName.editText?.editableText
                .takeIf { it?.isNotBlank() == true }
                ?.toString()
            val phone = frC_Ct_EdBinding.inputPhone.editText?.editableText
                .takeIf { it?.isNotBlank() == true }
                ?.toString()
            val email = frC_Ct_EdBinding.inputEmail.editText?.editableText
                .takeIf { it?.isNotBlank() == true }
                ?.toString()

            if (typeFragment == "CREATE_CONTACT") {
                val contact: Contact = Contact(name = name!!, phone = phone, email = email, avatar = null)
                contactViewModel.insertContact(contact)
            } else {
                val newContact = existContact?.copyWith(name!!, phone, email)
                contactViewModel.updateContact(newContact!!)
            }

            requireActivity().supportFragmentManager.popBackStack()
        }




    }


    companion object {
        @JvmStatic
        fun newInstance(typeFragment: String) = ContactCreateEditFragment().apply {
            arguments = Bundle().apply {
                putString("TYPE_FRAGMENT", typeFragment)
            }
        }

    }
}