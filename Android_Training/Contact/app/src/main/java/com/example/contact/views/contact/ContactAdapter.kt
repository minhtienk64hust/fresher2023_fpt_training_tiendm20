package com.example.contact.views.contact

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.contact.R
import com.example.contact.models.Contact
import io.getstream.avatarview.AvatarView
import kotlin.random.Random

class ContactAdapter(var contacts: List<Contact>): RecyclerView.Adapter< ContactAdapter.ContactViewHolder>(), Filterable {
    // ListAdapter không cần cái này
//    private var contacts = mutableListOf<Contact>(
//        Contact(id = 1, name = "Tien", phone = null, email = null, avatar = null)
//    )

    var navigateFragmentListener: NavigateFragmentListener? = null
    private val keepOldContacts: List<Contact> = contacts


    companion object {
        private val DiffCallback = object : DiffUtil.ItemCallback<Contact>() {
            override fun areItemsTheSame(oldItem: Contact, newItem: Contact): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Contact, newItem: Contact): Boolean {
                return oldItem == newItem
            }
        }
    }

    inner class ContactViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        val titleForHeaderInSection = view.findViewById<TextView>(R.id.titleForHeaderInSection)
        val avatar = view.findViewById<AvatarView>(R.id.imgAvatar)
        val txtName = view.findViewById<TextView>(R.id.txtName)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val itemView: View = LayoutInflater.from(parent.context).inflate(R.layout.item_contact, parent, false)
        val contactViewHolder = ContactViewHolder(itemView)
        return contactViewHolder
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        val contact = contacts.get(position)
        holder.txtName.text = contact.name
        holder.avatar.avatarInitials = contact.name[0].toString()
        holder.avatar.avatarInitialsBackgroundColor = Color().getRandomMaterialColor()

//        holder.txtName.text = getItem(position).name
        holder.view.setOnClickListener {
            navigateFragmentListener?.onNavigateToDetailContactFragment(contact)
        }

        var isVisibleForTitleSection = false
        if(position==0) isVisibleForTitleSection = true
        else if (isSameSection(contacts.get(position-1), contacts.get(position))) {
            isVisibleForTitleSection = false
        } else {
            isVisibleForTitleSection = true
        }
        if(isVisibleForTitleSection) {
            holder.titleForHeaderInSection.visibility = View.VISIBLE
            holder.titleForHeaderInSection.text = contact.name[0].toString()
        } else {
            // Nếu không có else thì sẽ bị lỗi chữ lung tung các item cuối do item bị cache
            holder.titleForHeaderInSection.visibility = View.INVISIBLE
        }

    }

    private fun isSameSection(contactPrevious: Contact, contactCurrent: Contact): Boolean {
        return contactPrevious.name[0] == contactCurrent.name[0]
    }



    override fun getItemCount(): Int {
        return contacts.count()
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val searchString = constraint.toString()
                if (searchString.isEmpty()) contacts = keepOldContacts
                val filteredListContacts = contacts.filter { it.name.startsWith(searchString, ignoreCase = true) }

                val filterResults = FilterResults()
                filterResults.values = filteredListContacts
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                contacts = results?.values as List<Contact>
                notifyDataSetChanged()
            }
        }
    }

}

fun Color.getRandomMaterialColor(): Int {
    val materialColors = arrayOf("#F44336", "#E91E63", "#9C27B0", "#673AB7", "#3F51B5",
        "#2196F3", "#03A9F4", "#00BCD4", "#009688", "#4CAF50")
    val randomColor = materialColors[Random.nextInt(materialColors.size)]
    return Color.parseColor(randomColor)
}