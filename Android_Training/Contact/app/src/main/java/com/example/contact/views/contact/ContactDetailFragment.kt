package com.example.contact.views.contact

import android.annotation.SuppressLint
import android.content.DialogInterface.BUTTON_NEGATIVE
import android.content.DialogInterface.BUTTON_POSITIVE
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.FragmentManager.OnBackStackChangedListener
import androidx.fragment.app.activityViewModels
import com.example.contact.R
import com.example.contact.databinding.FragmentContactDetailBinding
import com.example.contact.models.Contact
import com.example.contact.viewmodels.ContactViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar

// pContact nên là val immutable chỉ để đọc đổ dữ liệu, nếu chuyển tiếp cho màn hình read data thì truyền pContact, nếu chuyển tiếp cho màn hình edit thì truyền contact
class ContactDetailFragment(val pContact: Contact) : Fragment() {
    private lateinit var fragmentContactDetailBinding: FragmentContactDetailBinding
    private lateinit var toolbar: Toolbar
    private val contactViewModel: ContactViewModel by activityViewModels()

    // using to edit
    private var contact: Contact = pContact

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("TAG", "onCreate: ")
        super.onCreate(savedInstanceState)

    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d("TAG", "onCreateView: ")
        fragmentContactDetailBinding = FragmentContactDetailBinding.inflate(inflater, container, false)
        return fragmentContactDetailBinding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("TAG", "onViewCreated: ")

        toolbar = fragmentContactDetailBinding.toolbarDetail

        inflateDetailInfoContact()

        setUpActionInToolbar()

        fragmentContactDetailBinding.phonecall.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:${contact.phone}")
            startActivity(intent)
        }

    }

    private fun setUpActionInToolbar() {
        toolbar.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.deleteContact -> {

                    MaterialAlertDialogBuilder(requireContext())
                        .setTitle("Chuyển vào Thùng rác?")
                        .setMessage("Các mục liên hệ này sẽ bị xoá khỏi tất cả thiết bị đã đồng bộ ho của bạn")
                        .setNegativeButton("Huỷ") { dialog, which ->

                        }
                        .setPositiveButton("Chuyển vào thùng rác") { dialog, which ->
                            when (which) {
                                BUTTON_NEGATIVE -> {

                                }

                                BUTTON_POSITIVE -> {
                                    Snackbar.make(
                                        fragmentContactDetailBinding.root,
                                        "Đã xoá 1 liên hệ",
                                        Snackbar.LENGTH_SHORT
                                    )
                                        .setAction("Huỷ", object : View.OnClickListener {
                                            override fun onClick(v: View?) {

                                            }
                                        })
                                        .show()
                                    contactViewModel.deleteContact(contact.id)
                                    requireActivity().supportFragmentManager.popBackStack()

                                }
                            }
                        }
                        .show()
                    true
                }

                R.id.editContact -> {
                    val contactAddEditFragment =
                        ContactCreateEditFragment.newInstance("EDIT_CONTACT")

                    contactAddEditFragment.setExistContact(contact)

                    requireActivity().supportFragmentManager.beginTransaction()
                        .replace(R.id.host_fragment, contactAddEditFragment)
                        .addToBackStack(null)
                        .commit()

                    true
                }

                else -> {
                    false
                }
            }
        }

        toolbar.setNavigationOnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        }
    }

    private fun inflateDetailInfoContact() {
        // nếu dùng local val contact thì sẽ giải quyết được problem sau khi pop từ edit về thì detail sẽ cập nhật
        // nhưng khi ấn update lại phát nữa thì vẫn dùng biến contact cũ -> đổi contact global thành var
        // -> dùng var không ổn, nên là immutable

        contactViewModel.contacts.observe(viewLifecycleOwner) { contacts ->
            contact = contacts.find { it.id == contact.id }!!

            fragmentContactDetailBinding.apply {
                detailInfoName.text = contact.name
                if (contact.phone != null) {
                    detailInfoPhone.text = contact.phone
                    phonecall.isEnabled = true
                    message.isEnabled = true
                    videocall.isEnabled = true
                    detailInfoPhone.apply {
                        setTextAppearance(android.R.style.TextAppearance)
                    }
                } else {
                    detailInfoPhone.text = "Thêm số điện thoại"
                    detailInfoPhone.apply {
                        setTextAppearance(com.google.android.material.R.style.Widget_Material3_Button_TextButton)
                    }
                    phonecall.isEnabled = false
                    message.isEnabled = false
                    videocall.isEnabled = false
                }

                if (contact.email != null) {
                    detailInfoEmail.text = contact.email
                    detailInfoEmail.apply {
                        setTextAppearance(android.R.style.TextAppearance)
                    }
                } else {
                    detailInfoEmail.text = "Thêm email"
                    detailInfoEmail.apply {
                        setTextAppearance(com.google.android.material.R.style.Widget_Material3_Button_TextButton)
                    }
                }
            }
        }
    }

    override fun onResume() {
        Log.d("TAG", "onResume: ")
        super.onResume()
    }

    override fun onPause() {
        Log.d("TAG", "onPause: ")
        super.onPause()
    }

    override fun onStop() {
        Log.d("TAG", "onStop: ")
        super.onStop()
    }

    override fun onDestroy() {
        Log.d("TAG", "onDestroy: ")
        super.onDestroy()
    }


}