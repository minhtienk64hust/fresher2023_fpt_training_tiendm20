package com.example.contact.DAO

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.contact.models.Contact

@Dao
interface ContactDao {

    @Insert
    fun insertContact(contact: Contact)

    @Update
    fun updateContact(contact: Contact)

    @Query("DELETE FROM contact WHERE id = :contactId")
    fun deleteContactById(contactId: Int)

    @Query("SELECT * FROM contact ORDER BY name ASC")
    fun getAllContacts(): List<Contact>

    @Query("SELECT * FROM contact WHERE id = :id")
    fun getContactById(id: Int): Contact?

    @Query("SELECT * FROM contact WHERE name LIKE :name")
    fun getAllContactsByName(name: String): List<Contact>
}