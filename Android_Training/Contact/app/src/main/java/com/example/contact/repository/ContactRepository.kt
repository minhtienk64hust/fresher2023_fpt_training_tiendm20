package com.example.contact.repository

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.room.Room
import com.example.contact.DAO.ContactDao
import com.example.contact.DAO.ContactDatabase
import com.example.contact.models.Contact
import io.reactivex.rxjava3.core.Observable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import java.util.Dictionary
import java.util.Objects
import java.util.concurrent.Executors

interface OnResultListener<T> {
    fun onSuccess(result: T)
    fun onError(message: String)
}

class ContactRepository(val contactDao: ContactDao) {

    private val executor = Executors.newSingleThreadExecutor()

    fun getAllContacts(listener: OnResultListener<List<Contact>>) {
        executor.execute {
            val contacts: List<Contact> = contactDao.getAllContacts()
            // Log.d("TAG", "${Thread.currentThread().name}") // pool-2-thread-1
            executor.execute {
                Handler(Looper.getMainLooper()).post {
                    if (contacts.isNotEmpty()) listener.onSuccess(contacts)
                    else listener.onError("No contact available")
                }
            }
        }
    }

    fun insertContact(contact: Contact, listener: OnResultListener<Contact>? = null) {
        executor.execute {
            contactDao.insertContact(contact)
            executor.execute {
                Handler(Looper.getMainLooper()).post {
                    listener?.onSuccess(contact)
                }
            }
        }
    }

    fun deleteContact(contactId: Int, listener: OnResultListener<Any?>? = null) {
        executor.execute {
            contactDao.deleteContactById(contactId)
            executor.execute {
                Handler(Looper.getMainLooper()).post {
                    listener?.onSuccess(null)
                }
            }
        }
    }

    fun updateContact(contact: Contact, listener: OnResultListener<Any?>? = null) {
        executor.execute {
            contactDao.updateContact(contact)
            Log.d("TAG", contact.name)
            executor.execute {
                Handler(Looper.getMainLooper()).post {
                    listener?.onSuccess(null)
                }
            }
        }
    }

    //-----------------------------Using Rx instead Handler--------------------
    fun getAllContacts2(): Observable<List<Contact>> {
        return Observable.create { emitter ->
            val contacts: List<Contact> = contactDao.getAllContacts()
            if (contacts.isNotEmpty()) {
                emitter.onNext(contacts)
            } else {
                emitter.onError(Throwable("No contact available"))
            }
        }

    }


    //-----------------------------Using Flow instead Rx --------------------
    suspend fun getAllContacts4(): Flow<Contact> = flow {
        val contacts: List<Contact> = contactDao.getAllContacts()
        for (contact in contacts) {
            emit(contact)
        }
    }.flowOn(Dispatchers.IO)




    companion object {
        @Volatile
        private var INSTANCE: ContactRepository? = null

        fun getRepository(contactDao: ContactDao): ContactRepository {
            if (INSTANCE != null) return INSTANCE!!
            else {
                synchronized(this) {
                    INSTANCE = ContactRepository(contactDao)
                    return INSTANCE!!
                }
            }
        }
    }
}