package com.example.contact.models

import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Contact(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @NonNull @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "phone") val phone: String?,
    @ColumnInfo(name = "email") val email: String?,
    @ColumnInfo(name = "avatar") val avatar: ByteArray?
) {
    fun copyWith(name: String = this.name, phone: String? = this.phone, email: String? = this.email, avatar: ByteArray? = this.avatar): Contact {
        return Contact(this.id, name, phone, email, avatar)
    }

    fun copyWith(contact: Contact): Contact {
        return Contact(contact.id, contact.name, contact.phone, contact.email, contact.avatar)
    }
}