package com.example.contact

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.contact.databinding.ActivityMainBinding
import com.example.contact.views.contact.ContactCreateEditFragment
import com.example.contact.views.contact.ContactFragment
import com.example.contact.views.contact.NavigateFragmentListener

class MainActivity : AppCompatActivity() {
    private lateinit var activityMainBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(activityMainBinding.root)

        val fragmentManager: FragmentManager = supportFragmentManager

        val contactFragment: ContactFragment = ContactFragment()



        fragmentManager.beginTransaction().add(R.id.host_fragment, contactFragment).commit()


    }
}