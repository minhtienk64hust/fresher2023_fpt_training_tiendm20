package com.example.contact.DAO

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.contact.models.Contact
import com.example.contact.repository.ContactRepository

@Database(entities = arrayOf(Contact::class), version = 1)
abstract class ContactDatabase: RoomDatabase() {
    abstract fun contactDAO(): ContactDao

    companion object {
        @Volatile
        private var INSTANCE: ContactDatabase? = null

        fun getDatabase(context: Context): ContactDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(context, ContactDatabase::class.java, "app_database")
                    .createFromAsset("database/contact.db")
                    .build()
                INSTANCE = instance

                instance
            }
        }
    }
}