package com.example.contact.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.contact.DAO.ContactDao
import com.example.contact.models.Contact
import com.example.contact.repository.ContactRepository
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.contact.ContactApplication
import com.example.contact.repository.OnResultListener
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.flow.collect


class ContactViewModel(private val contactRepository: ContactRepository) : ViewModel() {
    private var _contacts = MutableLiveData<List<Contact>>(mutableListOf<Contact>())
    val contacts: LiveData<List<Contact>>
        get() = _contacts

    fun getAllContacts() {
        contactRepository.getAllContacts(object : OnResultListener<List<Contact>> {
            override fun onSuccess(contacts: List<Contact>) {
                // Log.d("TAG", "${Thread.currentThread().name}") // main

                // Log.d("TAG", "Success")
                /*
                    This method must be called from the main thread. If you need set a value from a background thread,
                    you can use postValue(Object)
                */
                // _contacts.value = contacts

                _contacts.postValue(contacts)
            }

            override fun onError(message: String) {
//                 Log.d("TAG", "Error")
                _contacts.postValue(mutableListOf<Contact>())
            }
        })
    }

//    suspend fun getAllContacts2(): List<Contact> = contactDao.getAllContacts()

    fun insertContact(contact: Contact) {
        contactRepository.insertContact(contact, object : OnResultListener<Contact> {
            override fun onSuccess(contact: Contact) {
                Log.d("TAG", "Success")
                val contacts = _contacts.value?.toMutableList()
                contacts?.add(contact)
               _contacts.postValue(contacts!!)
            }

            override fun onError(message: String) {
                Log.d("TAG", "Error")
            }
        })
    }

    fun deleteContact(contactId: Int) {
        contactRepository.deleteContact(contactId, object : OnResultListener<Any?> {
            override fun onSuccess(any: Any?) {
                getAllContacts()
            }

            override fun onError(message: String) {
                Log.d("TAG", "Error")
            }
        })
    }

    fun updateContact(contact: Contact) {
        contactRepository.updateContact(contact, object : OnResultListener<Any?> {
            override fun onSuccess(any: Any?) {
                getAllContacts()
            }

            override fun onError(message: String) {
                Log.d("TAG", "Error")
            }
        })
    }

    //-----------------------------Using Rx instead Handler--------------------
    fun getAllContacts2() {
        contactRepository.getAllContacts2()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ contacts ->
                _contacts.postValue(contacts)
            }, { error ->
                _contacts.postValue(mutableListOf<Contact>())
            })
    }

    fun getAllContacts3() {
        contactRepository.getAllContacts2()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object: Observer<List<Contact>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(contacts: List<Contact>) {
                    _contacts.postValue(contacts)
                }


                override fun onError(e: Throwable) {
                    _contacts.postValue(mutableListOf<Contact>())
                }

                override fun onComplete() {

                }
            })
    }


    //-----------------------------Using Flow instead Rx --------------------
    suspend fun getAllContacts4() {
        val localContacts: MutableList<Contact> = mutableListOf<Contact>()

        contactRepository.getAllContacts4().collect { contact ->
//            Log.d("TAG", "getAllContacts4: ") // gọi rất nhiều lần
//            val contacts = _contacts.value?.toMutableList() // luôn có size là 0
//            contacts?.add(contact)
//            Log.d("TAG", contacts?.first()!!.name) // mỗi lần in ra name khác nhau
//            _contacts.postValue(contacts!!) // chỉ gọi schedule chứ chưa chạy ngay, chỉ gộp lại 1 thể rồi postValue

            localContacts.add(contact)
        }

        _contacts.postValue(localContacts)
    }


}


class ContactViewModelFactory(val application: Application) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ContactViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ContactViewModel(ContactRepository.getRepository((application as ContactApplication).database.contactDAO())) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}