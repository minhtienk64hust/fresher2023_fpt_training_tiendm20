package com.example.contact

import android.app.Application
import com.example.contact.DAO.ContactDatabase

class ContactApplication: Application() {
    val database: ContactDatabase by lazy { ContactDatabase.getDatabase(this) }
}