package com.example.contact.views.contact

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.MenuItemCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.coroutineScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.contact.ContactApplication
import com.example.contact.R
import com.example.contact.databinding.FragmentContactBinding
import com.example.contact.models.Contact
import com.example.contact.viewmodels.ContactViewModel
import com.example.contact.viewmodels.ContactViewModelFactory
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

interface NavigateFragmentListener {
    fun onNavigateToDetailContactFragment(contact: Contact)
}

class ContactFragment : Fragment() {
    private lateinit var fragmentContactBinding: FragmentContactBinding
    private lateinit var rcvContact: RecyclerView
    private lateinit var contactAdapter: ContactAdapter
    private lateinit var toolbar: Toolbar

    private val contactViewModel: ContactViewModel by activityViewModels {
//        ContactViewModelFactory(
//            (activity?.application as ContactApplication).database.contactDAO()
//        )

        ContactViewModelFactory(
            (activity?.application as ContactApplication)
        )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        fragmentContactBinding = FragmentContactBinding.inflate(inflater, container, false)

        fragmentContactBinding.fab.setOnClickListener {
            val contactAddEditFragment = ContactCreateEditFragment.newInstance("CREATE_CONTACT")
            requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.host_fragment, contactAddEditFragment)
                .addToBackStack(null) // Để cho phép quay lại Fragment A khi nhấn nút back
                .commit()
        }

        // Log.d("TAG", contactViewModel.toString())

        return fragmentContactBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rcvContact = fragmentContactBinding.rcvContact
        rcvContact.layoutManager = LinearLayoutManager(requireContext())
//        lifecycle.coroutineScope.launch(Dispatchers.IO) {
//            contactAdapter.submitList(contactViewModel.getAllContacts())
//        }

//        lifecycle.coroutineScope.launch(Dispatchers.IO) {
//            val contacts = contactViewModel.getAllContacts()
//            val contactAdapter = ContactAdapter(contacts)
//
//            withContext(Dispatchers.Main) {
//                rcvContact.adapter = contactAdapter
//            }
//        }

//        lifecycle.coroutineScope.launch { // coroutine on Main
//            val contacts: List<Contact>
//            withContext(Dispatchers.IO) { // coroutine on IO
//               contacts = contactViewModel.getAllContacts()
//            }
//            val contactAdapter = ContactAdapter(contacts)
//            rcvContact.adapter = contactAdapter
//
//        }

//        contactViewModel.getAllContacts() // Using Handler
//        contactViewModel.getAllContacts3() // Using Rx

        // Using Flow
        lifecycle.coroutineScope.launch {
            contactViewModel.getAllContacts4()
        }


        contactViewModel.contacts.observe(viewLifecycleOwner) { contacts ->
            Log.d("TAG", "Thay đổi contacts")
            contactAdapter = ContactAdapter(contacts)
            contactAdapter.navigateFragmentListener = object: NavigateFragmentListener {
                override fun onNavigateToDetailContactFragment(contact: Contact) {
                    val contactDetailFragment = ContactDetailFragment(contact)
                    requireActivity().supportFragmentManager.beginTransaction()
                        .replace(R.id.host_fragment, contactDetailFragment)
                        .addToBackStack(null)
                        .commit()
                }

            }

            rcvContact.adapter = contactAdapter
        }


        toolbar = fragmentContactBinding.toolbar
        toolbar.menu.findItem(R.id.action_search).setOnActionExpandListener(object: MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                Log.d("TAG", "onMenuItemActionExpand: ")
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                Log.d("TAG", "onMenuItemActionCollapse: ")
                return true
            }

        })

        val menuItem: MenuItem = toolbar.menu.findItem(R.id.action_search)
        val searchView: SearchView = MenuItemCompat.getActionView(menuItem) as SearchView
        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                Log.d("TAG", query.toString())
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                contactAdapter.filter.filter(newText)
                return true
            }
        })


    }

    companion object {
        fun newInstance() = ContactFragment()
    }


}