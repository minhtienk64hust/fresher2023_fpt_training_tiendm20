package com.example.employeerecyclerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.employeerecyclerview.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), EmployeeSelectListener {
    var selectedEmployeeList = mutableListOf<Employee>()
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val recyclerView: RecyclerView = binding.recyclerViewEmployee
        recyclerView.layoutManager = LinearLayoutManager(this)
        val employeeAdapter = EmployeeAdapter(this)
        employeeAdapter.setOnEmployeeSelectFunc { employee ->  Toast.makeText(this@MainActivity, employee.name, Toast.LENGTH_SHORT).show() }
        recyclerView.adapter = employeeAdapter

        recyclerView.addItemDecoration(
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        )

        binding.btnEnter.setOnClickListener {
            val id = binding.edtId.text.toString()
            val name = binding.edtName.text.toString()
            val sex = if (binding.rbSex.checkedRadioButtonId == R.id.rbFemale) {
                "Female"
            } else {
                "Male"
            }

            val e = Employee(id, name, sex)
            employeeAdapter.employeeList.add(e)
            employeeAdapter.notifyItemInserted(employeeAdapter.employeeList.size - 1)

            binding.edtId.text.clear()
            binding.edtName.text.clear()
            binding.rbSex.clearCheck()

        }

        binding.btnRemove.setOnClickListener {
            employeeAdapter.employeeList.removeAll(selectedEmployeeList)
            employeeAdapter.notifyDataSetChanged()
            selectedEmployeeList.clear()
            if(employeeAdapter.employeeList.isEmpty())  binding.btnRemove.visibility = View.INVISIBLE

        }

    }

    override fun onEmployeeSelect(employee: Employee) {
        if (selectedEmployeeList.contains(employee))
            selectedEmployeeList.remove(employee)
        else
            selectedEmployeeList.add(employee)

        Log.d("TAG", selectedEmployeeList.size.toString())
        binding.btnRemove.visibility = if(selectedEmployeeList.isNotEmpty()) View.VISIBLE else View.INVISIBLE

    }



}