package com.example.employeerecyclerview

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityNodeInfo
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView

data class Employee(
    val id: String,
    val name: String,
    val sex: String
) {}

interface EmployeeSelectListener {
    fun onEmployeeSelect(employee: Employee)
}

class EmployeeAdapter(val esl: EmployeeSelectListener): RecyclerView.Adapter<EmployeeAdapter.EmployeeViewHolder>() {
    var employeeList = mutableListOf<Employee>(
        Employee("E1", "Employee1", "Male"),
        Employee("E2", "Employee2", "Male"),
        Employee("E3", "Employee3", "Female")
    )

    lateinit var onEmployeeSelectFunc1: (Employee) -> Unit
    var onEmployeeSelectFunc2: ((Employee) -> Unit)? = null
    var onEmployeeSelectFunc3: (Employee) -> Unit = {}

    fun setOnEmployeeSelectFunc(employeeSelectFun: (Employee) -> Unit) {
        this.onEmployeeSelectFunc1 = employeeSelectFun
    }

    inner class EmployeeViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        val img = view.findViewById<ImageView>(R.id.imgGender)
        val idName = view.findViewById<TextView>(R.id.tvIdName)
        val checkBox = view.findViewById<CheckBox>(R.id.chkEmployee)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeViewHolder {
        val itemView: View = LayoutInflater.from(parent.context).inflate(R.layout.item_employee_view, parent, false)
        val employeeViewHolder: EmployeeViewHolder = EmployeeViewHolder(itemView)
        return employeeViewHolder
    }

    override fun getItemCount(): Int {
        return employeeList.count()
    }

    override fun onBindViewHolder(holder: EmployeeViewHolder, position: Int) {
        val item = employeeList.get(position)
        holder.img.setImageResource(
            if (item.sex == "Male") R.drawable.ic_male else R.drawable.ic_female
        )
        holder.idName.text = "${item.id}: ${item.name}"
        holder.checkBox.isChecked = false
        holder.checkBox.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                esl.onEmployeeSelect(item)

                onEmployeeSelectFunc1(item)
            }

        })
    }



}