package com.example.fragmentmusic

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.fragmentmusic.databinding.FragmentSingerBinding

interface OnSingerFragmentListener {
    fun onSingerClick(singer: String)
}

class SingerFragment : Fragment() {
    private lateinit var recyclerView: RecyclerView
    var mListener: OnSingerFragmentListener? = null

    fun setOnSingerFragmentListener(mListener: OnSingerFragmentListener?) {
        this.mListener = mListener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView: View = inflater.inflate(R.layout.fragment_singer, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.rv_singer)
        recyclerView.layoutManager = LinearLayoutManager(context)
        val singerAdapter = SingerAdapter()
        singerAdapter.setOnSingerFragmentListener(mListener)
        recyclerView.adapter = singerAdapter
    }


}