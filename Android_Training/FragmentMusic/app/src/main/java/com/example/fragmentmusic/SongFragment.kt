package com.example.fragmentmusic

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

private const val SINGER_PARAM = "SINGER_PARAM"

class SongFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView: View = inflater.inflate(R.layout.fragment_song, container, false)

        arguments?.let {
            val singer = it.getString(SINGER_PARAM)
            if (singer!= null) {
                val recyclerView = rootView.findViewById<RecyclerView>(R.id.rv_song)
                recyclerView.layoutManager = LinearLayoutManager(context)
                recyclerView.adapter = SongAdapter(singer)
                val txt_songs = rootView.findViewById<TextView>(R.id.txt_songs)
                txt_songs.text = getString(R.string.list_songs_of_singer, singer)

                // Có thể trực tiếp navigate tại fragment luôn cũng được: parentFragmentManager
            }
        }

        return rootView
    }

    companion object {
        fun newInstance(singer: String) =
            SongFragment().apply {
                arguments = Bundle().apply {
                    putString(SINGER_PARAM, singer)
                }
            }
    }
}