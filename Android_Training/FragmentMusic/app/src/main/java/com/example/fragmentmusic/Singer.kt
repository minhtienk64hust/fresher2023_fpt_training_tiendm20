package com.example.fragmentmusic

class Singer(var name: String, var songList: List<String>)

val singerList: List<Singer> = listOf(
    Singer("Mỹ Tâm", listOf("Họa mi tóc nâu", "Cả một trời thương nhớ", "Ước gì")),
    Singer("Sơn Tùng MTP", listOf("Chạy ngay đi", "Em của ngày hôm qua", "Lạc trôi")),
    Singer("Mỹ Linh", listOf("Tóc ngắn", "Giọt nước mắt hạnh phúc", "Nơi bình yên")),
    Singer("Noo Phước Thịnh", listOf("Chờ đợi", "Như phút ban đầu", "Chạm")),
    Singer("Đức Phúc", listOf("Hết thương cạn nhớ", "Mình anh", "Mượn rượu tỏ tình")),
    Singer("Jack", listOf("Hoa Hải Đường", "Em gì ơi", "Đừng rời xa")),
    Singer("Đan Trường", listOf("Mưa bụi", "Tình yêu màu nắng", "Tình đơn phương")),
    Singer("Erik", listOf("Có tất cả nhưng thiếu anh", "Đừng có mơ", "Em không sai chúng ta sai")),
    Singer("Trung Quân", listOf("Tình yêu đẹp nhất", "Chỉ một lần thôi", "Tình yêu là gì"))
)