package com.example.fragmentmusic

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.fragmentmusic.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var activityMainBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(activityMainBinding.root)

        val singerFragment: SingerFragment = SingerFragment()
        singerFragment.setOnSingerFragmentListener(object: OnSingerFragmentListener{
            override fun onSingerClick(singer: String) {
                supportFragmentManager.beginTransaction().replace(R.id.host_fragment, SongFragment.newInstance(singer)).addToBackStack(null).commit()
            }
        })


        val fragmentManager: FragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.host_fragment, singerFragment).addToBackStack(null).commit()

    }
}