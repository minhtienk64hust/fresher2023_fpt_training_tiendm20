package com.example.fragmentmusic

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class SongAdapter(private val singer: String): RecyclerView.Adapter<SongAdapter.SongViewHolder>() {
    private val songListOfSinger: List<String>?

    init {
        songListOfSinger = singerList.find { it.name == singer }?.songList
    }

    class SongViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        val txt: TextView = view.findViewById<TextView>(R.id.txt_song)
    }

    override fun getItemCount(): Int {
        return songListOfSinger?.size ?: 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongViewHolder {
        val viewItem: View = LayoutInflater.from(parent.context).inflate(R.layout.item_song_view, parent, false)
        val songViewHolder: SongViewHolder = SongViewHolder(viewItem)
        return songViewHolder
    }

    override fun onBindViewHolder(holder: SongViewHolder, position: Int) {
        val song = songListOfSinger?.get(position)
        holder.txt.text = song
    }
}