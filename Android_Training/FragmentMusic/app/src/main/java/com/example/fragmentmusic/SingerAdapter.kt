package com.example.fragmentmusic

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class SingerAdapter: RecyclerView.Adapter<SingerAdapter.SingerViewHolder>() {
    var mListener: OnSingerFragmentListener? = null

    fun setOnSingerFragmentListener(mListener: OnSingerFragmentListener?) {
        this.mListener = mListener
    }

    class SingerViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        val txt: TextView = view.findViewById<TextView>(R.id.txt_singer)
    }

    override fun getItemCount(): Int {
        return singerList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SingerViewHolder {
        val viewItem: View = LayoutInflater.from(parent.context).inflate(R.layout.item_singer_view, parent, false)
        val singerViewHolder: SingerViewHolder = SingerViewHolder(viewItem)
        return singerViewHolder
    }

    override fun onBindViewHolder(holder: SingerViewHolder, position: Int) {
        val singer = singerList.get(position)
        holder.txt.text = singer.name
        holder.view.setOnClickListener {
            mListener?.onSingerClick(singer.name)
        }
    }
}