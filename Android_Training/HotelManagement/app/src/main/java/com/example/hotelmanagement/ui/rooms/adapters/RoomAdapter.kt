package com.example.hotelmanagement.ui.rooms.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.hotelmanagement.R
import com.example.hotelmanagement.data.db.models.Room
import com.example.hotelmanagement.databinding.ItemRoomBinding

class RoomAdapter(private var rooms: List<Room>) : RecyclerView.Adapter<RoomAdapter.RoomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoomViewHolder {
        val binding: ItemRoomBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_room, parent, false)
        return RoomViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RoomViewHolder, position: Int) {
        val room = rooms[position]
        holder.bind(room)
    }

    override fun getItemCount() = rooms.size
    fun setData(rooms: List<Room>) {
        this.rooms = rooms
    }

    inner class RoomViewHolder(private val binding: ItemRoomBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(room: Room) {
            binding.room = room
            binding.executePendingBindings()
        }
    }
}