package com.example.hotelmanagement.repository

import com.example.hotelmanagement.data.db.dao.RoomDao
import com.example.hotelmanagement.data.db.models.Room
import kotlinx.coroutines.flow.Flow

class RoomRepository(private val roomDao: RoomDao) {

    suspend fun getAll(): Flow<List<Room>> {
        return roomDao.getAll()
    }

    suspend fun insert(room: Room) {
        roomDao.insert(room)
    }
}