package com.example.hotelmanagement.viewmodels.rooms

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.hotelmanagement.data.db.models.Room
import com.example.hotelmanagement.repository.ClientRepository
import com.example.hotelmanagement.repository.RoomRepository
import kotlinx.coroutines.launch

class RoomViewModel(private val roomRepository: RoomRepository) : ViewModel() {
    private val _rooms: MutableLiveData<List<Room>> = MutableLiveData()
    val rooms: LiveData<List<Room>> get() = _rooms

    private fun getAll() =
        viewModelScope.launch {
            roomRepository.getAll().collect { rooms ->
                _rooms.value = rooms
            }
        }

    private fun insert(room: Room) =
        viewModelScope.launch {
            roomRepository.insert(room)
        }
}