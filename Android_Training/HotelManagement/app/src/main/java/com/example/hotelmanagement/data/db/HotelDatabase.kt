package com.example.hotelmanagement.data.db

import android.content.Context
import androidx.room.Room
import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.hotelmanagement.data.db.dao.ClientDao
import com.example.hotelmanagement.data.db.dao.RoomDao
import com.example.hotelmanagement.data.db.models.Client

@Database(entities = [Client::class, Room::class], version = 1, exportSchema = false)
abstract class HotelDatabase : RoomDatabase() {
    abstract fun roomDao(): RoomDao
    abstract fun clientDao(): ClientDao

    companion object {
        @Volatile
        private var INSTANCE: HotelDatabase? = null

        fun getInstance(context: Context): HotelDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    HotelDatabase::class.java,
                    "hotel.db").build()
                INSTANCE = instance
                instance
            }
        }
    }
}