package com.example.hotelmanagement.viewmodels.clients

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.hotelmanagement.data.db.models.Client
import com.example.hotelmanagement.repository.ClientRepository
import kotlinx.coroutines.launch

class ClientViewModel(private val clientRepository: ClientRepository) : ViewModel() {

    private val _clients: MutableLiveData<List<Client>> = MutableLiveData()
    val clients: LiveData<List<Client>> get() = _clients

    private fun getAll() = viewModelScope.launch {
        clientRepository.getAll().collect { clients ->
            _clients.value = clients
        }
    }

    private fun insert(client: Client) = viewModelScope.launch {
        clientRepository.insert(client)
    }
}