package com.example.hotelmanagement

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.hotelmanagement.databinding.ActivityMainBinding
import com.example.hotelmanagement.ui.clients.fragments.ClientFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportFragmentManager.beginTransaction().add(binding.frClient.id, ClientFragment()).commit()
    }
}