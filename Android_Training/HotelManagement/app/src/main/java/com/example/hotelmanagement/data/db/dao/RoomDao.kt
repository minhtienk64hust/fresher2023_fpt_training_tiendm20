package com.example.hotelmanagement.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.hotelmanagement.data.db.models.Room
import kotlinx.coroutines.flow.Flow

@Dao
interface RoomDao {
    @Query("SELECT * FROM rooms")
    fun getAll(): Flow<List<Room>>

    @Insert
    suspend fun insert(room: Room)
}