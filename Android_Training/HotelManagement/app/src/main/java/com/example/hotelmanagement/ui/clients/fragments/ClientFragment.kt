package com.example.hotelmanagement.ui.clients.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.hotelmanagement.R
import com.example.hotelmanagement.data.db.HotelDatabase
import com.example.hotelmanagement.data.db.models.Client
import com.example.hotelmanagement.databinding.FragmentClientBinding
import com.example.hotelmanagement.repository.ClientRepository
import com.example.hotelmanagement.ui.clients.adapters.ClientAdapter
import com.example.hotelmanagement.ui.rooms.fragments.RoomFragment
import com.example.hotelmanagement.viewmodels.clients.ClientViewModel
import com.example.hotelmanagement.viewmodels.clients.ClientViewModelFactory

class ClientFragment : Fragment() {


    private lateinit var binding: FragmentClientBinding

    private fun startRoomFragment(client: Client) {
        val roomFragment = RoomFragment.newInstance(client.name)
        parentFragmentManager.beginTransaction()
            .replace(R.id.frClient, roomFragment)
            .addToBackStack(null)
            .commit()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_client,
            container,
            false
        )

        val clients = listOf(
            Client("1", "John Doe", "Male", "USA", "1234567890", "2", "2021-10-01", "500"),
            Client("2", "Jane Smith", "Female", "Canada", "0987654321", "3", "2021-09-15", "700"),
            Client("3", "Michael Johnson", "Male", "Australia", "9876543210", "1", "2021-11-03", "400"),
            Client("4", "Emily Davis", "Female", "UK", "0123456789", "4", "2021-08-20", "800"),
            Client("5", "David Anderson", "Male", "Germany", "6789012345", "2", "2021-10-10", "600"),
            Client("6", "Emma Wilson", "Female", "France", "4567890123", "3", "2021-09-01", "900"),
            Client("7", "Daniel Thompson", "Male", "Spain", "5432109876", "1", "2021-11-15", "300"),
            Client("8", "Olivia Brown", "Female", "Italy", "8901234567", "2", "2021-08-05", "100")
        )

        binding.revenue = clients.sumOf { it.roomExpenses.toDouble() }

        val clientAdapter =ClientAdapter(clients) { client ->
            startRoomFragment(client)
        }

        val db = HotelDatabase.getInstance(requireContext())
        val clientDao = db.clientDao()
        val clientRepository = ClientRepository(clientDao)
        val clientViewModelFactory = ClientViewModelFactory(clientRepository)
        val clientViewModel = ViewModelProvider(this, clientViewModelFactory)[ClientViewModel::class.java]
        clientViewModel.clients.observe(viewLifecycleOwner) { clients ->
            clientAdapter.setData(clients)
        }

        binding.rvClients.adapter = clientAdapter

        return binding.root
    }

}