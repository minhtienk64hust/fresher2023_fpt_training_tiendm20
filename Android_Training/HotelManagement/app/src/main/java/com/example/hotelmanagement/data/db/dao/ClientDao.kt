package com.example.hotelmanagement.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.hotelmanagement.data.db.models.Client
import kotlinx.coroutines.flow.Flow

@Dao
interface ClientDao {
    @Query("SELECT * FROM clients")
    fun getAll(): Flow<List<Client>>

    @Insert
    suspend fun insert(client: Client)
}