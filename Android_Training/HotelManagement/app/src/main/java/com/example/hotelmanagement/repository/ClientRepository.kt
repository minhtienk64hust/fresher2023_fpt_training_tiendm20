package com.example.hotelmanagement.repository

import com.example.hotelmanagement.data.db.dao.ClientDao
import com.example.hotelmanagement.data.db.models.Client
import kotlinx.coroutines.flow.Flow

class ClientRepository(private val clientDao: ClientDao) {

    suspend fun getAll(): Flow<List<Client>> {
        return clientDao.getAll()
    }

    suspend fun insert(client: Client) {
        clientDao.insert(client)
    }
}