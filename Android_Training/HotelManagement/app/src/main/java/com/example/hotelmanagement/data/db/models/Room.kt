package com.example.hotelmanagement.data.db.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "rooms")
data class Room(
    @PrimaryKey val number: String,
    val type: RoomType,
    val facilities: String,
    val photos: Int = 1,
    val price: Double
)