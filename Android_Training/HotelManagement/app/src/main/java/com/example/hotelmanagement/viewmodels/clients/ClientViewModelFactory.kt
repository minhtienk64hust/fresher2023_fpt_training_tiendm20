package com.example.hotelmanagement.viewmodels.clients

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.hotelmanagement.repository.ClientRepository

class ClientViewModelFactory(private val clientRepository: ClientRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ClientViewModel::class.java)) {
            return ClientViewModel(clientRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}