package com.example.hotelmanagement.ui.rooms.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.hotelmanagement.R
import com.example.hotelmanagement.data.db.HotelDatabase
import com.example.hotelmanagement.data.db.models.Room
import com.example.hotelmanagement.data.db.models.RoomType
import com.example.hotelmanagement.databinding.FragmentRoomBinding
import com.example.hotelmanagement.repository.RoomRepository
import com.example.hotelmanagement.ui.rooms.adapters.RoomAdapter
import com.example.hotelmanagement.viewmodels.rooms.RoomViewModel
import com.example.hotelmanagement.viewmodels.rooms.RoomViewModelFactory

private const val ARG_CLIENT = "client"

class RoomFragment : Fragment() {
    private var clientName: String? = null
    private lateinit var binding: FragmentRoomBinding
    private lateinit var rooms: List<Room>
    private lateinit var roomAdapter: RoomAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            clientName = it.getString(ARG_CLIENT)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_room, container, false)

        rooms = listOf(
            Room("101", RoomType.SINGLE, "TV, Air conditioning", 1, 100.0),
            Room("102", RoomType.DOUBLE, "TV, Air conditioning", 2, 150.0),
            Room("103", RoomType.SUITE, "TV, Air conditioning, Mini bar", 3, 200.0),
            Room("201", RoomType.SINGLE, "TV, Air conditioning", 1, 100.0),
            Room("202", RoomType.DOUBLE, "TV, Air conditioning", 2, 150.0)
        )

        val roomExpenses = rooms.sumOf { it.price }
        binding.roomExpenses = roomExpenses

        roomAdapter = RoomAdapter(rooms)

        val db = HotelDatabase.getInstance(requireContext())
        val roomDao = db.roomDao()
        val roomRepository = RoomRepository(roomDao)
        val roomViewModelFactory = RoomViewModelFactory(roomRepository)
        val roomViewModel = ViewModelProvider(this, roomViewModelFactory)[RoomViewModel::class.java]
        roomViewModel.rooms.observe(viewLifecycleOwner) { rooms ->
            roomAdapter.setData(rooms)
        }

        binding.rvRoom.adapter = roomAdapter

        return binding.root
    }

    companion object {
        @JvmStatic
        fun newInstance(clientName: String) =
            RoomFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_CLIENT, clientName)
                }
            }
    }
}