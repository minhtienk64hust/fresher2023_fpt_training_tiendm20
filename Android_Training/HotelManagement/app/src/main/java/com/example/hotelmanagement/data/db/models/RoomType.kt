package com.example.hotelmanagement.data.db.models

enum class RoomType {
    SUITE,
    SINGLE,
    DOUBLE,
}