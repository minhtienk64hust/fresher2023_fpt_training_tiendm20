package com.example.hotelmanagement.ui.clients.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.hotelmanagement.R
import com.example.hotelmanagement.data.db.models.Client
import com.example.hotelmanagement.databinding.ItemClientBinding

class ClientAdapter(
    private var clients: List<Client>,
    private val onClientListener: (Client) -> Unit
) : RecyclerView.Adapter<ClientAdapter.ClientViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClientViewHolder {
        val binding: ItemClientBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_client, parent, false)

        return ClientViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ClientViewHolder, position: Int) {
        val client = clients[position]
        holder.bind(client)
        holder.itemView.setOnClickListener {
            onClientListener(client)
        }
    }

    override fun getItemCount() = clients.size

    fun setData(clients: List<Client>) {
        this.clients = clients
    }

    inner class ClientViewHolder(
        private val binding: ItemClientBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(client: Client) {
            binding.client = client
            binding.executePendingBindings()
        }
    }
}