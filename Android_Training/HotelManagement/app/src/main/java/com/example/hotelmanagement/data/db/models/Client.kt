package com.example.hotelmanagement.data.db.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "clients")
data class Client(
    @PrimaryKey val id: String,
    val name: String,
    val gender: String,
    val country: String,
    val phone: String,
    val rooms: String,
    val occupationDate: String,
    val roomExpenses: String,
)