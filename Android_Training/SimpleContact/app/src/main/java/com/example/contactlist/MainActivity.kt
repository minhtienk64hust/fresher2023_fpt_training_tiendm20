package com.example.simplecontact

import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import android.Manifest
import android.annotation.SuppressLint
import android.provider.ContactsContract
import android.util.Log
class MainActivity : AppCompatActivity() {
    companion object {
        private const val CONTACTS_PERMISSION_REQUEST = 1
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS), CONTACTS_PERMISSION_REQUEST)
        }

        displayContacts()
    }

    @SuppressLint("Range")
    private fun displayContacts() {
        val contactsCursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)

        val contactsList = ArrayList<String>()

        if (contactsCursor != null && contactsCursor.count > 0) {
            while (contactsCursor.moveToNext()) {
                val name = contactsCursor.getString(contactsCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                contactsList.add(name)
            }
           
            contactsCursor.close()
        }

        for (name in contactsList) {
            Log.d("TAG", name)
        }
    }
}