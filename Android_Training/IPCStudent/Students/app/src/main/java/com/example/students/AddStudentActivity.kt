package com.example.students

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.students.databinding.ActivityAddStudentBinding

class AddStudentActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAddStudentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_student)

        binding = ActivityAddStudentBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.btnSave.setOnClickListener {

            val id = binding.etId.text.toString()
            val name = binding.etName.text.toString()
            val age = binding.etAge.text.toString().toInt()
            val studentClass = binding.etClass.text.toString()
            val gpa = binding.etGpa.text.toString().toDouble()

            val dbHelper = DatabaseHelper(this)
            dbHelper.addStudent(Student(R.drawable.ic_student, id, name, age, studentClass, gpa))

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}