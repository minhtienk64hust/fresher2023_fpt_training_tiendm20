package com.example.students

import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.students.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var studentAdapter: StudentAdapter
    private lateinit var dbHelper: DatabaseHelper
    private var students = emptyList<Student>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.rvStudents.layoutManager = LinearLayoutManager(this)
        studentAdapter = StudentAdapter(students)
        binding.rvStudents.adapter = studentAdapter
        dbHelper = DatabaseHelper(this)

        binding.fabAddStudent.setOnClickListener {
            val intent = Intent(this, AddStudentActivity::class.java)
            startActivity(intent)
        }

        loadStudents()

        binding.toolbar.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.action_delete -> {
                    showDeleteDialog()
                    true
                }
                R.id.action_sort_by_alpha -> {
                    sortByAlpha(students)
                    true
                }
                R.id.action_search -> {
                    searchByName(students)
                    true
                }
                R.id.action_sort_by_gpa -> {
                    sortByGpa(students)
                    true
                }
                R.id.action_refresh -> {
                    loadStudents()
                    true
                }
                else -> false
            }
        }

        studentAdapter.setItemClickListener(object : StudentAdapter.ItemClickListener {
            override fun onItemClick(student: Student) {
                showStudentInfo(student)
            }
        })
    }

    private fun sortByGpa(students: List<Student>) {
        val sortedStudents = students.sortedBy { it.gpa }
        studentAdapter.setStudent(sortedStudents)
    }

    private fun searchByName(students: List<Student>) {
        val dialogBuilder = AlertDialog.Builder(this)
        dialogBuilder.setTitle("Search by Name")
        val input = EditText(this)
        input.inputType = InputType.TYPE_CLASS_TEXT
        dialogBuilder.setView(input)

        dialogBuilder.setPositiveButton("Search") { _, _ ->
            val searchQuery = input.text.toString().trim()

            val filteredStudents = dbHelper.search(searchQuery)
            studentAdapter.setStudent(filteredStudents)

            if (filteredStudents.isEmpty()) {
                Toast.makeText(this, "Empty students", Toast.LENGTH_SHORT).show()
            }
        }

        dialogBuilder.setNegativeButton("Cancel") { dialog, _ ->
            dialog.dismiss()
        }

        val dialog = dialogBuilder.create()
        dialog.show()
    }

    private fun sortByAlpha(students: List<Student>) {
        val sortedStudents = students.sortedBy { it.name }
        studentAdapter.setStudent(sortedStudents)
    }

    private fun showStudentInfo(student: Student) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Contact Information")
        builder.setMessage("ID: ${student.id}" +
                            "\nName: ${student.name}" +
                            "\nAge: ${student.age}" +
                            "\nClass: ${student.studentClass}" +
                            "\nGPA: ${student.gpa}")

        builder.setIcon(student.image)
        builder.setPositiveButton("Edit") { dialog, _ ->
            showEditDialog(student)
            dialog.dismiss()
        }

        builder.setNegativeButton("Cancel") { dialog, _ ->
            dialog.dismiss()
        }

        builder.show()
    }

    private fun showEditDialog(student: Student) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Edit Student")

        val view = layoutInflater.inflate(R.layout.dialog_edit_student, null)
        val etName = view.findViewById<EditText>(R.id.etEditedName)
        val etAge = view.findViewById<EditText>(R.id.etEditedAge)
        val etClass = view.findViewById<EditText>(R.id.etEditedClass)
        val etGpa = view.findViewById<EditText>(R.id.etEditedGpa)

        etName.setText(student.name)
        etAge.setText(student.age.toString())
        etClass.setText(student.studentClass)
        etGpa.setText(student.gpa.toString())

        builder.setView(view)

        builder.setPositiveButton("Save") { dialog, _ ->
            val newName = etName.text.toString()
            val newAge = etAge.text.toString()
            val newClass = etClass.text.toString()
            val newGpa = etGpa.text.toString()

            val success = dbHelper.updateStudent(student.id, newName, newAge, newClass, newGpa)
            dbHelper.close()

            if (success) {
                Toast.makeText(this, "Student updated successfully", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Failed to update student", Toast.LENGTH_SHORT).show()
            }

            loadStudents()
            dialog.dismiss()
        }

        builder.setNegativeButton("Cancel") { dialog, _ ->
            dialog.dismiss()
        }

        builder.show()
    }

    private fun showDeleteDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("DELETE STUDENTS")

        val view = layoutInflater.inflate(R.layout.dialog_delete_student, null)
        val etId = view.findViewById<EditText>(R.id.etDeletedId)
        val etName: EditText = view.findViewById(R.id.etDeletedName)

        builder.setView(view)

        builder.setPositiveButton("Delete") { _, _ ->
            val name = etName.text.toString()
            val id = etId.text.toString()

            dbHelper.deleteStudent(id)

            loadStudents()
        }

        builder.setNegativeButton("Cancel") { dialog, _ ->
            dialog.dismiss()
        }

        builder.show()
    }

    private fun loadStudents() {
        if (students.isEmpty()) {
            Toast.makeText(this, "Empty students", Toast.LENGTH_SHORT).show()
        }

        students = dbHelper.getAllStudents()
        studentAdapter.setStudent(students)
    }
}

