package com.example.students.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.example.students.DatabaseHelper
import com.example.students.IStudentService
import com.example.students.Student

class StudentService : Service() {

    private val binder: IBinder = object : IStudentService.Stub() {
        override fun getAll(): List<Student> {
            val dbHelper = DatabaseHelper(this@StudentService)
            return dbHelper.getAllStudents()
        }

        override fun create(student: Student) {
            val dbHelper = DatabaseHelper(this@StudentService)
            dbHelper.addStudent(student)
        }

        override fun update(student: Student): Boolean {
            val dbHelper = DatabaseHelper(this@StudentService)
            return dbHelper.updateStudent(
                student.id,
                student.name,
                student.age.toString(),
                student.studentClass,
                student.gpa.toString())
        }

        override fun sortByName(): List<Student> {
            val dbHelper = DatabaseHelper(this@StudentService)
            return dbHelper.sortByName()
        }

        override fun sortByGPA(): List<Student> {
            val dbHelper = DatabaseHelper(this@StudentService)
            return dbHelper.sortByGpa()
        }

        override fun search(query: String): List<Student> {
            val dbHelper = DatabaseHelper(this@StudentService)
            return dbHelper.search(query)
        }

        override fun delete(id: String) {
            val dbHelper = DatabaseHelper(this@StudentService)
            return dbHelper.deleteStudent(id)
        }
    }

    override fun onBind(intent: Intent?): IBinder = binder
}