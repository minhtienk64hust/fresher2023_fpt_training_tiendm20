package com.example.students

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DatabaseHelper(context: Context) : SQLiteOpenHelper(context, TABLE_NAME, null, DATABASE_VERSION) {

    companion object {
        private const val TABLE_NAME = "students"
        private const val DATABASE_VERSION = 1

        private const val COLUMN_ID = "id"
        private const val COLUMN_NAME = "name"
        private const val COLUMN_AGE = "age"
        private const val COLUMN_CLASS = "class"
        private const val COLUMN_GPA = "gpa"
        private const val COLUMN_AVATAR = "avatar"

        private const val CREATE_TABLE = "CREATE TABLE $TABLE_NAME ($COLUMN_ID INTEGER PRIMARY KEY, $COLUMN_NAME TEXT, $COLUMN_AGE INTEGER, $COLUMN_CLASS TEXT, $COLUMN_GPA DOUBLE, $COLUMN_AVATAR INTEGER)"
        private const val DROP_TABLE = "DROP TABLE IF EXISTS $TABLE_NAME"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL(DROP_TABLE)
        onCreate(db)
    }

    fun addStudent(student: Student) {
        val db = writableDatabase
        val values = ContentValues()

        values.put(COLUMN_NAME, student.name)
        values.put(COLUMN_AGE, student.age)
        values.put(COLUMN_CLASS, student.studentClass)
        values.put(COLUMN_GPA, student.gpa)
        values.put(COLUMN_AVATAR, student.image)

        db.insert(TABLE_NAME, null, values)

        db.close()
    }

    fun deleteStudent(id: String) {
        val db = writableDatabase
        val selection = "$COLUMN_ID = ?"
        val selectionArgs = arrayOf(id)
        db.delete(TABLE_NAME, selection, selectionArgs)
        db.close()
    }

    fun updateStudent(id: String?,
                      newName: String?, newAge: String?, newClass: String?, newGpa: String?): Boolean {
        val db = writableDatabase
        val values = ContentValues()
        values.put(COLUMN_NAME, newName)
        values.put(COLUMN_AGE, newAge)
        values.put(COLUMN_CLASS, newClass)
        values.put(COLUMN_GPA, newGpa)
        val selection = "$COLUMN_ID = ?"
        //val selectionArgs = arrayOf(oldName, oldAge, oldClass, oldGpa)
        val selectionArgs = arrayOf(id)
        val student = db.update(TABLE_NAME, values, selection, selectionArgs)
        db.close()

        return student != -1
    }

    fun getAllStudents(): List<Student> {
        val students = mutableListOf<Student>()

        val db = readableDatabase
        val cursor = db.query(
            TABLE_NAME,
            arrayOf(COLUMN_AVATAR, COLUMN_ID, COLUMN_NAME, COLUMN_AGE, COLUMN_CLASS, COLUMN_GPA),
            null,
            null,
            null,
            null,
            null
        )

        while (cursor.moveToNext()) {
            val id = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_ID))
            val name = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME))
            val age = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_AGE)).toInt()
            val studentClass = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_CLASS))
            val gpa = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_GPA)).toDouble()
            val avatar = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_AVATAR))
            val student = Student(avatar, id, name, age, studentClass, gpa)
            students.add(student)
        }

        cursor.close()
        db.close()

        return students
    }

    fun sortByName(): List<Student> {
        return getAllStudents().sortedBy { it.name }
    }

    fun sortByGpa(): List<Student> {
        return getAllStudents().sortedBy { it.gpa }
    }

    fun search(name: String): List<Student> {
        val students = getAllStudents()
        return students.filter { it.name?.contains(name, ignoreCase = true) ?: false }
    }
}