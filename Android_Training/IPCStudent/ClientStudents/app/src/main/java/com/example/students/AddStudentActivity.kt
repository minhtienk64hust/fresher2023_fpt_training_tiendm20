package com.example.students

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.service.controls.ControlsProviderService
import android.util.Log
import android.widget.Toast
import com.example.students.databinding.ActivityAddStudentBinding

class AddStudentActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAddStudentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_student)

        var iStudentService: IStudentService? = null

        val mConnection = object : ServiceConnection {

            override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
                iStudentService = IStudentService.Stub.asInterface(service)

                Toast.makeText(
                    this@AddStudentActivity,
                    "Service connected",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onServiceDisconnected(name: ComponentName?) {
                Log.e(ControlsProviderService.TAG, "Service has unexpectedly disconnected")
                iStudentService = null

                Toast.makeText(
                    this@AddStudentActivity,
                    "Service disconnected",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        val serviceIntent = Intent()
        serviceIntent.`package` = "com.example.students"
        serviceIntent.action = "com.example.students.IStudentService"
        bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE)


        binding = ActivityAddStudentBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.btnSave.setOnClickListener {

            val id = binding.etId.text.toString()
            val name = binding.etName.text.toString()
            val age = binding.etAge.text.toString().toInt()
            val studentClass = binding.etClass.text.toString()
            val gpa = binding.etGpa.text.toString().toDouble()

            val student = Student(1, id, name, age, studentClass, gpa)
            iStudentService?.create(student)

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}