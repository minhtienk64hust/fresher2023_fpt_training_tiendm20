package com.example.students

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class StudentAdapter(var students: List<Student>) : RecyclerView.Adapter<StudentAdapter.StudentViewHolder>() {

    private lateinit var itemClickListener: ItemClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_student, parent, false)

        return StudentViewHolder(view)
    }

    override fun onBindViewHolder(holder: StudentViewHolder, position: Int) {
        val contact = students[position]
        holder.bind(contact)
    }

    override fun getItemCount() = students.size

    fun setStudent(students: List<Student>) {
        this.students = students
        notifyDataSetChanged()
    }

    interface ItemClickListener {
        fun onItemClick(student: Student)
    }

    fun setItemClickListener(listener: ItemClickListener) {
        itemClickListener = listener
    }

    inner class StudentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val ivAvatar: ImageView = itemView.findViewById(R.id.ivAvatar)
        private val tvName: TextView = itemView.findViewById(R.id.tvName)

        fun bind(student: Student) {
            ivAvatar.setImageResource(student.image)
            tvName.text = student.name
        }

        init {
            itemView.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val student = students[position]
                    itemClickListener.onItemClick(student)
                }
            }
        }
    }
}