package com.example.students

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.service.controls.ControlsProviderService
import android.text.InputType
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.students.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var studentAdapter: StudentAdapter
    private var students = emptyList<Student>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.rvStudents.layoutManager = LinearLayoutManager(this)
        studentAdapter = StudentAdapter(students)
        binding.rvStudents.adapter = studentAdapter

        var iStudentService: IStudentService? = null

        val mConnection = object : ServiceConnection {

            override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
                iStudentService = IStudentService.Stub.asInterface(service)

                Toast.makeText(
                    this@MainActivity,
                    "Service connected",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onServiceDisconnected(name: ComponentName?) {
                Log.e(ControlsProviderService.TAG, "Service has unexpectedly disconnected")
                iStudentService = null

                Toast.makeText(
                    this@MainActivity,
                    "Service disconnected",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        val serviceIntent = Intent()
        serviceIntent.`package` = "com.example.students"
        serviceIntent.action = "com.example.students.IStudentService"
        bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE)

        binding.fabAddStudent.setOnClickListener {
            val intent = Intent(this, AddStudentActivity::class.java)
            startActivity(intent)
        }

        iStudentService?.let { loadStudents(it) }

        binding.toolbar.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.action_delete -> {
                    iStudentService?.let { showDeleteDialog(it) }
                    true
                }
                R.id.action_sort_by_alpha -> {
                    sortByAlpha(students)
                    true
                }
                R.id.action_search -> {
                    iStudentService?.let { searchByName(it, students) }
                    true
                }
                R.id.action_sort_by_gpa -> {
                    sortByGpa(students)
                    true
                }
                R.id.action_refresh -> {
                    iStudentService?.let { loadStudents(it) }
                    true
                }
                else -> false
            }
        }

        studentAdapter.setItemClickListener(object : StudentAdapter.ItemClickListener {
            override fun onItemClick(student: Student) {
                iStudentService?.let { showStudentInfo(it, student) }
            }
        })
    }

    private fun sortByGpa(students: List<Student>) {
        val sortedStudents = students.sortedBy { it.gpa }
        studentAdapter.setStudent(sortedStudents)
    }

    private fun searchByName(iStudentService: IStudentService, students: List<Student>) {
        val dialogBuilder = AlertDialog.Builder(this)
        dialogBuilder.setTitle("Search by Name")
        val input = EditText(this)
        input.inputType = InputType.TYPE_CLASS_TEXT
        dialogBuilder.setView(input)

        dialogBuilder.setPositiveButton("Search") { _, _ ->
            val searchQuery = input.text.toString().trim()

            val filteredStudents = iStudentService?.search(searchQuery)
            if (filteredStudents != null) {
                studentAdapter.setStudent(filteredStudents)
            }

            if (filteredStudents != null) {
                if (filteredStudents.isEmpty()) {
                    Toast.makeText(this, "Empty students", Toast.LENGTH_SHORT).show()
                }
            }
        }

        dialogBuilder.setNegativeButton("Cancel") { dialog, _ ->
            dialog.dismiss()
        }

        val dialog = dialogBuilder.create()
        dialog.show()
    }

    private fun sortByAlpha(students: List<Student>) {
        val sortedStudents = students.sortedBy { it.name }
        studentAdapter.setStudent(sortedStudents)
    }

    private fun showStudentInfo(iStudentService: IStudentService, student: Student) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Contact Information")
        builder.setMessage("ID: ${student.id}" +
                            "\nName: ${student.name}" +
                            "\nAge: ${student.age}" +
                            "\nClass: ${student.studentClass}" +
                            "\nGPA: ${student.gpa}")

        builder.setIcon(student.image)
        builder.setPositiveButton("Edit") { dialog, _ ->
            showEditDialog(iStudentService, student)
            dialog.dismiss()
        }

        builder.setNegativeButton("Cancel") { dialog, _ ->
            dialog.dismiss()
        }

        builder.show()
    }

    private fun showEditDialog(iStudentService: IStudentService, student: Student) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Edit Student")

        val view = layoutInflater.inflate(R.layout.dialog_edit_student, null)
        val etName = view.findViewById<EditText>(R.id.etEditedName)
        val etAge = view.findViewById<EditText>(R.id.etEditedAge)
        val etClass = view.findViewById<EditText>(R.id.etEditedClass)
        val etGpa = view.findViewById<EditText>(R.id.etEditedGpa)

        etName.setText(student.name)
        etAge.setText(student.age.toString())
        etClass.setText(student.studentClass)
        etGpa.setText(student.gpa.toString())

        builder.setView(view)

        builder.setPositiveButton("Save") { dialog, _ ->
            val newName = etName.text.toString()
            val newAge = etAge.text.toString()
            val newClass = etClass.text.toString()
            val newGpa = etGpa.text.toString()

            val success = iStudentService?.update(student)

            if (success == true) {
                Toast.makeText(this, "Student updated successfully", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Failed to update student", Toast.LENGTH_SHORT).show()
            }

            loadStudents(iStudentService)
            dialog.dismiss()
        }

        builder.setNegativeButton("Cancel") { dialog, _ ->
            dialog.dismiss()
        }

        builder.show()
    }

    private fun showDeleteDialog(iStudentService: IStudentService) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("DELETE STUDENTS")

        val view = layoutInflater.inflate(R.layout.dialog_delete_student, null)
        val etId = view.findViewById<EditText>(R.id.etDeletedId)
        val etName: EditText = view.findViewById(R.id.etDeletedName)

        builder.setView(view)

        builder.setPositiveButton("Delete") { _, _ ->
            val name = etName.text.toString()
            val id = etId.text.toString()

            iStudentService.delete(id)

            loadStudents(iStudentService)
        }

        builder.setNegativeButton("Cancel") { dialog, _ ->
            dialog.dismiss()
        }

        builder.show()
    }

    private fun loadStudents(iStudentService: IStudentService) {
        if (students.isEmpty()) {
            Toast.makeText(this, "Empty students", Toast.LENGTH_SHORT).show()
        }

        students = iStudentService.all
        studentAdapter.setStudent(students)
    }
}

