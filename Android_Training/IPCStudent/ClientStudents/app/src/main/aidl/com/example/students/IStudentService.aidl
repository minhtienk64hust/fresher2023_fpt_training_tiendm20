package com.example.students;

parcelable Student;

interface IStudentService {
    List<Student> getAll();
    void create(in Student student);
    boolean update(in Student student);
    void delete(String id);
    List<Student> sortByName();
    List<Student> sortByGPA();
    List<Student> search(in String query);
}