package com.example.orderapp.cart.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.orderapp.FoodApplication
import com.example.orderapp.R
import com.example.orderapp.databinding.ActivityCartBinding
import com.example.orderapp.models.CartItem
import com.example.orderapp.viewmodels.CartViewModel

class CartActivity : AppCompatActivity() {
    private lateinit var activityCartBinding: ActivityCartBinding

    private val cartViewModel: CartViewModel by lazy {
        (application as FoodApplication).cartViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityCartBinding = ActivityCartBinding.inflate(layoutInflater)
        setContentView(activityCartBinding.root)

        activityCartBinding.rcvCartItem.layoutManager = LinearLayoutManager(this)

        val cartAdapter: CartAdapter = CartAdapter()
        cartAdapter.cartItemQuantityChange = object : CartItemQuantityChange{
            override fun changeQuantityCartItem(cartItem: CartItem) {
                cartViewModel.updateQuantityCartItem(cartItem)
                activityCartBinding.totalPrice.text = cartViewModel.calculateTotalPrice().toString()
            }
        }

        cartViewModel.cartItems.observe(this) { cartItems ->
            cartAdapter.cartItemList = cartItems
            cartAdapter.notifyDataSetChanged()

        }
        activityCartBinding.rcvCartItem.adapter = cartAdapter
        activityCartBinding.totalPrice.text = cartViewModel.calculateTotalPrice().toString()


        activityCartBinding.toolbar.setNavigationOnClickListener {
            finish()
        }


    }


}