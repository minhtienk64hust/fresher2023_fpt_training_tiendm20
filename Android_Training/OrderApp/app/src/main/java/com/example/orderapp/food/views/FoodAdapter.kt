package com.example.orderapp.food.views
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.orderapp.FoodProvider
import com.example.orderapp.R
import com.example.orderapp.models.CartItem
import com.example.orderapp.models.Food
import com.google.android.material.button.MaterialButton

interface CartInteractionListener {
    fun onAddToCartClicked(cartItem: CartItem)
}

//interface CartInteractionListener {
//    // có thể trả về thẳng int nhưng không như thế không rõ nghĩa
//    fun onAddToCartClicked(cartItem: CartItem): FoodItemQuantityRetrieve
//}

interface FoodItemQuantityRetrieve {
    fun retrieveQuantityForFoodItem(): Int
}

//interface FoodItemQuantityDisplay {
//    fun showQuantityForFoodItem(quantity: Int)
//}

class FoodAdapter() : RecyclerView.Adapter<FoodAdapter.ViewHolder>() {
    private val foodList: List<Food>

    private var cartListener: CartInteractionListener? = null
    fun setCartListener(listener: CartInteractionListener) {
        cartListener = listener
    }

    init {
        foodList = FoodProvider.foodList
    }

    private val foodListWithQuantity: MutableList<Pair<Food, Int>>
    init {
        foodListWithQuantity = foodList.associate { food -> Pair(food, 0) }.toList().toMutableList()
    }

    fun setFoodListWithQuantity(cartItems: List<CartItem>) {
        cartItems.map {cartItem ->
            // Tìm food tương ứng với cartItems
            val foodIndex = foodListWithQuantity.indexOfFirst { it.first == cartItem.food && it.second != cartItem.quantity}
            if (foodIndex != -1) {
                val existingPair = foodListWithQuantity[foodIndex]
                Log.d("TAG", existingPair.first.name)
                Log.d("TAG", foodIndex.toString())
                foodListWithQuantity[foodIndex] = Pair(existingPair.first, cartItem.quantity)
            }
        }

        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_food, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        val food = foodList[position]
//
//        // Bind data to the views
//        holder.textViewName.text = food.name
//        holder.textViewDescription.text = food.description
//        holder.textViewPrice.text = "Giá: ${food.price}"

        val foodWithQuantityPair: Pair<Food, Int> = foodListWithQuantity[position]
        val (food, quantity) = foodWithQuantityPair
        holder.textViewName.text = food.name
        holder.textViewDescription.text = food.description
        holder.textViewPrice.text = "Giá: ${food.price}"
        if(quantity>=1) {
            holder.textViewQuantity.visibility = View.VISIBLE
            holder.textViewQuantity.text = "x${quantity}"
        }
        // không có else sẽ bị lỗi cache
        else {
            holder.textViewQuantity.visibility = View.INVISIBLE
            holder.textViewQuantity.text = "x0"
        }



        // Set click listeners for increase and decrease buttons
//        holder.buttonIncrease.setOnClickListener {
//            val quantity = holder.textViewQuantity.text.toString().toInt()
//            holder.textViewQuantity.text = (quantity + 1).toString()
//        }
//
//        holder.buttonDecrease.setOnClickListener {
//            val quantity = holder.textViewQuantity.text.toString().toInt()
//            holder.textViewQuantity.text = if(quantity > 0) (quantity-1).toString() else 0.toString()
//
//        }

        holder.btnAddToCart.setOnClickListener {
            val cartItem = CartItem(food, quantity = 1)
//            // có thể trả về thẳng int nhưng không như thế không rõ nghĩa
//            val quantity = cartListener?.onAddToCartClicked(cartItem)?.retrieveQuantityForFoodItem()
//            holder.textViewQuantity.visibility = View.VISIBLE
//            holder.textViewQuantity.text = "x${quantity}"

            cartListener?.onAddToCartClicked(cartItem)

        }
    }

    override fun getItemCount(): Int {
        return foodListWithQuantity.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewName: TextView = itemView.findViewById(R.id.textViewName)
        val textViewDescription: TextView = itemView.findViewById(R.id.textViewDescription)
        val textViewPrice: TextView = itemView.findViewById(R.id.textViewPrice)
        val textViewQuantity: TextView = itemView.findViewById(R.id.textViewQuantity)
        val btnAddToCart = itemView.findViewById<MaterialButton>(R.id.btnAddToCart)
    }

}


