package com.example.orderapp.cart.views

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.orderapp.R
import com.example.orderapp.models.CartItem
import com.google.android.material.button.MaterialButton

interface CartItemQuantityChange {
    fun changeQuantityCartItem(cartItem: CartItem)
}

class CartAdapter(var cartItemList: List<CartItem> = mutableListOf<CartItem>()) : RecyclerView.Adapter<CartAdapter.ViewHolder>() {

    var cartItemQuantityChange: CartItemQuantityChange? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cart_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cartItem = cartItemList[position]

        holder.nameOfCItem.text = cartItem.food.name
        holder.originPriceOfCItem.text = "Giá gốc: ${cartItem.food.price}đ"
        holder.txtQuantity.text = cartItem.quantity.toString()
        holder.totalPriceOfCItem.text = "Giá: ${cartItem.food.price * cartItem.quantity}đ"

        holder.btnIncrease.setOnClickListener {
            val newQuantity = holder.txtQuantity.text.toString().toInt() + 1
            // update qua viewmodel
//            holder.txtQuantity.text = newQuantity.toString()
//            holder.totalPriceOfCItem.text = "Giá gốc: ${cartItem.food.price * newQuantity}đ"
            val newCartItem = cartItem.copy(quantity = newQuantity)
            cartItemQuantityChange?.changeQuantityCartItem(newCartItem)
        }

        holder.btnDecrease.setOnClickListener {
            val oldQuantity = holder.txtQuantity.text.toString().toInt()
            val newQuantity = if(oldQuantity>1) (oldQuantity - 1) else 1
//            holder.txtQuantity.text = newQuantity.toString()
//            holder.totalPriceOfCItem.text = "Giá: ${cartItem.food.price * newQuantity}đ"
            val newCartItem = cartItem.copy(quantity = newQuantity)
            cartItemQuantityChange?.changeQuantityCartItem(newCartItem)

        }
    }

    override fun getItemCount(): Int {
        return cartItemList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameOfCItem: TextView = itemView.findViewById(R.id.nameOfCItem)
        val originPriceOfCItem: TextView = itemView.findViewById(R.id.originPriceOfCItem)
        val txtQuantity: TextView = itemView.findViewById(R.id.txtQuantity)
        val totalPriceOfCItem: TextView = itemView.findViewById(R.id.totalPriceOfCItem)
        val btnIncrease: MaterialButton = itemView.findViewById(R.id.btnIncrease)
        val btnDecrease: MaterialButton = itemView.findViewById(R.id.btnDecrease)
    }
}

class CartDiffCallback(
    private val oldCartItems: List<CartItem>,
    private val newCartItems: List<CartItem>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldCartItems.size
    }

    override fun getNewListSize(): Int {
        return newCartItems.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldCartItems[oldItemPosition].food == newCartItems[newItemPosition].food
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldCartItems[oldItemPosition] == newCartItems[newItemPosition]
    }
}
