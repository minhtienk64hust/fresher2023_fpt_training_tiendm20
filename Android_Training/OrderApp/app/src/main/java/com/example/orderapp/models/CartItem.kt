package com.example.orderapp.models
data class CartItem(val food: Food, val quantity: Int)