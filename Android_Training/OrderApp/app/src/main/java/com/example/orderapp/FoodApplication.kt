package com.example.orderapp

import android.app.Application
import com.example.orderapp.models.Food
import com.example.orderapp.viewmodels.CartViewModel

class FoodApplication: Application() {
    val cartViewModel: CartViewModel by lazy {
        CartViewModel()
    }

    override fun onCreate() {
        super.onCreate()
    }
}

object FoodProvider {
    val foodList: List<Food>
        get() {
            return listOf(
                Food(name = "Cơm gà", description = "Cơm gà là một món ăn phổ biến ở Việt Nam, được làm từ cơm và thịt gà. Cơm gà thường được ăn với nước mắm chua ngọt và dưa chua.", price = 50000.0),
                Food(name = "Phở", description = "Phở là một món ăn truyền thống của Việt Nam, được làm từ bánh phở, thịt bò hoặc thịt gà, và nước dùng xương hầm. Phở thường được ăn với rau thơm và chanh.", price = 60000.0),
                Food(name = "Bún chả", description = "Bún chả là một món ăn đặc sản của Hà Nội, được làm từ bún, thịt nướng, và nước chấm. Bún chả thường được ăn với rau sống và dưa góp.", price = 70000.0),
                Food(name = "Bánh mì", description = "Bánh mì là một món ăn phổ biến ở Việt Nam, được làm từ bánh mì baguette, thịt, rau, và nước sốt. Bánh mì thường được ăn vào bữa sáng hoặc bữa trưa.", price = 30000.0),
                Food(name = "Bún bò Huế", description = "Bún bò Huế là một món ăn đặc sản của Huế, được làm từ bún, thịt bò, và nước dùng xương hầm. Bún bò Huế thường được ăn với rau thơm và chanh.", price = 80000.0),
                Food(name = "Cơm tấm", description = "Cơm tấm là một món ăn phổ biến ở miền Nam Việt Nam, được làm từ cơm tấm, sườn nướng, và bì chả. Cơm tấm thường được ăn với nước mắm chua ngọt và dưa chua.", price = 40000.0),
                Food(name = "Hủ tiếu", description = "Hủ tiếu là một món ăn phổ biến ở miền Nam Việt Nam, được làm từ hủ tiếu, thịt heo, và nước dùng xương hầm. Hủ tiếu thường được ăn với rau thơm và chanh.", price = 50000.0),
                Food(name = "Mì Quảng", description = "Mì Quảng là một món ăn đặc sản của Quảng Nam, được làm từ mì Quảng, thịt heo, và tôm. Mì Quảng thường được ăn với rau sống và nước chấm.", price = 60000.0),
                Food(name = "Bún cá", description = "Bún cá là một món ăn đặc sản của miền Trung Việt Nam, được làm từ bún, cá, và nước dùng xương hầm. Bún cá thường được ăn với rau thơm và chanh.", price = 70000.0),
                Food(name = "Cao lầu", description = "Cao lầu là một món ăn đặc sản của Hội An, được làm từ cao lầu, thịt heo, và tôm. Cao lầu thường được ăn với rau sống và nước chấm.", price = 80000.0)
            )
        }
}