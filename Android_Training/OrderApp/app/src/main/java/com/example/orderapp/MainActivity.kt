package com.example.orderapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import com.example.orderapp.databinding.ActivityMainBinding
import com.example.orderapp.food.views.FoodFragment

// lưu trữ cho food -> thực chất là FoodActivity
class MainActivity : AppCompatActivity() {
    private lateinit var activityMainBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(activityMainBinding.root)

        val fragmentManager: FragmentManager = supportFragmentManager

        val foodFragment = FoodFragment()

        fragmentManager.beginTransaction().add(R.id.host_fragment, foodFragment).commit()


    }
}
