package com.example.orderapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.orderapp.models.CartItem
import com.example.orderapp.models.Food

class CartViewModel: ViewModel() {
    private var _cartItems = MutableLiveData<List<CartItem>>(mutableListOf<CartItem>())
    val cartItems: LiveData<List<CartItem>>
        get() = _cartItems

    private val _itemCount: MutableLiveData<Int> = MutableLiveData<Int>(_cartItems.value?.size ?: 0)
    val itemCount: LiveData<Int>
        get() = _itemCount

    fun addToCart(item: CartItem) {
        val currentCart = _cartItems.value.orEmpty().toMutableList()
        val existFood = currentCart.find { it.food.name == item.food.name }
        if (existFood != null) {
            // cập nhật quantity của existFood lên 1
            val indexOfFood = currentCart.indexOf(existFood)
            val updatedFood = existFood?.copy(quantity = existFood.quantity + 1)
            currentCart[indexOfFood] = updatedFood!!
            // Viết code phần này cho tôi
        } else {
            // nếu chưa có thì thêm item đó vào với quantity là 1
            currentCart.add(item)
        }

        _cartItems.value = currentCart
        _itemCount.value = _cartItems.value?.size ?: 0
    }


    fun removeFromCart(item: CartItem) {
        val currentCart = _cartItems.value.orEmpty().toMutableList()
        currentCart.remove(item)
        _cartItems.value = currentCart
        _itemCount.value = _cartItems.value?.size ?: 0
    }

    fun calculateTotalPrice(): Double {
        val cartItems = _cartItems.value.orEmpty()
        var totalPrice = 0.0

        for (cartItem in cartItems) {
            totalPrice += cartItem.food.price * cartItem.quantity
        }

        return totalPrice
    }

    fun getQuantityPerFoodItem(food: Food): Int {
        val currentCart = _cartItems.value.orEmpty().toMutableList()
        val existFood = currentCart.find { it.food.name == food.name }
        return if(existFood!=null) return existFood.quantity else 0
    }

    // update với quantity min là 1
    fun updateQuantityCartItem(newItem: CartItem) {
        val currentCart = _cartItems.value.orEmpty().toMutableList()
        val existFood = currentCart.find { it.food.name == newItem.food.name }
        if (existFood != null) {
            // cập nhật quantity của existFood lên quantity
            val indexOfFood = currentCart.indexOf(existFood)
            val updatedFood = existFood?.copy(quantity = newItem.quantity)
            currentCart[indexOfFood] = updatedFood!!
        } else {
            // nếu chưa có thì thêm item đó vào với quantity là quantity
            currentCart.add(newItem.copy(quantity = newItem.quantity))
        }

        _cartItems.value = currentCart
        _itemCount.value = _cartItems.value?.size ?: 0
    }

}