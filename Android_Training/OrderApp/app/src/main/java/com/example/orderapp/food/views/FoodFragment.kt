package com.example.orderapp.food.views

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.orderapp.FoodApplication
import com.example.orderapp.R
import com.example.orderapp.cart.views.CartActivity
import com.example.orderapp.databinding.FragmentFoodBinding
import com.example.orderapp.models.CartItem
import com.example.orderapp.viewmodels.CartViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior

class FoodFragment : Fragment() {
    private lateinit var fragmentFoodFragmentBinding: FragmentFoodBinding

    private val cartViewModel: CartViewModel by lazy {
        (activity?.application as FoodApplication).cartViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentFoodFragmentBinding = FragmentFoodBinding.inflate(inflater, container, false)
        return fragmentFoodFragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val standardBottomSheet = fragmentFoodFragmentBinding.standardBottomSheet
        val standardBottomSheetBehavior = BottomSheetBehavior.from(standardBottomSheet)
        standardBottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        standardBottomSheetBehavior.isDraggable = false

        val txtCartCount = standardBottomSheet.findViewById<TextView>(R.id.txtCartCount)
        val txtTotalPrice = standardBottomSheet.findViewById<TextView>(R.id.txtTotalPrice)

        val foodAdapter = FoodAdapter()
        fragmentFoodFragmentBinding.rcvFood.layoutManager = LinearLayoutManager(context)
        fragmentFoodFragmentBinding.rcvFood.adapter = foodAdapter

        foodAdapter.setCartListener(object: CartInteractionListener {
            override fun onAddToCartClicked(cartItem: CartItem) {
//                Log.d("TAG", "onAddToCartClicked: ")
                cartViewModel.addToCart(cartItem)
                val totalPrice = cartViewModel.calculateTotalPrice()
                txtTotalPrice.text = "${totalPrice}đ"

//                val quantity = cartViewModel.getQuantityPerFoodItem(food = cartItem.food)
//                return object : FoodItemQuantityRetrieve{
//                    override fun retrieveQuantityForFoodItem(): Int {
//                        return quantity
//                    }
//
//                }
            }
        })




        cartViewModel.cartItems.observe(viewLifecycleOwner) { cartItems ->
            Log.d("TAG", "Change")
            if(cartItems.size > 0) {
                foodAdapter.setFoodListWithQuantity(cartItems)
            }
            val totalPrice = cartViewModel.calculateTotalPrice()
            txtTotalPrice.text = "${totalPrice}đ"
        }

        cartViewModel.itemCount.observe(viewLifecycleOwner) {itemCount ->
            if(itemCount>0) {
                standardBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                txtCartCount.text = "${itemCount} món"
            } else {
                standardBottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            }
        }

        standardBottomSheet.setOnClickListener {
            startActivity(Intent(activity, CartActivity::class.java))
        }

    }
}