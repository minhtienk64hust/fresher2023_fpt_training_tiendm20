package com.example.orderapp.models

data class Food(val name: String, val description: String, val price: Double)
