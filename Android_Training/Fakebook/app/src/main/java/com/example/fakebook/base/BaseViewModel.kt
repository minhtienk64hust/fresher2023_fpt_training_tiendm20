package com.example.fakebook.base

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.AndroidViewModel
import com.example.fakebook.data.model.UserInformation
import com.example.fakebook.data.repository.UserRepository
import com.example.fakebook.data.repository.source.UserDataSourceImpl
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

open class BaseViewModel(application: Application): AndroidViewModel(application) {
    private val list = listOf(
        UserInformation(name = "TienDM20", email = "tiendm20@fpt.com", password = "1111"),
        UserInformation(name = "TienDM21", email = "tiendm21@fpt.com", password = "1111"),
    )

    private val mExecutor = Executors.newSingleThreadExecutor()
    val executor: ExecutorService
        get() = mExecutor



    private val mRepository by lazy {
        UserRepository.getInstance(
            UserDataSourceImpl.getInstance(application.applicationContext)
        )
    }
    val repository: UserRepository
        get() = mRepository



    private val mSharedPreferences by lazy {
        application.applicationContext.getSharedPreferences("my app", Context.MODE_PRIVATE)
    }
    val sharedPreferences: SharedPreferences
        get() = mSharedPreferences



    init {
        mExecutor.execute {
            if (mRepository.getUserCount() < 1) {
                list.forEach {
                    mRepository.insertUser(it)
                }

                mSharedPreferences.edit().putLong(KEY, 1).apply()
            }
        }
    }

    companion object {
        val KEY = "1111"
    }
}
