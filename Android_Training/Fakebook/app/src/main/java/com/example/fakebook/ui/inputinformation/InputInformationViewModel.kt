package com.example.fakebook.ui.inputinformation

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.example.fakebook.base.BaseViewModel

class InputInformationViewModel(application: Application): BaseViewModel(application) {
    val account = MutableLiveData<String>()

    val password = MutableLiveData<String>()

    fun login(listener: (Boolean) -> Unit) {
        val account = account.value?.toString() ?: ""
        val password = password.value?.toString() ?: ""
        executor.execute {
            val user = repository.getUser(account, password)
            if (user != null) {
                sharedPreferences.edit().putLong(KEY, user.id).apply()
                listener.invoke(true)
            } else {
                listener.invoke(false)
            }
        }
    }
}
