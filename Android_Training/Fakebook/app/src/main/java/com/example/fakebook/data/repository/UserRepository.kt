package com.example.fakebook.data.repository

import com.example.fakebook.data.repository.source.UserDataSource

class UserRepository private constructor(
    private val source: UserDataSource
) : UserDataSource by source {
    companion object {
        private var instance: UserRepository? = null
        fun getInstance(source: UserDataSource) = synchronized(this) {
            instance ?: UserRepository(source)
        }
    }
}
