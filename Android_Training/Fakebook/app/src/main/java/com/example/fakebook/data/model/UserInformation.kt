package com.example.fakebook.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.fakebook.data.database.UserContract

@Entity(tableName = UserContract.TABLE_NAME)
data class UserInformation(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = UserContract.ID_COLUMN)
    val id: Long = 0,
    @ColumnInfo(name = UserContract.NAME_COLUMN)
    val name: String = "Anonymous",
    @ColumnInfo(name = UserContract.EMAIL_COLUMN)
    val email: String = "Anonymous",
    @ColumnInfo(name = UserContract.PASSWORD_COLUMN)
    val password: String = "Anonymous",
)
