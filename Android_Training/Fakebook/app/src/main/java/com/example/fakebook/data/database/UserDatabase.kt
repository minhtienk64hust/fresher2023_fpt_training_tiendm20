package com.example.fakebook.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.fakebook.data.model.UserInformation

@Database(entities = [UserInformation::class], version = 1)
abstract class UserDatabase: RoomDatabase() {
    abstract fun userDao(): UserDao
}
