package com.example.fakebook.ui.inputinformation

import android.graphics.Rect
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.fakebook.R
import com.example.fakebook.base.BaseFragment
import com.example.fakebook.databinding.FragmentInputInformationBinding

class InputInformationFragment : BaseFragment<FragmentInputInformationBinding>(
    FragmentInputInformationBinding::inflate
) {
    private val viewModel by lazy {
        ViewModelProvider(this)[InputInformationViewModel::class.java]
    }

    override fun initData() {
        binding.vm = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
    }

    override fun initView() {
        binding.root.viewTreeObserver.addOnGlobalLayoutListener {
            val rect = Rect()

            binding.root.getWindowVisibleDisplayFrame(rect)

            val screenHeight: Int = binding.root.rootView.height
            val keypadHeight = screenHeight - rect.bottom

            val visibility = if (keypadHeight > screenHeight * 0.15) {
                View.GONE
            } else {
                View.VISIBLE
            }
            binding.ivLogo.visibility = visibility
        }

        binding.tvForgot.setOnClickListener {
            navigate(R.id.action_inputInformationFragment_to_forgotPasswordFragment)
        }

        binding.btnLogin.setOnClickListener {
//            viewModel.login { hasUserAcc ->
//                Handler(Looper.getMainLooper()).post {
//                    if (hasUserAcc) findNavController().popBackStack()
//                    else makeToast("wrong password")
//                }
//            }

            viewModel.login {
                Handler(Looper.getMainLooper()).post {
                    if (it) findNavController().popBackStack()
                    else makeToast("wrong password")
                }
            }
        }
    }
}
