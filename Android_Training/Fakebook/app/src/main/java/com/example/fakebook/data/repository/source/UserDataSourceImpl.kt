package com.example.fakebook.data.repository.source

import android.content.Context
import androidx.room.Room
import com.example.fakebook.data.database.UserDatabase
import com.example.fakebook.data.model.UserInformation

class UserDataSourceImpl private constructor(
    private val db: UserDatabase
): UserDataSource {
    override fun getUserCount() = db.userDao().getUserCount()
    override fun getUser(email: String, password: String) = db.userDao().getUser(email, password)
    override fun insertUser(user: UserInformation) = db.userDao().insert(user)
    override fun getUserById(id: Long) = db.userDao().getUserById(id)

    companion object {
        private var instance: UserDataSourceImpl? = null

        fun getInstance(context: Context) = synchronized(this) {
            if (instance != null) instance
            else {
                val db = Room.databaseBuilder(
                    context,
                    UserDatabase::class.java,
                    "user_database"
                ).build()

                UserDataSourceImpl(db).also { instance = it }
            }!!
        }
    }
}
