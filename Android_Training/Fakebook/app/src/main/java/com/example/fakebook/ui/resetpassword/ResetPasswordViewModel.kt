package com.example.fakebook.ui.resetpassword

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.example.fakebook.base.BaseViewModel


class ResetPasswordViewModel(application: Application): BaseViewModel(application) {
    val password = MutableLiveData("")

    fun updatePassword() {
        executor.execute {
            val id = sharedPreferences.getLong(KEY, -1)
            val user = repository.getUserById(id)

            val isPasswordValid = password.value != null && password.value!!.isNotBlank()
            if (user != null && isPasswordValid) {
                repository.insertUser(user.copy(password = password.value!!))
            }
        }
    }
}
