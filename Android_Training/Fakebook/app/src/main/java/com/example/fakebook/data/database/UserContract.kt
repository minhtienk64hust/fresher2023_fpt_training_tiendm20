package com.example.fakebook.data.database

import android.provider.BaseColumns

object UserContract: BaseColumns {
    const val TABLE_NAME = "users"
    const val ID_COLUMN = "_id"
    const val NAME_COLUMN = "name"
    const val EMAIL_COLUMN = "email"
    const val PASSWORD_COLUMN = "password"
}
