package com.example.fakebook.ui.resetpassword

import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.fakebook.R
import com.example.fakebook.base.BaseFragment
import com.example.fakebook.databinding.FragmentResetPasswordBinding
import com.example.fakebook.ui.resetpassword.ResetPasswordViewModel

class ResetPasswordFragment : BaseFragment<FragmentResetPasswordBinding>(
    FragmentResetPasswordBinding::inflate
) {
    private val viewModel by lazy {
        ViewModelProvider(this)[ResetPasswordViewModel::class.java]
    }

    override fun initData() {
        binding.vm = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
    }

    override fun initView() {
        binding.btnContinue.setOnClickListener { reset() }

        binding.toolbarMain.setNavigationOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun reset() {
        if (binding.edtCode.text.toString().isNotBlank()) {
            viewModel.updatePassword()
            navigate(R.id.action_resetPasswordFragment_to_loginFragment)
        }
        else
            makeToast("Fill new password")
    }
}
