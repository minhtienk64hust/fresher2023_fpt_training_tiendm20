package com.example.fakebook.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.fakebook.data.model.UserInformation

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: UserInformation): Long

    @Query("SELECT COUNT(*) FROM ${UserContract.TABLE_NAME}")
    fun getUserCount(): Int

    @Query("SELECT * FROM ${UserContract.TABLE_NAME} WHERE ${UserContract.EMAIL_COLUMN} = :email AND ${UserContract.PASSWORD_COLUMN} = :password")
    fun getUser(email: String, password: String): UserInformation?

    @Query("SELECT * FROM users WHERE ${UserContract.ID_COLUMN} = :userId")
    fun getUserById(userId: Long): UserInformation?
}
