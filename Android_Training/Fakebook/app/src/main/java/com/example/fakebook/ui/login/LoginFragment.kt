package com.example.fakebook.ui.login

import androidx.lifecycle.ViewModelProvider
import com.example.fakebook.R
import com.example.fakebook.base.BaseFragment
import com.example.fakebook.databinding.FragmentLoginBinding
import com.example.fakebook.ui.login.LoginViewModel


class LoginFragment : BaseFragment<FragmentLoginBinding>(FragmentLoginBinding::inflate) {
    private val viewModel by lazy {
        ViewModelProvider(this)[LoginViewModel::class.java]
    }

    override fun initData() {
        binding.vm = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        viewModel.getCurrentUser()
    }

    override fun initView() {
        binding.tvAnotherAccount.setOnClickListener {
            navigate(R.id.action_loginFragment_to_inputInformationFragment)
        }
        binding.tvFind.setOnClickListener {
            navigate(R.id.action_loginFragment_to_forgotPasswordFragment)
        }
    }
}
