package com.example.fakebook.ui.splash

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.core.app.ActivityOptionsCompat
import com.example.fakebook.MainActivity
import com.example.fakebook.R


@SuppressLint("CustomSplashScreen")
class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler(Looper.getMainLooper()).postDelayed({
            val options = ActivityOptionsCompat
                .makeSceneTransitionAnimation(this)
                .toBundle()
            val intent = Intent(this, MainActivity::class.java)

            startActivity(intent, options)
        }, 300)
    }
}
