package com.example.fakebook.ui.forgotpassword

import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.fakebook.R
import com.example.fakebook.base.BaseFragment
import com.example.fakebook.databinding.FragmentForgotPasswordBinding
import com.example.fakebook.ui.login.LoginViewModel
import com.example.fakebook.util.toHtmlSpan


class ForgotPasswordFragment : BaseFragment<FragmentForgotPasswordBinding>(
    FragmentForgotPasswordBinding::inflate
) {
    private val viewModel by lazy {
        ViewModelProvider(this)[LoginViewModel::class.java]
    }

    override fun initData() {
        viewModel.getCurrentUser()
    }

    override fun initView() {
        binding.btnContinue.setOnClickListener {
            if (binding.edtCode.text.toString().isNotBlank())
                navigate(R.id.action_forgotPasswordFragment_to_resetPasswordFragment)
            else
                makeToast("Fill code please")
        }

        binding.toolbarMain.setNavigationOnClickListener {
            findNavController().popBackStack()
        }

        viewModel.user.observe(this) {
            binding.tvMessage.text = getString(R.string.message, it.email).toHtmlSpan()
        }
    }

}
