package com.example.fakebook.ui.login

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.fakebook.base.BaseViewModel
import com.example.fakebook.data.model.UserInformation


class LoginViewModel(application: Application): BaseViewModel(application) {
    private val mUser = MutableLiveData(UserInformation())
    val user: LiveData<UserInformation>
        get() = mUser

    fun getCurrentUser() {
        executor.execute {
            val id = sharedPreferences.getLong(KEY, -1)
            val user = repository.getUserById(id)
            mUser.postValue(user)
        }
    }
}
