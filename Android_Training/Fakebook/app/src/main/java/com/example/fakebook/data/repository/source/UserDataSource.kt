package com.example.fakebook.data.repository.source

import com.example.fakebook.data.model.UserInformation

interface UserDataSource {
    fun getUserCount(): Int

    fun getUser(email: String, password: String): UserInformation?

    fun insertUser(user: UserInformation): Long

    fun getUserById(id: Long): UserInformation?
}
