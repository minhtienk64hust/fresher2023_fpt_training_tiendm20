package com.example.simplebmi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.simplebmi.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
//    lateinit var weightEdt: EditText
//    lateinit var heightEdt: EditText
//    lateinit var calculateBMIBtn: Button
    lateinit var mainBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
//        weightEdt = findViewById<EditText>(R.id.weight_edt)
//        heightEdt = findViewById<EditText>(R.id.height_edt)
//        calculateBMIBtn = findViewById<Button>(R.id.calculate_bmi_btn)

        mainBinding = ActivityMainBinding.inflate(layoutInflater)
        val view = mainBinding.root
        setContentView(view)

        mainBinding.calculateBmiBtn.setOnClickListener {
            calculateBMI()
        }
    }

    private fun calculateBMI() {
        var weight = mainBinding.weightEdt.text.toString().toDoubleOrNull()
        var height = mainBinding.heightEdt.text.toString().toDoubleOrNull()

        if (weight != null && height != null) {
            var bmi = weight / (height/100 * height/100)
            mainBinding.resultTxt.text = "${resources.getString(R.string.result)}: ${bmi.toInt()}"
        } else {
            Toast.makeText(this@MainActivity, "Không được để trống", Toast.LENGTH_LONG).show()
        }
    }
}