package com.example.navigationactivitydemo

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import com.example.navigationactivitydemo.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.yourName.setOnClickListener {
            val intentName = Intent(this, InputValueActivity::class.java)
            intentName.putExtra("TYPE", "TYPE_NAME")
            myActivityResultLauncher.launch(intentName)
        }

        binding.yourAge.setOnClickListener {
            val intentAge= Intent(this, InputValueActivity::class.java)
            intentAge.putExtra("TYPE", "TYPE_AGE")
            myActivityResultLauncher.launch(intentAge)
        }

        binding.yourMajor.setOnClickListener {
            val intentMajor = Intent(this, InputValueActivity::class.java)
            intentMajor.putExtra("TYPE", "TYPE_MAJOR")
            myActivityResultLauncher.launch(intentMajor)
        }

        binding.yourFavourite.setOnClickListener {
            val intentFavourite = Intent(this, InputValueActivity::class.java)
            intentFavourite.putExtra("TYPE", "TYPE_FAVOURITE")
            myActivityResultLauncher.launch(intentFavourite)
        }
    }

    private val myActivityResultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if(result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            val returnValue = data?.getStringExtra("RETURN_INPUT_VALUE")
            val type = data?.getStringExtra("TYPE")
            when (type) {
                "TYPE_NAME" -> {
                    binding.yourName.text = returnValue
                }
                "TYPE_AGE" -> {
                    binding.yourAge.text = returnValue
                }
                "TYPE_MAJOR" -> {
                    binding.yourMajor.text = returnValue
                }
                "TYPE_FAVOURITE" -> {
                    binding.yourFavourite.text = returnValue
                }

            }

        }
    }
}