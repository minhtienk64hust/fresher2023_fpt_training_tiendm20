package com.example.navigationactivitydemo

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.navigationactivitydemo.databinding.ActivityInputValueBinding


class InputValueActivity : AppCompatActivity() {
    private lateinit var activityInputValueBinding: ActivityInputValueBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityInputValueBinding = ActivityInputValueBinding.inflate(layoutInflater)
        setContentView(activityInputValueBinding.root)

        setSupportActionBar(activityInputValueBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true);
        supportActionBar?.setDisplayShowHomeEnabled(true);
        activityInputValueBinding.toolbar.setNavigationOnClickListener { finish() }


        activityInputValueBinding.submit.setOnClickListener {
            checkAndReturnValue()
        }

        activityInputValueBinding.inputValue.editText?.addTextChangedListener(
            object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                }

                override fun afterTextChanged(s: Editable?) {
                    if (s.toString().isNotEmpty())
                        activityInputValueBinding.submit.isEnabled = true
                    else
                        activityInputValueBinding.submit.isEnabled = false
                }

            }
        )


    }

    private fun checkAndReturnValue() {
        val type = intent.getStringExtra("TYPE")

        // edit text đã khác rỗng mới check tiếp
        val returnValue = activityInputValueBinding.inputValue.editText?.text.toString()

        val validReturnValue = when(type) {
            "TYPE_NAME" -> validateName(returnValue)
            "TYPE_AGE" -> validateAge(returnValue)
            else -> true
        }

        if (validReturnValue) {
            val returnIntent = Intent()
            returnIntent.putExtra("RETURN_INPUT_VALUE", returnValue)
            returnIntent.putExtra("TYPE", type)
            setResult(Activity.RESULT_OK, returnIntent)

            finish()
        } else {
            activityInputValueBinding.inputValue.error = "Không hợp lệ"
        }


    }

    private fun validateName(name: String): Boolean {
        val regex = "^[a-zA-Z ]+$"
        return name.matches(Regex(regex))
    }

    private fun validateAge(age: String): Boolean {
        return age.toIntOrNull() in 1..100
    }


}