package com.example.musicplayer

import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.ServiceConnection
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.registerReceiver
import androidx.core.os.postDelayed
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.button.MaterialButton
import com.google.android.material.progressindicator.LinearProgressIndicator
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SongFragment() : Fragment(), SongAdapterListener {
    var mSongList: ArrayList<Song>? = arrayListOf<Song>()
    private lateinit var rootView: View
    private lateinit var standardBottomSheet: View
    private lateinit var ibtnPlayPause: Button
    private lateinit var progressMusic: LinearProgressIndicator
    var mediaPlayer: MediaPlayer? = null
    var musicService: MusicService? = null
    var connection: ServiceConnection? = null
    var job: Job? = null
    var currentSongSelected: Int = -1


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        Log.d("Tag", "onCreateView")
        rootView = inflater.inflate(R.layout.fragment_song, container, false)

        arguments?.let {
            mSongList = it.getParcelableArrayList<Song>("mSongList")
            // Log.d("TAG3", mSongList?.size.toString())

        }

        val recyclerView = rootView.findViewById<RecyclerView>(R.id.rv_song)
        recyclerView.layoutManager = LinearLayoutManager(context)
        val songAdapter = SongAdapter(mSongList)
        songAdapter.setSongAdapterListener(this)
        recyclerView.adapter = songAdapter

        mediaPlayer = MediaPlayer()

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d("Tag", "onViewCreated")
        super.onViewCreated(view, savedInstanceState)
        standardBottomSheet = view.findViewById<View>(R.id.standard_bottom_sheet)
        val standardBottomSheetBehavior = BottomSheetBehavior.from(standardBottomSheet)
        standardBottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

    }

    companion object {
        fun newInstance(mSongList: ArrayList<Song>): SongFragment {
            // Log.d("TAG2", mSongList.size.toString())
            return SongFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList("mSongList", mSongList)
                }
            }
        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onSongSelected(song: Song?) {
        currentSongSelected = mSongList?.indexOf(song) ?: -1
        Log.d("TAG", currentSongSelected.toString())

        initMusicUI(song)

        ibtnPlayPause.setOnClickListener {
            if (musicService?.mediaPlayer?.isPlaying == true) {
                pauseMusic()
            } else {
                startMusic()
            }
        }

        startMusicServiceToPlay(song)
        // update UI khi lần đầu chạy, pause thì thoát update UI
        updateMusicProgressIndicatorUI2()

        rootView.findViewById<Button>(R.id.ibtnSkipPrevious).setOnClickListener {
            previousMusic()
        }

        rootView.findViewById<Button>(R.id.ibtnSkipNext).setOnClickListener {
            nextMusic()
        }
    }

    private fun initMusicUI(song: Song?) {
        // init UI
        BottomSheetBehavior.from(standardBottomSheet).state = BottomSheetBehavior.STATE_COLLAPSED
        rootView.findViewById<TextView>(R.id.title).text = song?.title
        rootView.findViewById<TextView>(R.id.artist).text = song?.artist
        val imageArtByte: ByteArray? = LoadImage.getSongArt(Uri.parse(song?.path))
        val thumbnail: ImageView = rootView.findViewById<ImageView>(R.id.thumbnail)
        LoadImage.loadThumbnail(requireContext(), imageArtByte, thumbnail)
        progressMusic = rootView.findViewById<LinearProgressIndicator>(R.id.progress_music)
        progressMusic.setProgressCompat(0, true)

        ibtnPlayPause = rootView.findViewById<Button>(R.id.ibtnPlayPause)
        (ibtnPlayPause as MaterialButton).icon =
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_pause)
    }

    fun startMusicServiceToPlay(song: Song?) {
        // play song on main thread
//        playSong(song!!)

        val musicServiceIntent = Intent(requireContext(), MusicService::class.java)
        musicServiceIntent.putExtra("song", song)

        // Foreground service
        // experiment: sau khi thoát app thì dừng luôn
        requireContext().startService(musicServiceIntent)
        // experiment: sau khi thoát app thì chạy đc tầm 20s rồi dừng
        // requireContext().startForegroundService(musicServiceIntent)

        // Bound service
        connection = object: ServiceConnection {
            override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
                val musicServiceBinder = service as MusicService.MusicServiceBinder
                musicService = musicServiceBinder.service

                val musicSkipListener = object : MusicSkipListener {
                    override fun previous() {
                        previousMusic()
                    }

                    override fun next() {
                        nextMusic()
                    }


                }

                musicService!!.musicSkipListener = musicSkipListener
            }

            override fun onServiceDisconnected(name: ComponentName?) {
                // Xóa tham chiếu đến MusicService
                musicService = null
            }
        }
        requireContext().bindService(musicServiceIntent, connection as ServiceConnection, Context.BIND_AUTO_CREATE)

        // đăng kí nhận broadcast
        val broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (intent.action == "PAUSE_MUSIC") {
                    Log.d("TAG", "PAUSE IN BROADCAST")
                    pauseMusic()
                } else if (intent.action == "PLAY_MUSIC") {
                    Log.d("TAG", "PLAY IN BROADCAST")
                    startMusic()
                }
//                else if (intent.action == "PREVIOUS_MUSIC") {
//                    Log.d("TAG", "PREVIOUS IN BROADCAST")
//                    previousMusic()
//                } else if (intent.action == "NEXT_MUSIC") {
//                    Log.d("TAG", "NEXT_MUSIC")
//                    nextMusic()
//                }
            }
        }

        requireContext().registerReceiver(broadcastReceiver, IntentFilter().apply {
            addAction("PAUSE_MUSIC")
            addAction("PLAY_MUSIC")
//            addAction("PREVIOUS_MUSIC")
//            addAction("NEXT_MUSIC")
        })

    }

    fun pauseMusic() {
        (ibtnPlayPause as MaterialButton).icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_play)
        musicService?.MusicServiceBinder()?.pause()
    }

    fun startMusic() {
        (ibtnPlayPause as MaterialButton).icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_pause)
        musicService?.MusicServiceBinder()?.start()
        updateMusicProgressIndicatorUI2()
    }

    fun previousMusic() {
        if(currentSongSelected > 0) currentSongSelected--;
        val song = mSongList?.get(currentSongSelected)
        initMusicUI(song)
        startMusicServiceToPlay(song)
    }

    fun nextMusic() {
        if(currentSongSelected < mSongList!!.size - 1) currentSongSelected++;
        val song = mSongList?.get(currentSongSelected)
        initMusicUI(song)
        startMusicServiceToPlay(song)
    }

    fun updateMusicProgressIndicatorUI() {
        job = GlobalScope.launch() {
            delay(2000) // đợi bind xong service hãng chạy job
            while (true) {
//                val threadName = Thread.currentThread().name
//                Log.d("TAG","printed on thread $threadName")

                /*
                    Đến đây sau khi xong Job nè -> null -> onBind
                 */

                // tránh chạy liên tục trong coroutine
                if(musicService == null) {
                    job?.cancel()
                    break
                }

                // lẽ ra dùng viewmodel tốt hơn
                if (musicService?.mediaPlayer?.isPlaying == false) {
                    job?.cancel()
                    break
                }

                val progress = musicService?.MusicServiceBinder()?.getProgressPercentage()
                Log.d("TAG", progress.toString())
                if (progress != null && progress != -1.0F) {
                    progressMusic.setProgressCompat(progress.toInt(), true)
                }
                delay(1000)
            }
        }

        Log.d("TAG","Đến đây sau khi xong Job nè")
    }

    fun updateMusicProgressIndicatorUI2() {
        val handler = Handler(Looper.getMainLooper())

        val updateProgress = object : Runnable {
            override fun run() {
                val progress = musicService?.MusicServiceBinder()?.getProgressPercentage()
                // Log.d("TAG", progress.toString())
                if (progress != null && progress != -1.0F) {
                    progressMusic.setProgressCompat(progress.toInt(), true)
                }
                handler.postDelayed({ // Gọi lại phương thức này sau 1 giây
                    // chưa bind kịp service thì gọi lại như thế này
                    if(musicService == null && currentSongSelected != -1) {
                        this.run()
                    }
                    if(musicService?.mediaPlayer?.isPlaying == true && currentSongSelected != -1) {
                        this.run()
                    }

                }, 1000)
            }
        }
        handler.post(updateProgress)
    }


    override fun onStop() {
        Log.d("Tag", "onStop")
        super.onStop()
        job?.cancel()

    }

    // đang chạy trên main thread, convert to service
//    fun playSong(song: Song) {
//        if (mediaPlayer == null) {
//            mediaPlayer = MediaPlayer()
//        }
//
//        mediaPlayer?.apply {
//            reset()
//
//            val audioAttributes = AudioAttributes.Builder()
//                .setUsage(AudioAttributes.USAGE_MEDIA)
//                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
//                .build()
//            setAudioAttributes(audioAttributes)
//            setDataSource(requireContext(), Uri.parse(song.path))
//            prepare()
//            start()
//        }
//    }

    override fun onPause() {
        Log.d("Tag", "onPause")
        super.onPause()
    }

    override fun onResume() {
        Log.d("Tag", "onResume")
        updateMusicProgressIndicatorUI2()
        super.onResume()
    }

    override fun onDestroy() {
        Log.d("Tag", "onDestroy")
        super.onDestroy()
        //        if (connection != null)
//            requireContext().unbindService(connection as ServiceConnection)
    }


}