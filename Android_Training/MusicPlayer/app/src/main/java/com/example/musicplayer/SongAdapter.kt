package com.example.musicplayer

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import java.io.ByteArrayInputStream
import java.io.InputStream


interface SongAdapterListener {
    fun onSongSelected(song: Song?)
}

class SongAdapter(val songList: List<Song>?): RecyclerView.Adapter<SongAdapter.SongViewHolder>() {
    private var listener: SongAdapterListener? = null
    fun setSongAdapterListener(listener: SongAdapterListener) {
        this.listener = listener
    }

    class SongViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        val viewForeground = view.findViewById<RelativeLayout>(R.id.view_foreground)
        val thumbnail = view.findViewById<ImageView>(R.id.song_thumbnail)
        val title = view.findViewById<TextView>(R.id.song_title)
        val artist = view.findViewById<TextView>(R.id.song_artist)
    }

    override fun getItemCount(): Int {
        return songList?.size ?: 5
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongViewHolder {
        val viewItem: View = LayoutInflater.from(parent.context).inflate(R.layout.item_song, parent, false)
        val songViewHolder: SongViewHolder = SongViewHolder(viewItem)
        return songViewHolder
    }

    override fun onBindViewHolder(holder: SongViewHolder, position: Int) {
        val song = songList?.get(position)
        holder.title.text = song?.title ?: "Song title $position"
        holder.artist.text = song?.artist?: "Unknown artis"

        // Không chạy được
//        Glide.with(holder.thumbnail.context).load(song?.thumbnail) // thumbail hay path đều không dùng được
//            .placeholder(R.drawable.ic_music_video).error(R.drawable.ic_music_video)
//            .circleCrop().centerCrop().into(holder.thumbnail);

        // Convert to function
//        val imageArtByte = LoadImage.getSongArt(Uri.parse(song?.path)) // song.thumbnail lại không được
//        Glide.with(holder.thumbnail.context).asBitmap().load(imageArtByte)
//            .placeholder(R.drawable.ic_music_video).error(R.drawable.ic_music_video)
//            .circleCrop().centerCrop().into(holder.thumbnail);

        val imageArtByte = LoadImage.getSongArt(Uri.parse(song?.path)) // song.thumbnail lại không được
        LoadImage.loadThumbnail(holder.thumbnail.context, imageArtByte, holder.thumbnail)

        holder.view.setOnClickListener {
            listener?.onSongSelected(song)
        }
    }


}

object LoadImage {
    fun loadThumbnail(context: Context, imageArtByte: ByteArray?, imageView: ImageView) { // song.thumbnail lại không được
        Glide.with(context).asBitmap().load(imageArtByte)
            .placeholder(R.drawable.ic_music_video).error(R.drawable.ic_music_video)
            .circleCrop().centerCrop().into(imageView);
    }

    fun getSongArt(uri: Uri): ByteArray?  {
        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(uri.toString())
        val art: ByteArray? = retriever.embeddedPicture
        retriever.release()
        return art
    }

    fun getAlbumArtBitmap(uri: Uri): Bitmap? {
        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(uri.toString())
        var inputStream: InputStream? = null
        if (retriever.getEmbeddedPicture() != null) {
            inputStream = ByteArrayInputStream(retriever.getEmbeddedPicture());
        }
        val bitmap = BitmapFactory.decodeStream(inputStream);
        retriever.release()
        return bitmap
    }
}