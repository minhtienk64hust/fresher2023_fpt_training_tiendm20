package com.example.musicplayer

import android.Manifest
import android.content.ContentResolver
import android.content.ContentUris
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.example.musicplayer.databinding.ActivityMainBinding
import com.google.android.material.tabs.TabLayout


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val PERMISSION_REQUEST_CODE = 100
    private var mSongList = arrayListOf<Song>()
    private lateinit var viewPagerAdapter: ViewPagerAdapter

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Kiểm tra xem ứng dụng đã có permission truy cập media hay chưa.
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_VIDEO) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            // Yêu cầu người dùng cấp permission.
            ActivityCompat.requestPermissions(this, arrayOf(
                Manifest.permission.READ_MEDIA_IMAGES,
                Manifest.permission.READ_MEDIA_VIDEO,
                Manifest.permission.READ_MEDIA_AUDIO
            ), PERMISSION_REQUEST_CODE)
        } else {
            // Ứng dụng đã có permission truy cập media.
            mSongList = getSongList()
        }

        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab?) {
                // Có thể không cần mà dùng TabLayoutMediator
                // Connecting TabLayout to Adapter
                if (tab != null) {
                    binding.viewPager.setCurrentItem(tab.getPosition())
                };
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                // Handle tab reselect
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                // Handle tab unselect
            }
        })

        val fragmentManager: FragmentManager = supportFragmentManager
        val lifecycle: Lifecycle = lifecycle

        // Log.d("TAG1", mSongList.size.toString())
        viewPagerAdapter = ViewPagerAdapter(fragmentManager, lifecycle, mSongList)

        binding.viewPager.adapter = viewPagerAdapter

        // Có thể không cần mà dùng TabLayoutMediator
        // Change Tab when swiping
        binding.viewPager.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                binding.tabLayout.selectTab(binding.tabLayout.getTabAt(position))
            }
        })

//        The method TabLayoutMediator.attach() calls the method tabLayout.removeAllTabs(); which removes all tabs and then adds tabs again.
//        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
//
//        }.attach()


    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                // Người dùng đã cấp permission.
                mSongList = getSongList()
            } else {
                // Người dùng đã từ chối permission.
                Toast.makeText(this, "Ứng dụng cần permission truy cập media để hoạt động bình thường.", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun getSongList(): ArrayList<Song> {
        /*
            file mp3 ở chrome download hiển thị thông tin là content://media/external/downloads/:id

            Nhưng mà khi tôi vào thư mục của máy là File, Audio thì khi xem info tệp mp3 hiển thị URI là
            content://com.android.providers.media.documents/document/artist:id

         */

//        val sArtworkUri = Uri.parse("content://media/external/downloads") -> Không dùng được

        val mSongList = arrayListOf<Song>()
        val musicResolver: ContentResolver = contentResolver
        val musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI // content://media/external/audio/media

        /*
            1. /storage/emulated/0/Downloads/: Đây là đường dẫn vật lý đến tệp MP3. Bạn có thể sử dụng đường dẫn này
                để truy cập tệp MP3 trong ứng dụng File Explorer.

            2. content://media/external/audio/media: Đây là URI nội bộ được sử dụng bởi Android để truy cập tệp MP3.
            URI này được sử dụng bởi các ứng dụng để truy cập dữ liệu phương tiện, chẳng hạn như âm nhạc, video và ảnh.

            Trong trường hợp này, hai đường dẫn trỏ đến cùng một tệp MP3. Tệp MP3 được lưu trữ trong
            thư mục /storage/emulated/0/Downloads/. Tuy nhiên, URI nội bộ trỏ đến tệp MP3 theo cách khác.
            URI nội bộ sử dụng thư mục /storage/emulated/0/Music làm thư mục gốc cho tất cả các tệp âm thanh.
            Tệp MP3 trong thư mục /storage/emulated/0/Downloads/ được ánh xạ đến thư mục /storage/emulated/0/Music
            bằng cách sử dụng ID của tệp MP3.

            Để tìm hiểu thêm về URI nội bộ, bạn có thể tham khảo tài liệu của Android về MediaStore: https://developer.android.com/reference/android/provider/MediaStore.

         */
        // Log.d("TAG", musicUri.toString()) // content://media/external/audio/media

        val musicCursor: Cursor? = musicResolver.query(musicUri, null, null, null, null)
        if (musicCursor != null && musicCursor.moveToFirst()) {
            //get columns
            val titleColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.TITLE)
            val idColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media._ID)
            val albumID = musicCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID)
            val artistColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST)
            val songPath = musicCursor.getColumnIndex(MediaStore.Audio.Media.DATA)
            val durationID = musicCursor.getColumnIndex(MediaStore.Audio.Media.DURATION)

            //add songs to list
            do {
                val thisId = musicCursor.getLong(idColumn)
                val thisTitle = musicCursor.getString(titleColumn)
                val thisArtist = musicCursor.getString(artistColumn)
                val thisSongPath = Uri.parse(musicCursor.getString(songPath))
                val albumID = musicCursor.getLong(albumID)
                val uri = ContentUris.withAppendedId(musicUri, albumID)
                val duration = musicCursor.getInt(durationID)

                // Log.d("TAG", thisSongPath.toString()) // /storage/emulated/0/Download/Cắt Đôi Nỗi Sầu-Tăng Duy Tân, Drum7.mp3
                // Log.d("TAG", uri.toString() ) // content://media/external/audio/media/4262104993398956869
                // Log.d("TAG", thisSongLink.toString()) // /storage/emulated/0/Download/:id
                mSongList.add(
                    Song(
                        thisId, thisTitle, thisArtist, uri.toString(),
                        thisSongPath.toString(),
                        duration
                    )
                )
            } while (musicCursor.moveToNext())
        }

        musicCursor?.close()
        return mSongList
    }


}

class ViewPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) : FragmentStateAdapter(fragmentManager, lifecycle) {
    var fragments = mutableListOf<Fragment>()

    private lateinit var mSongList: ArrayList<Song>

    constructor(
        fragmentManager: FragmentManager,
        lifecycle: Lifecycle,
        mSongList: ArrayList<Song>,
    ) : this(fragmentManager, lifecycle) {
        this.mSongList = mSongList
        fragments.apply {
            add(SongFragment.newInstance(mSongList))
            add(PlaylistFragment())
            add(ArtistFragment())
            add(AlbumFragment())
        }
    }

    override fun getItemCount(): Int = fragments.size

    override fun createFragment(position: Int): Fragment {
        return fragments.get(position)
    }
}