package com.example.musicplayer

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.util.Log
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.media.app.NotificationCompat.MediaStyle
import com.bumptech.glide.Glide
import java.util.Timer
import java.util.TimerTask

interface MusicSkipListener {
    fun previous()
    fun next()
}


// Foreground service chạy OK -> Chuyển sang Bound service để pause video
class MusicService : Service() {
    var mediaPlayer: MediaPlayer? = null
    var song: Song? = null
    val musicServiceBinder: MusicServiceBinder = MusicServiceBinder()
    var mNotifyManager: NotificationManager? = null
    lateinit var mediaSession: MediaSessionCompat
    lateinit var playbackStateBuilder: PlaybackStateCompat.Builder
    var musicSkipListener: MusicSkipListener? = null

    override fun onCreate() {
        super.onCreate()
        Log.d("TAG", "onCreate")
        // Tạo notification
        createNotificationChannel()
    }

    // vẫn gọi khi dùng Bound service
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d("TAG", "onStartCommand")

        val song = intent?.getParcelableExtra<Song>("song")
        this.song = song
//        Log.d("TAG", song?.title.toString())

        if (song!=null) playSong(song)

        // Foreground service
//        return super.onStartCommand(intent, flags, startId)

        // Bound service
        // Cho phép các thành phần khác của ứng dụng tương tác với service



        return START_NOT_STICKY
    }

    // Bound service
    override fun onUnbind(intent: Intent?): Boolean {
        return super.onUnbind(intent)
    }

    // lần đầu phát nhạc
    fun playSong(song: Song) {
        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer()
        }
        mediaPlayer?.apply {
            reset()

            val audioAttributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_MEDIA)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build()
            setAudioAttributes(audioAttributes)
            setDataSource(this@MusicService, Uri.parse(song.path))
            prepare()
            start()
        }

        sendNotification()
    }

    // Bound service
    override fun onBind(intent: Intent?): IBinder? {
        Log.d("TAG", "onBind")
        return musicServiceBinder
    }

    // Bound service
    inner class MusicServiceBinder : Binder() {
        val service: MusicService
            get() = this@MusicService

        fun pause() {
            if (mediaPlayer?.isPlaying == true) {
                mediaPlayer?.pause()
            }

            // vẫn có thể bị lỗi không đổi được trạng thái???
            mediaSession.setPlaybackState(
                playbackStateBuilder
                    .setState(PlaybackStateCompat.STATE_PAUSED, mediaPlayer?.currentPosition!!.toLong(), 1f)
                    .build()
            )

        }

        fun start() {
            if (mediaPlayer?.isPlaying == false) {
                mediaPlayer?.start()
            }

            // ấn start ở fragment thì cập nhật giao diện ở notification
            // vẫn có thể bị lỗi không đổi được trạng thái???
            mediaSession.setPlaybackState(
                playbackStateBuilder
                    .setState(PlaybackStateCompat.STATE_PLAYING, mediaPlayer?.currentPosition!!.toLong(), 1f)
                    .build()
            )

        }

       // milliseconds
        fun getCurrentPlayingTime(): Int {
            return mediaPlayer?.currentPosition ?: -1
        }

        // milliseconds
        fun getTotalDuration(): Int {
            return mediaPlayer?.duration ?: -1
        }

        // chả hiểu sao được tầm 2,3s là lại in ra 0.0 rồi bắt đầu lại từ đầu mới đúng (bị sai 1 lần bắt đầu lại vòng)
        // %
        fun getProgressPercentage(): Float {
            if (mediaPlayer == null) return (-1).toFloat()
            else {
                return (getCurrentPlayingTime().div(1000).toFloat()
                        .div(getTotalDuration().div(1000).toFloat())
                        ) * 100
            }

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("TAG", "onDestroy")
        mediaPlayer?.release()
    }

    fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                PRIMARY_CHANNEL_ID,
                "Music player notification",
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.enableVibration(true)
            notificationChannel.description = "Music player notification description"
            mNotifyManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            mNotifyManager!!.createNotificationChannel(notificationChannel)
        }
    }

    fun sendNotification() {
        val notifyBuilder = notificationBuilder
        // mỗi lần tạo lại một notification builder mới, nên rất có thể xảy ra lỗi nếu previous/next liên tục gây ra lỗi
        Log.d("TAG", notificationBuilder.toString())
        mNotifyManager?.notify(NOTIFICATION_ID, notifyBuilder.build())
    }

    private val notificationBuilder: NotificationCompat.Builder
        private get() {
            // khởi động lại app
//            val notificationIntent = Intent(this, MainActivity::class.java)
            //  bring an existing instance of the app to the foreground, or create a new instance if the app isn't running
            val notificationIntent = applicationContext.packageManager.getLaunchIntentForPackage(applicationContext.packageName)
            val notificationPendingIntent = PendingIntent.getActivity(
                this, NOTIFICATION_ID, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
            )

            val imageBitmap = LoadImage.getAlbumArtBitmap(Uri.parse(song?.path))

            mediaSession = MediaSessionCompat(
                applicationContext,
                "MUSIC_SESSION_TAG"
            )

            mediaSession.isActive = true
            mediaSession.setMetadata(
                MediaMetadataCompat.Builder()
                    .putString(MediaMetadataCompat.METADATA_KEY_TITLE, song?.title ?: "Music notification")
                    .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, song?.artist ?: "This is your notification text.")
                    .putLong(MediaMetadataCompat.METADATA_KEY_DURATION, song?.duration!!.toLong())
                    .build()
            )

            mediaSession.setCallback(object : MediaSessionCompat.Callback() {
                override fun onSeekTo(pos: Long) {
                    super.onSeekTo(pos)
                    mediaPlayer?.seekTo(pos.toInt())
                }

                override fun onPlay() {
                    Log.d("TAG", "Play")
                    MusicServiceBinder().start()

                    // cập nhật giao diện bottomsheet trong fragment
                    val intent = Intent()
                    intent.action = "PLAY_MUSIC"
                    sendBroadcast(intent)
                }

                override fun onPause() {
                    Log.d("TAG", "Pause")
                    MusicServiceBinder().pause()

                    val intent = Intent()
                    intent.action = "PAUSE_MUSIC"
                    sendBroadcast(intent)
                }

                override fun onSkipToNext() {
                    // làm như này sẽ gây ra lỗi next liên tục??, lý do phức tạp do tạo lại service
//                    val intent = Intent()
//                    intent.action = "NEXT_MUSIC"
//                    sendBroadcast(intent)

                    musicSkipListener?.next()

                }

                override fun onSkipToPrevious() {
//                    val intent = Intent()
//                    intent.action = "PREVIOUS_MUSIC"
//                    sendBroadcast(intent)

                    musicSkipListener?.previous()
                }

            })

            playbackStateBuilder = PlaybackStateCompat.Builder()
                .setState(PlaybackStateCompat.STATE_PLAYING, 0, 1f)

                .setActions(
                    PlaybackStateCompat.ACTION_SEEK_TO or
                    PlaybackStateCompat.ACTION_PLAY or
                    PlaybackStateCompat.ACTION_PAUSE or
                    PlaybackStateCompat.ACTION_SKIP_TO_NEXT or
                    PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS or
                    PlaybackStateCompat.ACTION_SET_REPEAT_MODE
                )
            mediaSession.setPlaybackState(playbackStateBuilder.build())



            // Build the notification
            return NotificationCompat.Builder(this, PRIMARY_CHANNEL_ID).apply {
//                setContentTitle(song?.title ?: "Music notification")
//                setContentText(song?.artist ?: "This is your notification text.")
                setSmallIcon(R.drawable.ic_music_video)
                setLargeIcon(imageBitmap)
                setStyle(
                    MediaStyle()
                        .setMediaSession(
                            mediaSession.sessionToken
                        )
                        .setShowActionsInCompactView(0, 1, 2)
                )
                setPriority(NotificationCompat.PRIORITY_HIGH)

                setContentIntent(notificationPendingIntent)
                setAutoCancel(true) // Setting auto-cancel to true closes the notification when user taps on it.
                setDefaults(NotificationCompat.DEFAULT_ALL)
//                addAction(
//                    R.drawable.ic_skip_previous,
//                    null,
//                    PendingIntent.getBroadcast(
//                        this@MusicService,
//                        NOTIFICATION_ID,
//                        Intent("PAUSE"),
//                        PendingIntent.FLAG_MUTABLE
//                    )
//                )
//                if (mediaPlayer?.isPlaying == true) {
//                    addAction(
//                        R.drawable.ic_pause,
//                        null,
//                        PendingIntent.getBroadcast(
//                            this@MusicService,
//                            NOTIFICATION_ID,
//                            Intent("PAUSE"),
//                            PendingIntent.FLAG_MUTABLE
//                        )
//                    )
//                }
//                if (mediaPlayer?.isPlaying == false) {
//                    addAction(
//                        R.drawable.ic_play,
//                        null,
//                        PendingIntent.getBroadcast(
//                            this@MusicService,
//                            NOTIFICATION_ID,
//                            Intent("PLAY"),
//                            PendingIntent.FLAG_MUTABLE
//                        )
//                    )
//                }
//                addAction(
//                    R.drawable.ic_skip_next,
//                    null,
//                    PendingIntent.getBroadcast(
//                        this@MusicService,
//                        NOTIFICATION_ID,
//                        Intent("NEXT"),
//                        PendingIntent.FLAG_MUTABLE
//                    )
//                )
            }
        }

    companion object {
        // Constants for the notification actions buttons.
        private const val ACTION_PAUSE_MUSIC_NOTIFICATION = "com.android.example.musicplayer.ACTION_PAUSE_MUSIC_NOTIFICATION"

        // Notification channel ID.
        private const val PRIMARY_CHANNEL_ID = "primary_notification_channel"

        // Notification ID.
        private const val NOTIFICATION_ID = 0

    }





}

