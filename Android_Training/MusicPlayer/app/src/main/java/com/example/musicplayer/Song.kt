package com.example.musicplayer

import android.os.Parcel
import android.os.Parcelable
import kotlin.time.Duration

//import java.io.Serializable
//
//class Song(val id: Long, val title: String, val artist: String, val thumbnail: String, val path: String) : Serializable {
//}

data class Song(val id: Long, val title: String, val artist: String, val thumbnail: String, val path: String, val duration: Int) :
    Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(title)
        parcel.writeString(artist)
        parcel.writeString(thumbnail)
        parcel.writeString(path)
        parcel.writeInt(duration)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Song> {
        override fun createFromParcel(parcel: Parcel): Song = Song(parcel)

        override fun newArray(size: Int): Array<Song?> = arrayOfNulls(size)
    }
}